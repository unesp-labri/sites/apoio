const navbar = {
    hideOnScroll: true, // add
    title: 'Apoio LabRI',
    logo: {
        alt: 'LabRI',
        src: 'img/labriunesp-02.svg',
    },
    items: [
        {
            type: 'doc',
            position: 'right',
            docId: 'geral/intro',
            label: 'Geral',
        },
        {
            label: "Projetos",
            position: "right",
            items: [
                {
                    docId: 'projetos/dados/intro',
                    label: 'Projetos de Dados',
                },
                {
                    docId: 'projetos/ensino/intro',
                    label: 'Projetos de Ensino',
                },
                {
                    docId: 'projetos/extensao/intro',
                    label: 'Projetos de Extensão',
                },
                {
                    docId: 'projetos/sistemas/intro',
                    label: 'Projetos de Sistemas',
                },
            ],
        },
        {
            href: 'https://gitlab.com/unesp-labri',
            label: 'Gitlab',
            position: 'right',
            className: 'header-gitlab-link',
            'aria-label': 'GitLab repository'
        },
        {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Tutorial',
        },
        { to: '/blog', label: 'Blog', position: 'left' },
    ],
};

export default navbar;