import React, { useState } from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import ThemedImage from "@theme/ThemedImage";
import Link from "@docusaurus/Link";
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext, Dot } from 'pure-react-carousel';
import "pure-react-carousel/dist/react-carousel.es.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft, faChevronRight } from "@fortawesome/free-solid-svg-icons";
import HomepageFeatures from '../components/HomepageFeatures';
import styles from "./styles.module.css";


const slides = [
  <header className={clsx(styles.heroBanner, styles.slide)}
    style={{ backgroundImage: "linear-gradient(to top, #969fae, #849bc1, #7097d5, #5893e7, #348efa)" }}>
    <div className="container">
      <h1 className={clsx(styles.h1HeroTitle, "hero__title")}>LabRI ─ Site de Apoio</h1>
      <p className={clsx(styles.subtitle, "hero__subtitle")}>Desenvolvimento de projetos</p>
      <div className={styles.button}>
        <Link className={clsx("button button--outline button-secondary button-lg")} style={{ color: "white" }} to="/docs/projetos/intro"
          target="_blank">
          Get Started
        </Link>
      </div>
    </div>
  </header>,
  <div className={clsx(styles.heroBanner, styles.slide)}
    style={{ backgroundImage: "linear-gradient(to left, #969fae, #849bc1, #7097d5, #5893e7, #348efa)" }}>
    <div className="container">
      <Link to="/docs/projetos/extensao/intro" target="_blank">
        <img src="./img/projetos/logos/extensao.svg" className={styles.bkgimg} />
      </Link>
      <div className={styles.button}>
        <Link className={clsx("button button--outline button-secondary button-lg")} style={{ color: "white" }}
          to="/docs/projetos/extensao/intro" target="_blank">
          Acesse aqui
        </Link>
      </div>
    </div>
  </div>,
  <div className={clsx(styles.heroBanner, styles.slide)}
    style={{ backgroundImage: "linear-gradient(to left, #969fae, #849bc1, #7097d5, #5893e7, #348efa)" }}>
   <div className="container">
      <Link to="/docs/projetos/sistemas/intro" target="_blank">
        <img src="./img/projetos/logos/sistemas.svg" className={styles.bkgimg} />
      </Link>
      <div className={styles.button}>
        <Link className={clsx("button button--outline button-secondary button-lg")} style={{ color: "white" }}
          to="/docs/projetos/sistemas/intro" target="_blank">
          Acesse aqui
        </Link>
      </div>
    </div>
  </div>,
  <div className={clsx(styles.heroBanner, styles.slide)}
    style={{ backgroundImage: "linear-gradient(to left, #969fae, #849bc1, #7097d5, #5893e7, #348efa)" }}>
    <div className="container">
      <Link to="/docs/projetos/ensino/intro" target="_blank">
        <img src="./img/projetos/logos/ensino.svg" className={styles.bkgimg} />
      </Link>
      <div className={styles.button}>
        <Link className={clsx("button button--outline button-secondary button-lg")} style={{ color: "white" }}
          to="/docs/projetos/ensino/intro" target="_blank">
          Acesse aqui
        </Link>
      </div>
    </div>
  </div>,
  <div className={clsx(styles.heroBanner, styles.slide)}
    style={{ backgroundImage: "linear-gradient(to left, #969fae, #849bc1, #7097d5, #5893e7, #348efa)" }}>
    <div className="container">
      <Link to="/docs/projetos/dados/intro" target="_blank">
        <img src="./img/projetos/logos/dados.svg" className={styles.bkgimg} />
      </Link>
      <div className={styles.button}>
        <Link className={clsx("button button--outline button-secondary button-lg")} style={{ color: "white" }}
          to="/docs/projetos/dados/intro" target="_blank">
          Acesse aqui
        </Link>
      </div>
    </div>
  </div>,
]

export default function Home() {
  const { siteConfig } = useDocusaurusContext();
  const [buttonVisible, setButtonVisible] = useState(false);
  return (
    <Layout title={`${siteConfig.title}`}>
      <CarouselProvider
        className={styles.carousel}
        totalSlides={slides.length}
        isPlaying
        interval={3000}
        dragEnabled={false}
        isIntrinsicHeight
        infinite
        naturalSlideHeight={undefined}
        naturalSlideWidth={undefined}>
        <div
          onMouseEnter={() => {
            setButtonVisible(true);
          }}
          onMouseLeave={() => {
            setButtonVisible(false);
          }}>
          <Slider>
            {slides.map((elem, idx) => (
              <Slide key={idx} index={idx}>
                {elem}
              </Slide>
            ))}
          </Slider>
          <ButtonBack
            id="back"
            className={clsx(
              styles.button,
              styles.carouselbutton,
              styles.backbutton,
            )}
            style={{ visibility: buttonVisible ? "visible" : "hidden" }}>
            <FontAwesomeIcon icon={faChevronLeft} />
          </ButtonBack>
          <ButtonNext
            className={clsx(
              styles.button,
              styles.carouselbutton,
              styles.nextbutton,
            )}
            style={{ visibility: buttonVisible ? "visible" : "hidden" }}>
            <FontAwesomeIcon icon={faChevronRight} />
          </ButtonNext>
          <div className={styles.carouseldots}>
            {slides.map((__, idx) => (
              <Dot className={styles.carouseldot} key={idx} slide={idx} />
            ))}
          </div>
        </div>
      </CarouselProvider>
      <main>
        <HomepageFeatures />
      </main>
    </Layout>
  );
}
