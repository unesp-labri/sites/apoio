import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    Svg: require('../../static/img/projetos/dados/logo-projetos-dados.svg').default,
    description: (
      <>
        Os projetos de dados são iniciativas que visam coletar, tratar, analisar e, quando possível, disponibilizar dados relevantes para as pesquisas em Relações Internacionais. Deste modo, neste espaço é possível encontrar projetos voltados à incorporação de tecnologias digitais para melhorar o manuseio da crescente quantidade de dados disponíveis nas redes sociais, nos meios de comunicação, nas instâncias governamentais e organismos internacionais.
      </>
    ),
    link: './docs/projetos/dados/intro'
  },
  {
    Svg: require('../../static/img/projetos/ensino/logo-projetos-ensino.svg').default,
    description: (
      <>
        Os projetos de ensino são iniciativas voltadas à promoção de oficinas, material de apoio e cursos que incentivem e auxiliem uma melhor incorporação de ferramentas tecnológicas nas atividades de pesquisa. Sendo assim, neste espaço é possível encontrar projetos conectados para melhorar o intercâmbio entre as Relações Internacionais e a ciência de dados.
      </>
    ),
    link: './docs/projetos/ensino/intro'
  },
  {
    Svg: require('../../static/img/projetos/extensao/logo-projetos-extensao.svg').default,
    description: (
      <>
        Os projetos de extensão são iniciativas voltadas para melhorar e incentivar a interação entre o meio acadêmico e a sociedade. Assim, neste espaço é possível encontrar projetos que buscam envolver docentes e discentes em atividades voltadas à divulgação científica.
      </>
    ),
    link: './docs/projetos/extensao/intro'
  },
  {
    Svg: require('../../static/img/projetos/sistemas/logo-projetos-sistemas.svg').default,
    description: (
      <>
      Os projetos de infraestrutura computacional são iniciativas que visam promover uma melhor utilização dos recursos computacionais nas pesquisas de Relações Internacionais. Com isso, neste espaço é possível encontrar projetos relacionados à construção e manutenção de pequenas e versáteis estações de trabalho multifuncionais que dão suporte às atividades de pesquisa, ensino e extensão.      
      </>
    ),
    link: './docs/projetos/sistemas/intro'
  },
];

function Feature({Svg, title, description,link}) {
  return (
    <div className={clsx('col col--6')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <p>{description}</p>
      </div>
      <div className="text--center">
          <a
            className={clsx(
              'button button--secondary button--md',
            )}
            href={link} target="_blank">
            Acesse aqui
          </a>
        </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
