import React from 'react';
import clsx from 'clsx';
import { MDXProvider } from "@mdx-js/react";
import Link from "@docusaurus/Link";
import { useBaseUrlUtils } from "@docusaurus/useBaseUrl";
import { usePluralForm } from "@docusaurus/theme-common";
import MDXComponents from "@theme/MDXComponents";
import BlogPostAuthors from "@theme/BlogPostAuthors";
import TagsListInline from "@theme/TagsListInline";
import styles from "./styles.module.css";

const MONTHS = [
  'Jan',
  'Fev',
  'Mar',
  'Abr',
  'Mai',
  'Jun',
  'Jul',
  'Ago',
  'Set',
  'Out',
  'Nov',
  'Dez',
];

function BlogPostItem(props) {
  const {
    children, frontMatter, metadata, truncated, assets, isBlogPostPage = false,
  } = props;
  const { date, permalink, tags, readingTime, title, authors } = metadata;
  const { keywords } = frontMatter;
  const { withBaseUrl } = useBaseUrlUtils();
  const renderPostHeader = () => {
    const TitleHeading = isBlogPostPage ? "h1" : "h2";
    const match = date.substring(0, 10).split('-');
    const year = match[0];
    const month = MONTHS[parseInt(match[1], 10) - 1];
    const day = parseInt(match[2], 10);
    return (
      <header>
        <TitleHeading className={clsx(styles.blogPostTitle)} itemProp="headline">
          {isBlogPostPage ? title : <Link to={permalink}>{title}</Link>}
        </TitleHeading>
        <div className={clsx(styles.blogPostData, "margin-vert--md")}>
          <time itemProp="datePublished">
            {day}/{month}/{year}
            {readingTime && <> · Tempo de leitura: {Math.ceil(readingTime)} min</>}
          </time>
        </div>
        {isBlogPostPage && (
          <BlogPostAuthors authors={authors} assets={assets} />
        )}
      </header>
    )
  }

  if (!isBlogPostPage) {
    return (
      <div
        className={clsx("col col--4", styles.blogCard)}
        itemProp="blogPost"
        itemScope
        itemType="http://schema.org/BlogPosting">
        <Seo {...{ keywords, image: assets.image }} />
        {assets.image && (
          <img itemProp="image" className={styles.cardImg} src={assets.image} />
        )}
        <div className={styles.cardContent}>
          {renderPostHeader()}
          <div className="markdown">
            <MDXProvider components={MDXComponents}>{children}</MDXProvider>
            {truncated && (
              <Link
                to={metadata.permalink}
                aria-label={`Read more about ${title}`}>
                <b>
                  Leia mais
                </b>
              </Link>
            )}
          </div>
          {tags.length > 0 && (
            <div className={"col col--9 margin-top--md"}>
              <TagsListInline tags={tags} />
            </div>
          )}
        </div>
      </div>
    )
  }

  return (
    <>
      <Seo {...{ keywords, image: assets.image }} />

      <article
        itemProp="blogPost"
        itemScope
        itemType="http://schema.org/BlogPosting">
        {renderPostHeader()}
        {assets.image && (
          <meta
            itemProp="image"
            content={withBaseUrl(assets.image, { absolute: true })}
          />
        )}
        <div className="markdown">
          <MDXProvider components={MDXComponents}>{children}</MDXProvider>
        </div>
        {(tags.length > 0 || truncated) && (
          <footer
            className={clsx(
              "row docusaurus-mt-lg",
              styles.blogPostDetailsFull,
            )}>
            {tags.length > 0 && (
              <div className={"col"}>
                <TagsListInline tags={tags} />
              </div>
            )}
          </footer>
        )}
      </article>
    </>
  );
}



export default BlogPostItem;