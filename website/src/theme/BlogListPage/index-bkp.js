import React from 'react';
import clsx from 'clsx';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import Link from "@docusaurus/Link";
import Layout from "@theme/Layout";
import BlogPostItem from '@theme/BlogPostItem';
import BlogListPaginator from '@theme/BlogListPaginator';
import Seo from "@theme/Seo";
import { ThemeClassNames } from "@docusaurus/theme-common";
import styles from "./styles.module.css";

function BlogListPage(props) {
  const { metadata, items, assets, truncated } = props;
  const {
    siteConfig: { title: siteTitle },
  } = useDocusaurusContext();
  const { blogDescription, blogTitle, permalink } = metadata;
  const isBlogOnlyMode = permalink === "/";
  const title = isBlogOnlyMode ? siteTitle : blogTitle;
  return (
    <Layout
      title={title}
      description={blogDescription}
      wrapperClassName={ThemeClassNames.wrapper.blogPages}
      pageClassName={ThemeClassNames.page.blogListPage}>
      <header>
        <h1 className={clsx(styles.title, "text--center margin-top--lg margin-bottom--xs")}>
          {blogTitle}
        </h1>
        <div className="text--center margin-top--md">
          <a href="/blog/tags" style={{ marginRight: 22 }} title="Tags">
            <img itemProp="image" className={styles.iconeHome} src="/img/blog/tags_default.png" />
          </a>
        </div>
      </header>
      <section className={styles.wrapper}>
        <div className={clsx("col col--4", styles.cardInfos)}>
          <h3>Apresentação</h3>
          <p>Os Cadernos LabRI são uma publicação circunscrita a divulgar os trabalhos de grupos e indivíduos vinculados ao LabRI/UNESP. Os principais objetivos desta publicação são
            <p>1. Fornecer um espaço aos grupos para divulgação de seus trabalhos.</p>
            <p>2. Auxiliar uma melhor comunicação dos projetos e dos produtos derivados</p>
            <p>3. Estímulo à utilização de tecnologias digitais no cotidiano acadêmico</p>
            <p>Devido aos aspectos apontados acima, os Cadernos LabRI não estão abertos a submissão de trabalhos externos para a publicação. Apesar disso, eventualmente, convidados também poderão ter seus trabalhos divulgados nos Cadernos LabRI.</p>
          </p>
          <Link
            to="https://labriunesp.org/docs/cadernos/intro"
            aria-label={`Read more about ${title}`}
            target="_blank">
            <b>
              Leia mais
            </b>
          </Link>
        </div>
        <div className={clsx("col col--4", styles.cardInfos)}>
          <h3>Expediente</h3>
          <p>
            O editor principal é o docente que  está responsável pela coordenação do LabRI/UNESP.
            <p>Porém, as séries temáticas e de grupos de pesquisa terão como editor responsável o coordenador da respectiva série temática ou grupo de pesquisa.</p>
            <p>- Editor principal: Marcelo Passini Mariano</p>
            <p>- Assistentes editoriais: Júlia dos Santos Silveira, Pedro Campagna, Rafael de Almeida</p>
          </p>
          <Link
            to="https://labriunesp.org/docs/cadernos/expediente"
            aria-label={`Read more about ${title}`}
            target="_blank">
            <b>
              Leia mais
            </b>
          </Link>
        </div>
      </section>
      <main className={styles.wrapper}>
        <div className={styles.CardContainer}>
          {items.map(({ content: BlogPostContent }) => (
            <BlogPostItem
              key={BlogPostContent.metadata.permalink}
              frontMatter={BlogPostContent.frontMatter}
              assets={BlogPostContent.assets}
              metadata={BlogPostContent.metadata}
              truncated={BlogPostContent.metadata.truncated}>
              <BlogPostContent />
            </BlogPostItem>
          ))}
        </div>
        <BlogListPaginator metadata={metadata} />
      </main>
    </Layout>
  )
}

export default BlogListPage;