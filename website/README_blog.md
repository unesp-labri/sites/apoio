# Documentação Docusaurus: Considerações

## Plugins 

### plugin-content-blog

Possui algumas ferramentas que não constam no '@docusaurus/preset-classic'. 

Não é necessário instalar o plugin se já estiver utilizando o '@docusaurus/preset-classic', apenas utilizar a partir do 'preset'. 

- Facilita a utilização de documentos diversos (md, mdx, js, ...)

## Swizzling 

- Permite uma costumização avançada do site. 

- Ex: customização da página padrão "404 not found" a partir do swizzle.

```
yarn run swizzle @docusaurus/theme-classic NotFound
```

Esse comando irá criar uma página de customização da página NotFound.

- Customização com React. 

- Ter cuidado ao utilizar a opção 'swizzle' para customizar uma página/componente. 

- [Ler mais](https://docusaurus.io/pt-BR/docs/swizzling) 

## Página Inicial 

Por padrão, o blog do Docusaurus faz da página inicial uma "blog list page" onde todos os posts do blog são disponibilizados e o mais recente se encontra no topo. 

### "truncate" 

Utilizar ```<!--truncate-->``` para limitar o conteúdo do post que será exibido na página inicial. 

### Numero de post por página

A página inicial, por padrão, exibe os **10 últimos posts** do blog, porém é possível editar o número de posts exibidos. 

Basta seguir as seguintes modificações: 

```
module.exports = {
  // ...
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        blog: {
          blogTitle: 'Docusaurus blog!',
          blogDescription: 'A Docusaurus powered blog!',
          postsPerPage: 'ALL',
        },
      },
    ],
  ],
};
```

## Customização do blog 

Criar documento de ```styles.css``` para editar elementos chave do blog e depois adicionar no ```docusaurus.config.js```. 

# Sobre o blog atual 

## Sobre os arquivos Cadernos LabRI 

|Nome do arquivo|O que faz|Comentário|
|---------------|---------|----------|
|**BlogListPage**|Página inicial do blog|
|**BlogListPaginator**|Paginação do blog|Pode ser feito de maneira automática. EX: ```postsPerPage: 10```|
|**BlogPostItem**|Tempo de leitura||
|**BlogPostPage**/**BlogPostPage1**|Página com o texto da publicação||
|**BlogTagesListPage**/**BlogTagsPostsPage**|Tags utilizadas no blog|Pode ser feito de modo automático.|


É possível simplificar o processo de mostrar o tempo de leitura de uma postagem ajustando apenas o ```docusaurus.config.js```: 

```
blog: {
          showReadingTime: true, // When set to false, the "x min read" won't be shown
          readingTime: ({content, frontMatter, defaultReadingTime}) =>
            defaultReadingTime({content, options: {wordsPerMinute: 300}}),
        },
```

Modificando o `docusaurus.config.js`:

|Nome|O que faz|Como usar|
|----|---------|---------|
|**blogTitle**|Muda o nome do blog|Cadernos LabRI/UNESP|


### Reverter commit

```
git reset --hard 6b9a5d70dfed1efe3cc0984044a63c87316f0eba
git push origin HEAD --force
```

### Plugins 

```
yarn add @fortawesome/font awesome-free
yarn add @fortawesome/free-solid-svg-icons
yarn add pure-react-carousel
```

### Mudar NAVBAR 

Criação da pasta config 

Exemplo: [Josh Cena](https://github.com/Josh-Cena/Josh-Cena.github.io)
