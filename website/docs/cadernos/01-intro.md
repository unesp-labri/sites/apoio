---
id: intro
title: Apresentação Cadernos LabRI
sidebar_label: Apresentação
slug: /cadernos/intro
---

- As informações sobre a publicação "Cadernos LabRI/UNESP" está no [site aberto](https://labriunesp.org/docs/cadernos/intro)
- A documentação do sistema de geração dos Cadernos LabRI/UNESP está no [site fechado](/docs/projetos/sistemas/dev/geral/cadernos-labri)

