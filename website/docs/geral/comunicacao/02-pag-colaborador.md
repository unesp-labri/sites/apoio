---
id: pag-colaborador
title: Como customizar a Página do Colaborador
sidebar_label: Página do Colaborador
slug: /geral/comunicacao/pag-colaborador
---

## Como costumizar a Página do Colaborador

Acesse os exemplos de página do colaborador para visualizar o que será editado. 

- [Exemplo geral](https://labriunesp.org/exemplo/equipe);

- [Exemplo Júlia Silveira](https://labriunesp.org/exemplo/equipe/julia-silveira)

### Inserir informações na página 

#### Como editar o código diretamente do Gitlab utilizando o Gitpod 

1. Acesso o repositório do site no [Gitlab](https://gitlab.com/unesp-labri/sites/labri/-/tree/main/website);

2. Clique na opção "Gitpod" para abrir automaticamente o código sem precisar clonar o repositório;

[gif gitpod](https://i.imgur.com/wVB1dhn.gif)

3. Entre na pasta **"website"**;

4. Entre na pasta **"src"**;

5. Entre na pasta **"exemplo"**;

6. Entre na pasta **"equipe"**;

7. Siga para o tutorial de como encontrar o código necessário para a edição.


#### Como encontrar o código necessário para customizar a sua página do colaborador 

1. Entre na pasta **"src"**;

2. Entre na pasta **"pages"**;

3. Entre na pasta **"exemplos"**;

4. Entre na pasta **"equipe"**;

5. Crie uma pasta com o seu nome (Ex: "julia-silveira");

6. Copie o documento **"index.js"** da pasta **"julia-silveira"** ou da pasta **"equipe"** e coloque na sua pasta;

7. Copie o documento **"styles.module.css"** da pasta **"julia-silveira"** ou da pasta **"equipe"** e coloque na sua pasta.

_Dica_: escreva no terminal do Gitpod **"yarn install"** e depois **"yarn start"** para conseguir visualizar as informações que estão sendo inseridas. Para acessar a sua página basta colocar **"/exemplo/equipe/nome-da-sua-pasta"** na frente do link gerado pelo comando yarn. 

_Dica 2_: visualize os exemplos já inseridos no site para ter uma ideia de como é o modelo que será modificado. Acesse aqui: [Exemplo padrão](https://labriunesp.org/exemplo/equipe) e [Página Júlia Silveira](https://labriunesp.org/exemplo/equipe/julia-silveira)

- **Como modificar as informações da header**

1. Identifique a função principal do código "function Equipe()";

2. Modifique o nome da página em "Página Membros" no "Layout title";

![Gif 1](https://i.imgur.com/OApNDWm.gif)

3. Para modificar o título da header modifique o "Nome do Membro" no "hero__title" com o seu nome;

4. Para modificar o subtítulo da header modifique a "Função dentro do LabRI" no "hero__subtitle";

![Gif 2](https://i.imgur.com/opU55gt.gif)

5. Escolha e modifique a paleta de cores da sua página 

6. Para escolher a Paleta de Cores: 

    Utilize sites que geram paletas de cores automaticamente 

    - [Coolors](https://coolors.co/): Ferramenta que permite criar paletas de cores de maneira personalizada, além de permitir salvar e/ou arquivar uma paleta de cor que já foi criada. 

    - Outras ferramentas similares: [Colormind](http://colormind.io/) e [Adobe Color](https://color.adobe.com/create)

    -  _Dica_: Optar por uma paleta de cor que não possua muitos contrastes ou cores muito quentes.

7. Para definir a paleta de cores é necessário entrar no documento "styles.module.css" da sua pasta criada para poder modificar o CSS: 

    1. Identifique o container ou texto que deseja modificar a cor;

    2. Escreva o código da cor escolhida (Ex: #F1F1F1)

![Gif 9](https://i.imgur.com/rJqIbH9.gif)

8. Escolha e modifique as fontes da página

9. Para escolher as fontes: 

Escolha manualmente ou utilize sites que geram combinação de fontes automaticamente 

- [Font Pair](https://www.fontpair.co/): Essa plataforma do Google permite que o usuário escolha combinação de fontes de forma  facilitada e com fontes disponibilizadas pelo próprio Google.

- [Font Joy](https://fontjoy.com/): Plataforma que gera automaticamente combinações de fontes, sendo elas também disponibilizadas de forma gratuita. 

- _Dica_: Optar por duas ou três fontes para o layout da página, sempre mantendo um padrão entre elas (utilizar uma fonte apenas para títulos e uma fonte para o corpo da página)

10. Para definir no CSS as novas fontes: 

    1. Faça o import da fonte com o link gerado pelo Google "@import URL('link')"

![Fonts](https://i.imgur.com/Il1XBqt.gif)

    2. Identifique o "font-family" da parte que deseja modificar a fonte;

    3. Coloque o nome da fonte (ex: "Josefin Sans") e a família na qual ela pertence (ex: "serif-sans").

![Gif 8](https://i.imgur.com/COnnbDW.gif)
 
- **Como modificar as informações do "Quem Sou"**

1. Identifique a constante "const sobre" para poder modificar;

2. Escolha a imagem e/ou ilustração que será utilizada nessa sessão

3. Para escolher a imagem: 

Escolher ilustrações e imagens open-source através de sites online

    - [Many Pixels](https://www.manypixels.co/gallery): Acervo de ilustrações personalizáveis e open source

    - [Undraw](https://undraw.co/search): Acervo de ilustrações personalizáveis e open source

    - _Importante_: é necessário lembrar a importância de dar os devidos créditos para os artistas por trás das ilustrações e imagens utilizadas. 

4. Verifique se a imagem e/ou ilustração se encontra no formato "SVG", caso não esteja: 

    1. Entre no convertor online [aconvert](https://www.aconvert.com/image/png-to-svg/) e faça upload da imagem que deseja converter

    2. Clique no botão "Convert Now!"

    3. Clique no link gerado 

    4. Entre na guia aberta e faça o download manualmente da imagem que foi convertida

![Converter](https://i.imgur.com/AuOZBZb.gif)

5. Faça o upload da imagem diretamente do Gitpod: 

    1. Abra a pasta "static"

    2. Abra a pasta "membros-pag"

    3. Clique com o botão direito para fazer o upload da imagem 

![Upload](https://i.imgur.com/1UaDGRV.gif)

6. Em "imgFoto" colocar o caminho relativo da imagem desejada; 

3. Em "text" Adicione o seu texto de introdução.

![Gif 3](https://i.imgur.com/CmgGcSo.gif)

- **Como modificar as informações da "Formação Acadêmica"**

1. Identifique a constante " const estudos" para poder modificar;

2. Em "text1" adicione as suas informações da primeira coluna;

3. Em "text2" adicione as suas inforamções da segunda coluna;

4. Em "imgFoto" coloque o caminho relativo da sua ilustração escolhida.

![Gif 4](https://i.imgur.com/eENOo1k.gif)

- **Como modificar as informações de "Conhecimentos e interesses"**

_Título e Subtítulo_

1. Identifique a constante "const conhecimentos" para poder modificar;

2. Em "subtitle" escreva brevemente suas informações.

![Gif 5](https://i.imgur.com/t5b2Hxj.gif)

_Corpo do Texto_

3. Identifique a constante "const interesses" para poder modificar;

4. Em "imgFoto1", "imgFoto2", etc coloque o caminho relativo das suas ilustrações escolhidas;

5. Em "text", "text2" etc adicione informações sobre seus interesses;

![Gif 6](https://i.imgur.com/vZ6lM7i.gif)

- **Como modificar as informações do "Footer"**

1. Identifique a constante "const footer" para poder modificar;

2. Em "imgLogo", "imgLogo2" e "imgLogo3" coloque o caminho relativo da imagem que contém o logo da rede social que deseja colocar;

3. Em "link", "link2" e "link3" coloque a URL da rede social correspondente ao logo;

4. Em "text2" coloque o seu endereço de email.

![Gif 7](https://i.imgur.com/mYN5FoI.gif)
