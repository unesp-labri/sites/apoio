---
id: templates-ig
title: Como Utilizar os Templates do Instagram
sidebar_label: Templates Instagram
slug: /geral/comunicacao/templates-ig
---

:::caution

Os templates atualizados (2021/2022) do LabRI/UNESP já estão disponíveis na página do [Tutorial Figma](https://apoio.labriunesp.org/docs/geral/comunicacao/templates-figma)

:::

#### Como utilizar os templates para Instagram do LabRI

* Escolha o template na pasta do [LabRI](https://drive.google.com/drive/u/0/folders/1Lvln54QGno5WO3AOKJKhf0b7h_olg2XS)
* Faça o download do template escolhido
* Abra o template no site [Photopea](https://www.photopea.com/)

**Para Editar o Texto**:

![Passo 1](https://i.imgur.com/PfRl8no.png) 

    1. Encontre a camada correspondente da parte do texto que deseja modificar (As camadas sempre ficam na direita inferior da janela);

    2. Clique duas vezes na letra "T" no começo da camada para editá-la. 

**Para Editar algum Elemento da imagem**:

![Passo 2](https://i.imgur.com/B91awnA.png)

    1. Habilite a opção "Controles de Transformação/Transform Controls";

**Para modificar/adicionar uma imagem**

![Passo 3](https://i.imgur.com/zd36nrB.png)

    1. Abra a imagem no Photopea

![Passo 4](https://i.imgur.com/Ee4GF6u.png)

    2. Redimencione a imagem caso seja necessário 

![Passo 5](https://i.imgur.com/Cmlmv2a.png)

    3. Selecione a imagem e a arraste até o documento que está sendo editado (Template)

![Passo 6](https://i.imgur.com/kNVZgZh.png)

    4. Posicione a imagem no local desejado 

    5. Utilize a ferramenta "Controles de Transformação" para adequar a imagem ao template

![Gif Photopea](https://i.imgur.com/Aiy4eJE.gif)
