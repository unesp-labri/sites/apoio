---
id: tutorial-pagina
title: Utilização do Site LabRI
sidebar_label: Tutorial de Utilização das Páginas do LabRI
slug: /geral/comunicacao/tutorial-pagina
---

Nesta seção estão reunidos alguns tutoriais que devem auxiliar os colaboradores do LabRI/UNESP a utilizar e editar as páginas do Site LabRI. Os itens na lateral direita indicam os tutoriais que estão disponíveis atualmente. 

---

## **Web IDE**: Edição das Páginas

Aprenda a editar as páginas do site utilizando o Web IDE do GitLab.

:::tip 

É necessário estar logado em uma conta do [GitLab](https://gitlab.com) para seguir este tutorial.

:::

#### Entrar no Repositório do LabRI

1. Faça o login na sua conta do GitLab e entre no repositório do LabRI. 

2. Em seguida, entre na pasta "website".

![Selecionando o repositório do LabRI](https://i.imgur.com/EfxnfD8.gif)

#### Entrar no editor "Web IDE" 

1. Selecione a opção "Web IDE" para começar a editar as páginas do site.

2. Clique na opção escolhida para entrar na Web IDE.

![Selecionando a opção "Web IDE" para editar a página](https://i.imgur.com/xY6Qd54.gif)

#### Entrando na pasta da página que deseja editar

1. Entre na pasta "src" > "pages".

2. Encontre o nome da pasta que deseja editar (**Ex**: Cidades Sustentáveis).

![Selecionando pasta de edição](https://i.imgur.com/Kf03KbM.gif)

#### Editando informações 

O exemplo a seguir mostra como adicionar ou modificar informações de uma página do site LabRI.

:::tip 

Todas as informações que podem ser editadas estão guardadas dentro de uma constante (*const*), sendo necessário apenas identificar qual constante recebe as informações que você deseja modificar.

:::

1. Selecione o arquivo que deseja editar (**Ex**: "sobre.js").

2. Encontre a constante que irá receber as novas informações. 

3. Acrescente as informações que deseja. 

![Editando informações](https://i.imgur.com/glmerBE.gif)

#### Enviando as novas informações com um commit 

Para que as modificações sejam enviadas ao site é necessário fazer um commit.

1. Clique no botão "Create commit".

2. Selecione a opção "Commit to **main** branch".

3. Clique em "Commit". 

![Fazendo "commit" para enviar as informações novas](https://i.imgur.com/5N1Ynhg.gif)

---

## **Foto dos Membros**: Como editar e inserir imagens 

Edite sua foto com o template do Canva para ajudar a padronizar as imagens do site. 

:::tip 

É necessário estar logado em uma conta do [Canva](https://www.canva.com) para seguir este tutorial.

:::

#### Acessando o template

1. Entre no [Template do Canva](https://www.canva.com/design/DAFBQSemnxg/3RvBkpl9DPwPpVCvCMotZg/edit?utm_content=DAFBQSemnxg&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton) para ter acesso a edição da sua imagem. 

2. Duplique o template antes de adicionar a sua foto. 

![Duplicando o template](https://i.imgur.com/mJZkeHB.gif)

3. Faça o upload da sua foto. 

4. Insira sua foto no template. 

![Inserindo imagem](https://i.imgur.com/7vxoP1Q.gif)

#### Salvando a imagem 

1. Clique em "Compartilhar" > "Baixar". 

2. Selecione "Fundo Transparente". 

3. Selecione a página que deseja baixar. 

![Baixando imagem](https://i.imgur.com/a7iZNl3.gif)

#### Fazendo upload da imagem no GitLab

1. Acesse a pasta "static" > "img".

2. Encontre a pasta desejada e clique nos três pontos > "Upload Image". 

![Upload Imagem no Site](https://i.imgur.com/41yyEuq.gif)

#### Fazendo commit 

Para que as modificações sejam enviadas ao site é necessário fazer um commit.

1. Clique no botão "Create commit".

2. Selecione a opção "Commit to **main** branch".

3. Clique em "Commit". 

![Fazendo "commit" para enviar as informações novas](https://i.imgur.com/5N1Ynhg.gif)
