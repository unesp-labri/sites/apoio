---
id: edicao-trilha-dados
title: Edição da Trilha de Dados (Página)
sidebar_label: Tutorial de Edição da Trilha de Dados (Página)
slug: /geral/comunicacao/edicao-trilha-dados
---

Nesta seção encontram-se o tutoriais para a edição da página de Trilha de Dados que está disponível no site aberto do LabRI/UNESP. 

:::info

Para seguir os tutoriais será necessário:

- Ter uma conta no [GitLab](https://www.gitlab.com); 

- Acesso ao repositório do LabRI/UNESP no GitLab; 

- Ter uma conta no [Figma](https://www.figma.com) (Online ou Desktop).

:::

## Edição dos textos da Trilha (Paradas)

Atualmente a edição dos textos está sendo feita pelo Figma e depois eles são exportados para o site através de um arquivo SVG. 

:::note

O arquivo do Figma pode ser encontrando [aqui](https://www.figma.com/file/51fbq0YAfSXoPzxdaUKNV2/Trilha-de-Dados?node-id=0%3A1). Para editá-lo é necessário estar **conectado em uma conta do aplicativo Figma**

:::

### Passo 1 - Edição no Figma 

Primeiro acesse o documento do Figma e encontre o container que deseja modificar. 