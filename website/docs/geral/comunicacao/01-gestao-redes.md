---
id: gestao-redes
title: Gestão de Redes Sociais
sidebar_label: Gestão de Redes Sociais
slug: /geral/comunicacao/gestao-redes
---

## Gestão de Mídias Sociais 

- [Site aberto - labriunesp.org](https://labriunesp.org/)

- [Facebook](https://pt-br.facebook.com/labriunesp/)

- [Twitter](https://twitter.com/labriunesp)

- [LinkedIn](https://www.linkedin.com/company/labri-unesp-franca/)

- [Instagram](https://www.instagram.com/unesplabri/)

- [YouTube](https://www.youtube.com/channel/UCHx_m-4Cv7_ZLEDUe_wYATA/featured)

- [Logo do LabRI/UNESP e projetos integrados](https://drive.google.com/drive/u/1/folders/1tNrqZl0wLVZdEVMjdrCi2Vpzqrt6LZzM)

## Gestores de redes sociais

## Publer

- https://publer.io/dashboard#/

Software gratuito (com limitações) para programar e agendar posts

:::info 

Utilizar a conta do LabRI (Google) para entrar no Publer

:::

- Permite até 5 contas de redes sociais simultâneas
    1. Facebook
    1. Instagram
    1. LinkedIn
    1. Twitter
    1. YouTube
- Não é utilizável com posts já feitos anteriormente, só dá pra se usar com os posts feitos na plataforma do Publer
- Interface simples, com algumas funcionalidades 
    - Agendamento de posts
    - Agendamento automático, baseado no calendário do software
    - Posts recorrentes (repetição de posts diariamente, semanalmente, mensalmente, etc.)
    - Reciclagem de postagens (válido para as postagens feitas pelo Publer)

![Interface Padrão da Página de Criação](https://i.imgur.com/kgDe5CB.png)

- Adicionar mais contas
- Modo _Analytics_ (__pago__)

:::info

Utilizar o Publer apenas para postagens do **Twitter** e do **LinkedIn**. Para o **Facebook** e **Instagram** deve ser utilizado o **Meta Business Suite**. 

:::

#### Twitter 

- É possível fazer retweets colando o link do tweet na publicação. Desta forma, a integração entre as plataformas fará o retweet com comentário. Assim, __é possível fazer um retweet com comentário agendado através do Publer__.

![Retweet com comentário - Interface Publer](https://i.imgur.com/s12dHOW.png)
---
![Retweet com comentário - Preview](https://i.imgur.com/6spcczl.png)

### Estúdio de criação (Meta Business Suite) do Facebook e do Instagram

- [Link](https://business.facebook.com/creatorstudio/home)

Ferramenta para criação e edição de conteúdo do Facebook e do Instagram. Atualmente, está disponível para PC e Smartphones. 

- __Facebook__: trabalhando com postagens
    - Agendar
    - Editar
    - Criar
    - Pré-visualizações
    - Trabalhar com Stories, Testes de Publicação[^1], Lives (ao vivo) e etc.

- __Instagram__: trabalhando com postagens
    - Agendar
    - Editar
    - Criar
    - Pré-visualizações
    - Carroséis, IGTV, postagens simples e Stories

![Interface - Padrão](https://i.imgur.com/vmEc3ET.png)


![Interface - Padrão](https://i.imgur.com/1M2jGHj.png)

## Tutorial Publer 

Aprenda a utilizar o gestor de redes Publer. 

### Agendar postagem para o Twitter e o Linkedin 

1. Entrar na conta do Publer utilizando a conta do Google do LabRI;

![Imagem: Login no Publer](https://i.imgur.com/cB13U2d.png)

2. Selecionar a opção "Create";

3. Escrever o post na caixa de texto principal; 

4. Selecionar as redes sociais que deseja publicar o post; 

5. Clicar em "Schedule" e selecionar a data e horário que deseja publicar o post. 

![Gif: Visualização do Tutorial](https://i.imgur.com/vbzWxOm.gif)

## Tutorial Meta Business Suite

Aprenda a utilizar o gestor de redes Meta Business Suite 

### Agendar postagem para o Facebook e o Instagram 

1. Entrar no Meta Business Suite utilizando uma conta do Facebook que tenha acesso como administrador na página do LabRI no Facebook;

2. Entrar na aba "Planejador"; 

3. Selecionar o botão "Criar" > "Criar publicação"; 

![Gif: Visualização do Tutorial](https://i.imgur.com/5J0VHDl.gif)

4. Escreva o post;

5. Selecione as redes sociais que deseja publicar o post; 

6. Selecione "Horários Ideais" e escolha a data desejada; 

7. Selecione o botão "Programar publicação".

![Gif: Visualização do Tutorial](https://i.imgur.com/ZKd8Jxp.gif)

