---
id: tutorial-gif
title: Tutorial de Como Fazer Tutorial em Gifs
sidebar_label: Tutorial em Gif
slug: /geral/comunicacao/tutorial-gif
---

## Tutorial: Como fazer Tutorial em Gif 

### Sites e extensões utilizadas

1. [Screencastify](https://chrome.google.com/webstore/detail/screencastify-screen-vide/mmeijimgabbpbgpdklnllpncmdofkcpn): Extensão do google utilizada para gravar a tela e transformar em gif;

2. [Ezgif](https://ezgif.com/): Plataforma online de edição de Gifs;

3. [Imgur](https://imgur.com/): Plataforma online para publicar imagens/gifs.

### Como gravar a tela e gerar o Gif 

1. Instale a extensão Screencastify no Google Chrome;

2. Abra a extensão e configure as permissões necessárias;

3. Entre na tela/guia que deseja gravar e clique no logo da extensão no canto superior direito da tela;

4. Clique em "Browser tab" ou "Desktop" e pressione "Record" para começar a gravar;

![img print](https://i.imgur.com/zceCeot.png)

5. Grave automaticamente o que deseja e assim que terminar clique em "Pausar" e você será transferido para a visualização do vídeo;

6. Faça os ajustes necessários do tamanho do gif (cortar a duração) e clique para "Exportar como Gif".

![Tutorial 1](https://i.imgur.com/lZ6EKXM.gif)

### Como editar o Gif 

1. Abra o site Ezgif; 

2. Selecione a opção "Redimencionar" e faça o upload do gif; 

3. Clique na aba "cortar" e redimencione o gif da forma que desejar; 

4. Clique em "cortar" e depois no logo de "Salvar". 

![Tutorial 2](http://i.imgur.com/N2Dx5Jsh.gif)

### Como fazer o upload do Gif 

1. Entre no site Imgur;

2. Clique em 

1. Entre no site do Imgur e clique em "Criar novo post";

2. Faça o upload do gif; 

3. Clique em "Get share links";

4. Selecione e copie a opção "BBCode";

5. Escreva "![Nome](link copiado)" e apenas exclua "[img] e [/img]" do link copiado. 

![Tutorial 3](https://i.imgur.com/n9HVvD9.gif)
