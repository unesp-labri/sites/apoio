---
id: container
title: Container
sidebar_label: Container
slug: /geral/equipe/atual/rafael-almeida/container
---

- https://medium.com/@lets00/namespace-14c4e64d0559
- https://school.linuxtips.io/courses/enrolled/1259447
- https://github.com/badtuxx/DescomplicandoDocker


## Conceitos Importantes

- VMs X Containers
- Namespaces
  - PID: isolamendo dos processos
  - NET: isolamento das interfaces de rede
  - IPC: isolamento da comunicação entre processos e memória compartilhada
  - MNT: isolamento do sistema de arquivos / pontos de montagem
  - UTS: isolamento do kernel
- cgroups

## Instalação Docker


 - [Docker install -  via curl](https://github.com/docker/docker-install)

```
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
```

https://gitlab.com/rdealmeida/isite/-/blob/main/website/docs/estudar/infra/containerexpert/intro.md

### Adicionar user no grupo docker

```
usermod -aG docker <user_name>
```

## Principais comandos

- Imagem
- Container

### `docker run`

- cria um novo container e o executa

- Procura a imagem localmente -> Baixa a imagem caso não encontre localmente -> Valida o hash da imagem -> Executa o container.

`docker exec`

- permite executar um comando em um container que já está em execução

- envolvem o fluxo de inicialização e execução de comandos em containers, porém em contextos diferentes



```
docker container ls
docker container ls -a
docker container stop <id_container>
docker container start <id_container>
docker container pause <id_container>
docker container unpause <id_container>
docker container exec -it <id_container> bash
docker container exec 4277 ls
docker container rm <id_container>
docker container rm <id_container> --force # para e remove
docker run -d -P dockersamples/static-site
docker run -d -p 808:80  dockersamples/static-site
docker container port

```

```
docker container image ls
docker history <id_image>
docker inspect <id_image>
docker container image <id_image>


```


## Criar imagens

- Dockerfile


```
docker build -t rafaelrdealmeida/app-node:1.0 .
docker stop $(docker container ls -q)
docker tag teste/app-node:1.0 afaelrdealmeida/app-node:1.0

```

## Persistencia de dados

```
docker container rm $(docker container ls –aq) # remover todos os containers
docker rmi $(docker image ls –aq) --force # remover todos as imagens
docker container ls -s

```

## Bind mounts

```
docker run -it -v <caminho_host>:<caminho_container> ubuntu bash
docker run –it --mount type=bind,source=/home/daniel/volume-docker,target=/app ubuntu bash

```

### Volumes


```
docker volume ls
docker volume create meu-volume
cd /var/lib/docker
docker run -it -v meu-volume:/app ubuntu bash
docker run –it --mount source=meu-volume,target=/app ubuntu bash


```

### TMPFS

- escrita na memoria ram do host

```

docker run –it --tmpfs=/app ubuntu bash
docker run –it --mount type=tmpfs,destination=/app ubuntu bash


```

## Redes


### Bridge

```

docker run –it Ubuntu bash
docker inspect <id_container>
docker network ls
docker network create --driver bridge minha-bridge
docker run -it --name ubuntu1 --network minha-bridge ubuntu bash
docker run -d --name pong --network minha-bridge ubuntu sleep 1d

```
### None

a rede none remove a interface de rede.



### Host

A rede host remove o isolamento entre o container e o sistema.


### Comunicação entre app e banco

```
docker network ls
docker network create –driver bridge minha-bridge
docker run -d –network minha-bridge –name meu-mongo mongo:4.4.6
docker run -d –network minha-bridge –name alurabooks -p 3000:3000 aluradocker/alura-books:1.0

```

## Docker compose

- O Docker Compose irá resolver o problema de executar múltiplos containers de uma só vez e de maneira coordenada, evitando executar cada comando de execução individualmente.


- [Docker compose install](https://docs.docker.com/compose/install/)

```
 sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

```

```
docker-compose up -d
docker-compose ps
doxker-compose down

```

## Docker Multi-stage build

- https://cursos.alura.com.br/extra/alura-mais/usando-docker-multi-stage-build-para-otimizar-a-imagem-c74