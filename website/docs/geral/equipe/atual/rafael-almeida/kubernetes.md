https://school.linuxtips.io/courses/748679/lectures/27736288

- https://school.linuxtips.io/courses/1259521/lectures/28043293

- https://kubernetes.io/

- [GitHub - Descomplicando Kubernetes](https://github.com/badtuxx/DescomplicandoKubernetes)

- Borg: The Predecessor to Kubernetes

- escrito em Go

- Orquestração de containers

- Container run-time (execução)

- Papeis: 
  - master(1)
  - workers(2)

- schedule
- Pod- namespace ()
- controllers


## Conceitos gerais

- escalabilidade vertical
- escalabilidade horizontal


- Master ou controlplane (gerenciar o cluster)
  - api, c-m, sched,etcd
- nodes ou workers (executar as aplicações)
  - kubelet, k-proxy

- kubectl: criar, ler, atualizar e remover dados do cluster


## Instalação

- kubectl
- minikube

## PODs

- Um ou mais containers
- tem IPs proprios e os containers portas
- São efêmeros
- container no mesmo pod compartilham namespaces de rede e IPC; podem compartilhar volumes

```
minikube start
kubectl run ngix-pod --image=nginx:latest
kubectl get pods
kubectl get pods --watch
kubectl describe pod ngix-pod
kubectl edit pod ngix-pod


kubectl get pods 
kubectl delete pod ngix-pod
kubectl get pods 
kubectl apply -f primeiro-pod.yaml 
kubectl get pods 
kubectl get pods --watch
kubectl delete -f primeiro-pod.yaml 

```

## Services

SVC: ClusterIP, NodePort, LoadBalancer

- abstrações para expor aplicações executando em um ou mais pods
- proveem IPs fixos para comunicação
- proveem um DNS para um ou mais pds
- são capazes de fazer balanceamento de carga

### ClusterIP

- fazer a comunicação entre diferentes pods dentro de um mesmo cluster

### NodePort

- permitem a comunicação com o mundo externo



### LoadBalancer

- um ClusterIP que permite a comunicação entre uma maquina do mundo externo e os nosso pods

## ConfigMap

## Replica Set

- Tem a capacidade de encapsular um ou mais pods

```

kubectl get replicaset
kubectl get rs
kubectl get rs --watch
kubectl get rs
```

## Deployments

- auxiliam com controle de versionamento e criam um ReplicaSet automaticamente

```

kubectl get pods
kubectl apply -f nginx-deployments.yaml 
kubectl get pods
kubectl get rs
kubectl get deploy
kubectl get deployments
kubectl rollout history deployment nginx-deployment
kubectl apply -f nginx-deployments.yaml  --record
kubectl rollout history deployment nginx-deployment
kubectl annotate deployment nginx-deployment kubernetes.io/change-cause="Definido a imagem com versão latest"
kubectl rollout history deployment nginx-deployment
kubectl apply -f nginx-deployments.yaml  --record
kubectl rollout history deployment nginx-deployment
kubectl annotate deployment nginx-deployment kubernetes.io/change-cause="Definido a imagem com versão 1"
kubectl rollout history deployment nginx-deployment
kubectl rollout undo deployment nginx-deployment --to-revision=2
kubectl rollout history deployment nginx-deployment



159  kubectl apply -f portal-noticias-deployment.yaml 
  160  kubectl get pods --watch
  161  kubectl delete pod sistema-noticias
  162  kubectl apply -f sistema-noticias-deployment.yaml 
  163  kubectl get pods
  164  kubectl apply -f sistema-noticias-deployment.yaml 
  165  kubectl get pods
  166  kubectl delete pod db-noticias
  167  kubectl get pods
  168  kubectl delete pod sys-noticias
  169  kubectl get pods
  170  kubectl apply -f db-noticias-deployment.yaml 
  171  kubectl get pods

```

## Persistencia de dados

- volumes são independentes dos containers, mass dependentes dos pods

### hostpath