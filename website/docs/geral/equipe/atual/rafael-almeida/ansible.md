---
id: ansible
title: Ansible - Noções Gerais
sidebar_label: Ansible
slug: /geral/equipe/atual/rafael-almeida/ansible
---

- Inventário: list de máquinas que serão configuradas
- Playbook: é o arquivo com as "receitas de bolo" do que queremos fazer.
- roles

```mermaid

flowchart LR
    id1(Maquina de controle) --- |SSH| id2(Máquinas controladas)
```


## Instalação

[Instalaçao](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-ubuntu)

```
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt install ansible --yes

```

## Primeiros comandos

```
ansible -vvvv pc_itautec -u labri_adm -i hosts -m shell -a 'echo Hello, World'

```

- `-u`: usuário
- `-i`: inenvátio
- `-m`: modulo
- `-a`: argumentos

## Playbook

- o Ansible utiliza o princípio da Idempotência, com qual apenas irá alterar o resultado quando as operações forem efetivamente alteradas.

```


```