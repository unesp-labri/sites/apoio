---
id: intro
title: Rafael Almeida 
sidebar_label: Rafael
slug: /geral/equipe/atual/rafael-almeida/intro
---


```
git add . && git commit -m "estudos" && git pull origin main && git push origin main

```


```
cd website && yarn install && yarn start

```

```

https://archive.is/JMH6A
https://github.com/luizcarlosfaria/kb/wiki/Docker-no-Windows-vs-Docker-no-Linux
https://www.digitalocean.com/community/cheatsheets/getting-started-with-kubernetes-a-kubectl-cheat-sheet-pt
https://github.com/justmeandopensource/kubernetes/tree/master/rancher/rke

https://www.suse.com/c/rancher_blog/rancher-vs-rke-what-is-the-difference/
https://medium.com/@marcoscordeirojr/instala%C3%A7%C3%A3o-de-cluster-rancher-com-alta-disponibilidade-91582482c4f0


https://www.digitalocean.com/community/tutorials/understanding-nginx-http-proxying-load-balancing-buffering-and-caching

https://archive.is/6vZbz
https://rancher.com/docs/rancher/v2.6/en/installation/other-installation-methods/behind-proxy/launch-kubernetes/
https://rancher.com/docs/rancher/v2.0-v2.4/en/v1.6-migration/load-balancing/
https://rancher.com/docs/rancher/v2.6/en/installation/resources/k8s-tutorials/infrastructure-tutorials/nginx/
https://rancher.com/docs/rancher/v2.5/en/overview/architecture-recommendations/
https://rancher.com/docs/rke/latest/en/os/#opening-port-tcp-6443-using-iptables


https://www.macoratti.net/19/04/docker_contexec1.htm
https://github.com/alura-cursos/1846-kubernetes/blob/Aula6/portal-configmap.yaml
https://www.automateyournetwork.ca/uncategorized/introducing-mind-nmap-an-open-source-network-mind-map-tool/




```


## Ventoy


```

sudo bash Ventoy2Disk.sh -i /dev/sde
sudo mkdir -p /media/rafael/ventoy
lsblk
sudo mount /dev/sde1 /media/rafael/ventoy/


```


## PFsense

```
https://www.udemy.com/course/aprenda-o-firewall-pfsense-do-zero-ao-avancado/learn/lecture/6397484#overview
https://www.notion.so/org-geral/PROXMOX-08679a5144e24618a2f383739c309fbc
https://drive.mindmup.com/map/1vTiVIZ83BAaSYpTkeLbjVTLJKHToS9nP
https://gitlab.com/-/ide/project/unesp-labri/sites/apoio/tree/main/-/website/docs/projetos/sistemas/proxmox/02-howto.md/
https://kb.adentrocloud.com.br/knowledgebase/como-acessar-a-interface-web-do-pfsense-com-o-ip-de-wan/
https://www.laroberto.com/vlans-with-proxmox-and-pfsense/#:~:text=VLANs%20are%20a%20great%2C%20secure,without%20interference%20from%20each%20other.
https://forum.proxmox.com/threads/proxmox-pfsense-vlans-including-vms-with-two-nics.102385/
https://pve-user.pve.proxmox.narkive.com/1CTzLiVW/looking-for-recommendations-of-vlan-setup
https://getlabsdone.com/how-to-tag-vlan-on-proxmox-bridge-interface/
https://forum.proxmox.com/threads/pfsense-in-proxmox-and-vlans.100319/
https://forum.proxmox.com/threads/shared-lan-between-2-nodes.78016/
https://www.google.com/search?q=how+to+communicate+vlan+between+proxmox+nodes+pfsense&sourceid=chrome&ie=UTF-8
https://www.reddit.com/r/Proxmox/comments/t1q536/how_to_configure_proxmox_ve_networking_properly/
https://www.softsell.com.br/gateway-entenda-o-que-e-para-que-serve/#:~:text=Gateway%20funciona%20como%20uma%20esp%C3%A9cie,ou%20aplicativos%20instalados%20no%20celular.
https://translate.google.com.br/?hl=pt-BR&sl=en&tl=pt&text=encaminhar%20determinado&op=translate
https://forums.lawrencesystems.com/t/pfsense-to-nginx-routing/9637/7
https://www.udemy.com/course/haproxy-keepalived-alta-disponibilidade-para-linux/
http://tarefas.lantri.org/projects/wiki-01/wiki/06-07-proxmox-debian-buster
https://www.reddit.com/r/homelab/comments/imwcqr/home_lab_managed_switch_recommendations/
https://forums.lawrencesystems.com/t/best-core-switch-for-home-network-and-lab/10621
https://www.youtube.com/watch?v=y4Y2vletwzg
https://www.youtube.com/watch?v=IJts2RoHczE
https://www.youtube.com/watch?v=Q5l7VH6b5r4
https://www.youtube.com/watch?v=gVOEdt-BHDY
https://forum.proxmox.com/threads/pfsense-on-a-pve-cluster.84896/
https://www.hotmart.com/product/aprenda-configurar-um-active-directory-usando-nethserver/T52325559B
https://pve.proxmox.com/wiki/Network_Configuration#_masquerading_nat_with_tt_span_class_monospaced_iptables_span_tt
https://www.talos.dev/

```
