---
id: rancher
title: Rancher
sidebar_label: Rancher
slug: /geral/equipe/atual/rafael-almeida/rancher
---

- https://medium.com/@lets00/namespace-14c4e64d0559
- https://rancher.com/docs/rancher/v2.0-v2.4/en/installation/resources/advanced/helm2/

- [Recuperar senha de root linux](https://archive.is/8A2vF)
- [Desistalar Docker](https://archive.is/3fmAU)
- https://danielmiessler.com/study/manually-set-ip-linux/
- https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nfs-mount-on-ubuntu-20-04-pt
- https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/storage_administration_guide/s1-nfs-security




## Pré requisitos

### Instalar kubectl (rancher)

- https://rancher.com/docs/rancher/v2.6/en/faq/kubectl/

### Instalar Docker (rke e rancher)

https://rancher.com/docs/rancher/v2.6/en/installation/requirements/installing-docker/

```
curl https://releases.rancher.com/install-docker/20.10.sh | sh

```

 ### Adicionar user no grupo docker

```
usermod -aG docker <user_name>
```

### Desabilitar swap

- https://www.geeksforgeeks.org/how-to-permanently-disable-swap-in-linux/

```
swapoff -a
Remove any swap entry from /etc/fstab.
reboot

```
### Add in systemctl

```
echo 'net.bridge.bridge-nf-call-iptables=1' | sudo tee -a /etc/sysctl.conf

```

- https://rancher.com/docs/rke/latest/en/os/#flatcar-container-linux

```
systemctl enable docker.service
```

### Iptables

- https://rancher.com/docs/rke/latest/en/os/#opening-port-tcp-6443-using-iptables

```
iptables -A INPUT -p tcp --dport 6443 -j ACCEPT
```


- https://rancher.com/docs/rke/latest/en/os/#general-linux-requirements

### Ajuste ssh - /etc/ssh/sshd_config

- https://rancher.com/docs/rke/latest/en/os/#ssh-server-configuration

```
nano /etc/ssh/sshd_config
AllowTcpForwarding yes


```
### NGINX

## Instalação RKE

```
wget https://github.com/rancher/rke/releases/download/v1.3.8/rke_linux-amd64
mv rke_linux-amd64 rke
chmod +x rke
mv rke /usr/local/bin/rke
rke --version
```

## Instalar kubectl

- https://rancher.com/docs/rke/latest/en/os/#opening-port-tcp-6443-using-iptables

## Helm 

- https://helm.sh/pt/docs/intro/install/


```
curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash

```

## DNS

- https://www.tecmint.com/set-permanent-dns-nameservers-in-ubuntu-debian/


```
sudo apt install resolvconf
sudo systemctl status resolvconf.service
sudo nano /etc/resolvconf/resolv.conf.d/head
sudo systemctl restart resolvconf.service
reboot now
```

```
rke config --name cluster.yml
rke up

export=KUBECONFIG=kube_config_cluster.yml
```

## HA

- https://rancher.com/docs/rancher/v2.6/en/installation/install-rancher-on-k8s/#install-the-rancher-helm-chart

- https://rancher.com/docs/rancher/v2.6/en/installation/other-installation-methods/behind-proxy/install-rancher/

- https://rancher.com/docs/rancher/v2.6/en/installation/other-installation-methods/behind-proxy/install-rancher/

```
helm repo add rancher-latest https://releases.rancher.com/server-charts/latest

kubectl create namespace cattle-system

kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.5.1/cert-manager.crds.yaml

helm upgrade --install cert-manager jetstack/cert-manager \
  --namespace cert-manager --version v1.5.1 \
  --set http_proxy=http://${proxy_host} \
  --set https_proxy=http://${proxy_host} \
  --set no_proxy=127.0.0.0/8\\,10.0.0.0/8\\,cattle-system.svc\\,172.16.0.0/12\\,192.168.0.0/16\\,.svc\\,.cluster.local

kubectl get pods --namespace cert-manager


```

## Estudar

```
https://docs.docker.com/engine/security/rootless/
https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user
https://docs.docker.com/engine/security/#docker-daemon-attack-surface

https://austinsnerdythings.com/2021/09/01/how-to-deploy-vms-in-proxmox-with-terraform/
https://austinsnerdythings.com/2021/08/30/how-to-create-a-proxmox-ubuntu-cloud-init-image/
https://blog.dmcindoe.dev/posts/2021-07-31/automating-proxmox-with-ansible/
https://docs.technotim.live/posts/cloud-init-cloud-image/

```


