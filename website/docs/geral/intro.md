---
id: intro
title: Introdução
sidebar_label: Introdução
slug: /geral/intro
---

Nesta seção reunimos aspectos relativos a rotina do estagiário e demais colaboradores. Veja os itens no menu lateral para maiores detalhes

---

### Formulários de cadastro - LabRI/UNESP

Todo colaborador deve preencher os dois cadastros abaixo e, após o preenchimento, verificar se tem acesso aos itens apontados no __Checklist__ a seguir:


- [Cadastro - Novos colaboradores](https://forms.gle/e5CMfuCo88hAssK98)
- [Cadastro - Acesso estação remota de trabalho](https://forms.gle/pWC71gKdpeZTGradA)



- Checklist 
    - Psono 
    - Google Drive
    - Telegram
    - GitLab
    - Estação remota de trabalho 
    - Acesso ao discord 
    - Redes Sociais (Senhas armazenadas no Psono)


###  Treinamento para novos colaboradores

1. [Conhecimentos Gerais sobre o LabRI/UNESP](https://forms.gle/qEWgjPoEyQBeAMh1A)
1. [Gestão de atividades do LabRI/UNESP](https://forms.gle/CmaRU5wxkz16CmkFA)
1. [Documentação, Sites - LabRI/UNESP](https://forms.gle/5rdzWVajtGnkHF238)
1. [Tutorial acesso remoto](https://forms.gle/Z3kvbFsrnCpAmhfN7)
1. [Tutorial filezilla](https://forms.gle/CVipsNippw8WwPyj9)
1. [Tutorial pos processamento](https://forms.gle/cZ6cwmH5QnsCeHnN7)
1. [Tutorial recoll](https://forms.gle/EapVEEU6q1FEv5KW7)







