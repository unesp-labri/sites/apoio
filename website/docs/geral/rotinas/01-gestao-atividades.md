---
id: gestao-atividades
title: Programas e Serviços de Gestão de atividades
sidebar_label: Gestão de Atividades
slug: /geral/rotinas/gestao-atividades
---


O LabRI/UNESP utiliza as seguintes plataformas/tecnologias para a gestão de suas atividades:

- [__Notion__](https://www.notion.so/Bem-vindo-ao-LabRI-42b2ae426089479ab832af4f53e6e800): É um serviço utilizado pelo LabRI para gerir as tarefas, atividades e prazos;
- [__Psono__](https://chrome.google.com/webstore/detail/psono-free-password-manag/eljmjmgjkbmpmfljlmklcfineebidmlo): Ferramenta de gerenciamento de senhas. A partir dele que compartilhamos as senhas gerais do LabRI/UNESP (redes sociais, entre outros);
- [__Google Drive__](https://drive.google.com/drive/u/1/folders/0BzDme5Ht7oi2dWgzTWlPNUxiQUk?resourcekey=0-28syf5WpK5lNcYrbQDegYQ): Armazena dados como imagens, documentos e etc. O drive do LabRI/UNESP possui uma estrutura de pastas que deve ser mantida para a organização geral das atividades;
- [__GitLab__](https://gitlab.com/unesp-labri): É um serviço voltado para armazenar, compartilhar e gerenciar códigos;
- __Telegram__: É utilizado para a troca de mensagens entre os colaboradores e o coordenador do LabRI/UNESP;
- [__Discord__](https://discord.gg/cbGunzS2): É utilizado como principal meio de interação entre os colaboradores do LabRI/UNESP. Durante o período em que determinado colaborador está realizando suas atividades ligadas ao LabRI/UNESP, normalmente ele também está disponível ou trabalhando em conjunto em alguma das salas do Discord;
- __Docusaurus__: Utilizado para a construção dos sites do LabRI/UNESP, bem como para o armazenamento de nossa documentação. Possuímos dois sites, um [aberto](http://labriunesp.org) e outro [restrito](https://apoio.labriunesp.org)
- [__Google Meet__](https://meet.google.com/poe-qsad-zvs): Utilizado como sala virtual para reuniões eventuais.

### Como utilizar o Notion

- O foco principal da utilização do Notion é para gerir as tarefas desenvolvidas no LabRI/UNESP.
- O Notion possui três divisões principais: 
    - Sobre o LabRI
    - Gestão de Atividades
    - Apoio à Equipe

#### Sobre o LabRI

Reúne uma apresentação geral sobre o LabRI/UNESP e há uma agenda de atividades

#### Apoio à Equipe

Centraliza links para informações importantes no cotidiano do trabalho do LabRI/UNESP

#### Gestão de Atividades

Principal recurso utilizado no Notion. Reúne a gestão das tarefas e dos projetos que o LabRI/UNESP desenvolve

##### Categorias e status das tarefas 

- O fluxo de trabalho se baseia em três grandes conjuntos de categorias de tarefas (visualizações do Notion): __Inseridas__, __Em andamento__ e __Finalizadas__.

##### Localização das categorias das tarefas 

- Na imagem a seguir, é indicado a localização dessas três categorias, a partir das quais estão reunidos os status das tarefas.

![Views](https://i.imgur.com/5WultAA.png)

- A página indicada na imagem acima pode ser acessada através da [Home do LabRI/UNESP no Notion](https://www.notion.so/Bem-vindo-ao-LabRI-42b2ae426089479ab832af4f53e6e800), clicando no item "LabRI - Tarefas"


##### Relação entre as categorias e os status das tarefas
---

![](https://i.imgur.com/8OaBl9G.png)

##### Significado de cada status das tarefas 

|Categoria|Status|Descrição|
|---|---|---|
|Inserida|Não iniciada|Não há um responsável, a tarefa precisa ser priorizada[^1] e não há uma definição de data prevista para entrega|
|Inserida|Ideia|Não há um responsável, é uma possibilidade futura que ainda não é prioritária. Não há uma data prevista para entrega|
|Inserida|Planejada|Há um responsável mas a tarefa não foi priorizada[^1]|
|Inserida|Demanda|Não há um responsável mas é um pedido não priorizado[^1] de docente, discente ou grupo de pesquisa|
|Em andamento|Em progresso|A tarefa foi iniciada. Há responsável(is) e definições do que deve ser feito|
|Em andamento|Esperando|A tarefa foi iniciada mas paralisada por enquanto, aguardando a iniciativa de algum outro membro ou de terceiros|
|Finalizada*|Concluída|A tarefa foi concluída pelo responsável e aguarda que outro membro analise e sugira ajustes, caso necessário|
|Finalizada|Arquivada|A tarefa já foi concluída e revisada, podendo assim ser encerrada|
|Finalizada|Cancelada|A tarefa foi encerrada previamente por algum motivo|


#### A imagem a seguir indica as principais informações que devem constar em cada tarefa 

--- 

![Colunas](https://i.imgur.com/WCO4dYI.png)




- Você pode duplicar uma tarefa existente e ajustar as informações necessárias. Isso evita a necessidade do preenchimento de todas as informações nas colunas indicadas na imagem acima  
- Caso a tarefa seja muito grande, é aconselhável criar uma tarefa-pai e vincular a ela sub-tarefas (tarefas-filhas) que sejam concluídas dentro de uma semana
- É importante buscar definir datas para as tarefas 
- Quando a tarefa estiver com status de `Concluída`, necessariamente preencha o campo de data
- Quando a tarefa estiver com status de `Concluída`, um outro membro da equipe deve revisar a tarefa, verificando se todos os campos foram preenchidos adequadamente, além de avaliar se os objetivos da tarefa foram alcançados. Se necessário, ao revisar a tarefa, indique os ajustes que julgar necessários.
- Após a finalização da revisão da tarefa concluída, mudar o status para `Arquivada`. É importante que, ao arquivar a tarefa, todas as colunas indicadas acima estejam preenchidas.



### Como utilizar o Psono

- [Acesse este vídeo](https://youtu.be/OHiICe_QEeg)

### Como utilizar o GitLab



### Como utilizar o Google Drive

- [Acesse aqui](https://apoio.labriunesp.org/docs/geral/rotinas/google-drive)

### Como utilizar os Sites do LabRI/UNESP

- [Acesse aqui](https://apoio.labriunesp.org/docs/projetos/sistemas/dev/sites/intro)

### Notas de fim
[^1]: Tarefa priorizada significa que houve uma triagem e uma definição do responsável, de datas de entrega e do nível de prioridade (Alto, Médio ou Baixo)

