---
id: comunicacao
title: Comunicação
sidebar_label: Comunicação
slug: /geral/rotinas/comunicacao
---

## Gestão de Mídias Sociais 

- [Site aberto - labriunesp.org](https://labriunesp.org/)

- [Facebook](https://pt-br.facebook.com/labriunesp/)

- [Twitter](https://twitter.com/labriunesp)

- [LinkedIn](https://www.linkedin.com/company/labri-unesp-franca/)

- [Instagram](https://www.instagram.com/unesplabri/)

- [YouTube](https://www.youtube.com/channel/UCHx_m-4Cv7_ZLEDUe_wYATA/featured)

- [Logo do LabRI/UNESP e projetos integrados](https://drive.google.com/drive/u/1/folders/1tNrqZl0wLVZdEVMjdrCi2Vpzqrt6LZzM)

## Gestores de redes sociais

### Publer

- https://publer.io/dashboard#/

Software gratuito (com limitações) para programar e agendar posts

- Permite até 5 contas de redes sociais simultâneas
    1. Facebook
    1. Instagram
    1. LinkedIn
    1. Twitter
    1. YouTube
- Não é utilizável com posts já feitos anteriormente, só dá pra se usar com os posts feitos na plataforma do Publer
- Interface simples, com algumas funcionalidades 
    - Agendamento de posts
    - Agendamento automático, baseado no calendário do software
    - Posts recorrentes (repetição de posts diariamente, semanalmente, mensalmente, etc.)
    - Reciclagem de postagens (válido para as postagens feitas pelo Publer)

![Interface Padrão da Página de Criação](https://i.imgur.com/kgDe5CB.png)

- Adicionar mais contas
- Modo _Analytics_ (__pago__)

#### Twitter 

- É possível fazer retweets colando o link do tweet na publicação. Desta forma, a integração entre as plataformas fará o retweet com comentário. Assim, __é possível fazer um retweet com comentário agendado através do Publer__.

![Retweet com comentário - Interface Publer](https://i.imgur.com/s12dHOW.png)
---
![Retweet com comentário - Preview](https://i.imgur.com/6spcczl.png)

### Estúdio de criação (Creator Studio) do Facebook

- [Link](https://business.facebook.com/creatorstudio/home)

Ferramenta para criação e edição de conteúdo do Facebook. Atualmente, está disponível para PC e Smartphones. 

- __Facebook__: trabalhando com postagens
    - Agendar
    - Editar
    - Criar
    - Pré-visualizações
    - Trabalhar com Stories, Testes de Publicação[^1], Lives (ao vivo) e etc.

![Interface - Padrão](https://i.imgur.com/s059Z1x.png)

- __Instagram__: trabalhando com postagens
    - Criar 
    - Editar
    - Agendar
    - Somente é possível trabalhar com carroséis, IGTV e postagens simples. __Não é possível trabalhar com Stories ainda__ 

![Interface - Padrão](https://i.imgur.com/VxnR9Um.png)

### Redes Sociais 

#### Como utilizar os templates para Instagram do LabRI

* Escolha o template na pasta do [LabRI](https://drive.google.com/drive/u/0/folders/1Lvln54QGno5WO3AOKJKhf0b7h_olg2XS)
* Faça o download do template escolhido
* Abra o template no site [Photopea](https://www.photopea.com/)

**Para Editar o Texto**:

![Passo 1](https://i.imgur.com/PfRl8no.png) 

    1. Encontre a camada correspondente da parte do texto que deseja modificar (As camadas sempre ficam na direita inferior da janela);

    2. Clique duas vezes na letra "T" no começo da camada para editá-la. 

**Para Editar algum Elemento da imagem**:

![Passo 2](https://i.imgur.com/B91awnA.png)

    1. Habilite a opção "Controles de Transformação/Transform Controls";

**Para modificar/adicionar uma imagem**

![Passo 3](https://i.imgur.com/zd36nrB.png)

    1. Abra a imagem no Photopea

![Passo 4](https://i.imgur.com/Ee4GF6u.png)

    2. Redimencione a imagem caso seja necessário 

![Passo 5](https://i.imgur.com/Cmlmv2a.png)

    3. Selecione a imagem e a arraste até o documento que está sendo editado (Template)

![Passo 6](https://i.imgur.com/kNVZgZh.png)

    4. Posicione a imagem no local desejado 

    5. Utilize a ferramenta "Controles de Transformação" para adequar a imagem ao template

![Gif Photopea](https://i.imgur.com/Aiy4eJE.gif)

## Como costumizar a Página de Equipe

### Escolha da Paleta de Cores

Utilizar sites que geram paletas de cores automaticamente 

- [Coolors](https://coolors.co/): Ferramenta que permite criar paletas de cores de maneira personalizada, além de permitir salvar e/ou arquivar uma paleta de cor que já foi criada. 

- Outras ferramentas similares: [Colormind](http://colormind.io/) e [Adobe Color](https://color.adobe.com/create)

-  _Dica_: Optar por uma paleta de cor que não possua muitos contrastes ou cores muito quentes.

### Escolha de Fontes 

Escolher manualmente ou utilizar sites que geram combinação de fontes automaticamente 

- [Font Pair](https://www.fontpair.co/): Essa plataforma do Google permite que o usuário escolha combinação de fontes de forma  facilitada e com fontes disponibilizadas pelo próprio Google.

- [Font Joy](https://fontjoy.com/): Plataforma que gera automaticamente combinações de fontes, sendo elas também disponibilizadas de forma gratuita. 

- _Dica_: Optar por duas ou três fontes para o layout da página, sempre mantendo um padrão entre elas (utilizar uma fonte apenas para títulos e uma fonte para o corpo da página)

Para utilizar as fontes escolhidas, é necessário indicar o nome e a família no CSS (adicionar print de como fazer)

### Escolha de Ilustrações e Imagens 

Escolher ilustrações e imagens open-source através de sites online

- [Many Pixels](https://www.manypixels.co/gallery): Acervo de ilustrações personalizáveis e open source

- [Undraw](https://undraw.co/search): Acervo de ilustrações personalizáveis e open source

- _Importante_: é necessário lembrar a importância de dar os devidos créditos para os artistas por trás das ilustrações e imagens utilizadas. 

### Modificar Fontes e Cores da página

Para modificar as informações da página de equipe é necessário fazer um Git Clone para clonar o repositório

- **Como modificar as fontes**

1. Identifique o "font-family" da parte que deseja modificar a senha;

2. Coloque o nome da fonte (ex: "Josefin Sans") e a família na qual ela pertence (ex: "serif-sans").

![Gif 8](https://i.imgur.com/COnnbDW.gif)

- **Como modificar as cores da página**

1. Identifique o container ou texto que deseja modificar a cor;

2. Escreva o código da cor escolhida (Ex: #F1F1F1)

![Gif 9](https://i.imgur.com/rJqIbH9.gif)

### Inserir informações na página 

Para modificar as informações da página de equipe, é necessário fazer um Git Clone para clonar o repositório.

- **Como modificar as informações da header**

1. Identifique a função principal do código "function Equipe()";

2. Modifique o nome da página em "Página Membros" no "Layout title";

![Gif 1](https://i.imgur.com/OApNDWm.gif)

3. Para modificar o título da header modifique o "Nome do Membro" no "hero__title" com o seu nome;

4. Para modificar o subtítulo da header modifique a "Função dentro do LabRI" no "hero__subtitle";

![Gif 2](https://i.imgur.com/opU55gt.gif)
 
- **Como modificar as informações do "Quem Sou"**

1. Identifique a constante "const sobre" para poder modificar;

2. Em "imgFoto" colocar o caminho relativo da imagem desejada; 

3. Em "text" Adicione o seu texto de introdução.

![Gif 3](https://i.imgur.com/CmgGcSo.gif)

- **Como modificar as informações da "Formação Acadêmica"**

1. Identifique a constante " const estudos" para poder modificar;

2. Em "text1" adicione as suas informações da primeira coluna;

3. Em "text2" adicione as suas inforamções da segunda coluna;

4. Em "imgFoto" coloque o caminho relativo da sua ilustração escolhida.

![Gif 4](https://i.imgur.com/eENOo1k.gif)

- **Como modificar as informações de "Conhecimentos e interesses"**

_Título e Subtítulo_

1. Identifique a constante "const conhecimentos" para poder modificar;

2. Em "subtitle" escreva brevemente suas informações.

![Gif 5](https://i.imgur.com/t5b2Hxj.gif)

_Corpo do Texto_

3. Identifique a constante "const interesses" para poder modificar;

4. Em "imgFoto1", "imgFoto2", etc coloque o caminho relativo das suas ilustrações escolhidas;

5. Em "text", "text2" etc adicione informações sobre seus interesses;

![Gif 6](https://i.imgur.com/vZ6lM7i.gif)

- **Como modificar as informações do "Footer"**

1. Identifique a constante "const footer" para poder modificar;

2. Em "imgLogo", "imgLogo2" e "imgLogo3" coloque o caminho relativo da imagem que contém o logo da rede social que deseja colocar;

3. Em "link", "link2" e "link3" coloque a URL da rede social correspondente ao logo;

4. Em "text2" coloque o seu endereço de email.

![Gif 7](https://i.imgur.com/mYN5FoI.gif)

#### Notas

[^1]:Teste diferentes versões de sua publicação de vídeo com o Teste de publicação. Depois, publique o vídeo com o melhor desempenho em visualizações ou em qualquer métrica importante para você. Testes frequentes podem ajudar você a saber o que seu público está procurando.
