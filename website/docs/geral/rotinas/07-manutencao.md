---
id: manutencao
title: Manutenção - Infra
sidebar_label: Manutenção - Infra
slug: /geral/rotinas/manutencao
---


## Atualização dos Computadores do LabRI

Uma das principais funções do estagiário é realizar semanalmente a atualização dos computadores. Essa tarefa é realizada toda segunda feira. Para tanto, deve-se ligar os computadores através do WakeOnLan, acessar os clusters SSH instalados na máquina principal do laboratório e realizadas a atualização.

Acesse o Tutorial de como usar o Wake On Lan: [Tutorial](http://tarefas.lantri.org/projects/wiki-01/wiki/06-06-wakeonlan)

Acesse o Tutorial de como usar os Clusters SSH: [Tutorial](http://tarefas.lantri.org/projects/wiki-01/wiki/06-05-clusterssh)

Acesse o Tutorial de Atualização: [Tutorial](http://tarefas.lantri.org/projects/lantri-39/wiki/AtualizarPCs02)

Além disso, há uma [tarefa](http://tarefas.lantri.org/issues/2131) que deve ser atualizada colocando o tempo necessário para sua realização e então deslocar a sua data de execução para a próxima segunda-feira.
