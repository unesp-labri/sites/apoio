---
id: google-drive
title: Google Drive
sidebar_label: Google Drive
slug: /geral/rotinas/google-drive
---

## Organização do Google Drive 

Concentra os principais documentos do LabRI (como planilhas, tutoriais escritos, notas fiscais dos equipamentos, imagens dos equipamentos etc.) organizados de uma maneira prática para acesso e gerenciamento. A disposição dos documentos estão concentrados dentro de uma pasta chamada [LabRI](https://drive.google.com/drive/folders/0BzDme5Ht7oi2dWgzTWlPNUxiQUk?usp=sharing), que centraliza todos os documentos utilizados dividindo-os em subpastas, separado por temas:
- [01-Digitalização](https://drive.google.com/drive/folders/0BzDme5Ht7oi2N2Z6U2daQ3dEdWs?usp=sharing)
Concentra, a princípio os livros digitalizados a pedido dos professores, assim, utiliza-se essa pasta para compartilhar com os professores o arquivo final.

- [02-Documentos de texto](https://drive.google.com/drive/folders/0BzDme5Ht7oi2U1EtMXBPQlpTbG8?usp=sharing)
Reúne todos os documentos de texto (DOCS) produzido pelo LabRI. Alguns exemplos: atividades do estagiário, edital de estágio, mensagens padrão para e-mails, cursos, relatório anual de estágio, entre outros.
  - [Mensagens - registros](https://docs.google.com/document/d/1yGJB_F1FYoli2JdYpMMAu4x7_xSCNFXa3zbNjJbe7r8/edit?usp=sharing)
Reúne mensagens de e-mails diversos e chamados de manutenção e/ou suporte ao DTI que servem de modelo para uso futuro.
  - [Relatório Anual de Estágio 2017](https://docs.google.com/document/d/1Kp2nLmu6BmvjgbtBBWNI1j8sgKUn7EBgC8YBQbvJ7Ro/edit?usp=sharing)
O relatório mostra as atividades feitas, aquisições de equipamentos, como o LabRI tem sido usado pelos grupos de extensão, gestão de dados e histórico de estagiários.

- [03-Planilhas e formulários](https://drive.google.com/drive/folders/0BzDme5Ht7oi2Q2JRajRxdzROMkk?usp=sharing)
Reúne todas as planilhas e formulários produzidos pelo LabRI acerca de conteúdo áudio visual, bases de dados, formulários do edital de estágio, inventário, lista de tutoriais, entre outros. As principais planilhas podem ser acessadas diretamente a seguir:
  - [BD-Bases com acesso remoto e verificação de funcionamento](https://docs.google.com/spreadsheets/d/16kI70m_XoBwC8n5kWdXh4H3eT0szssv5buf69dErp-U/edit?usp=sharing)
Indica o acesso às bases de dados para pesquisa e a verificação de funcionamento da base de dados.
  - [BD-Bases de dados - Listagem Geral-02](https://docs.google.com/spreadsheets/d/1BnlB7z7NEiKuMPgwJTbC1XkxFcj_8DfO7nqTJqMAvOc/edit?usp=sharing)
Contém todas as informações detalhadas das bases de dados utilizadas pelo LANTRI
  - [Conteúdo Áudio-Visual do Labri](https://docs.google.com/spreadsheets/d/1uBbjnlGH38MyYzubGhQ7Zd137nzYIJE8cch9C-PnYSE/edit?usp=sharing)
filmes e vídeos presentes no LabRI
  - [Glossário](https://docs.google.com/spreadsheets/d/1kPOyWGRSfpSXvJbckP4UHtSnRSwlP-Be_vsbQLDF6uY/edit?usp=sharing)
Glossário de palavras utilizadas pelo LabRI os quais apresentamos seus significados.
  - [Lista de tutoriais e vídeos do Youtube](https://docs.google.com/spreadsheets/d/1v-cZF9y9Z3VGzCspLwmsQ9sWaKdZUoHaSrhmxuqiH6k/edit?usp=sharing)
Vídeos contidos no "canal do LabRI":https://www.youtube.com/channel/UCHx_m-4Cv7_ZLEDUe_wYATA no Youtube
  - [Livros, Teses e Artigos soltos](https://docs.google.com/spreadsheets/d/1E7TehGhd3ays7esqShP7l1pXNWc8bzSovm9yggWECTs/edit?usp=sharing)
Tabela dos scanneamentos feitos no LabRI
  - [Tempo de processamento - Livros digitalizados](https://docs.google.com/spreadsheets/d/1BPB6fwxIY3BZwqcGGqsOQ0APk9_SCzdCi1QyoVNgBCY/edit#gid=0)
Tabela que indica o tempo de processamento dos livros, desde a digitalização até a transformação em pdf pesquisável
  - [Inventário Utensílios LabRI](https://docs.google.com/spreadsheets/d/1wN2t-nQgQ9kB5eGdHSpSfczbhlyDg1z9Ee8aeqvbmo8/edit?usp=sharing)
Itens pertencentes ao LabRI presentes nos armários

- [04-Tutoriais](https://drive.google.com/drive/folders/0BzDme5Ht7oi2VU5iMEctUlkzbEk?usp=sharing)
Concentra todos os tutoriais feitos pelo LabRI na forma escrita (PDF/Docs). Os principais tutoriais podem ser acessados pela área de Documentação > Tutoriais

- [05-Mapas mentais](https://drive.google.com/drive/folders/0BzDme5Ht7oi2eEl5VGxtR1hVMnc?usp=sharing)
Encontra-se todos os mapas mentais (.mm) feitos pelo LabRI

- [06-Diagramas](https://drive.google.com/drive/folders/0BzDme5Ht7oi2cVdnY2tnZ1Q1eGc?usp=sharing)
Reúne todos os diagramas elaborados pelo LabRI, como o [MAPA de computadores do Labri](https://drive.google.com/file/d/0BzDme5Ht7oi2WWRTWmpfY1g1ckU/view?usp=sharing)

- [07-Arquivos em PDF](https://drive.google.com/drive/folders/0BzDme5Ht7oi2RG1WaFgyMXAwS28?usp=sharing)
Concentra os documentos em PDF criados pelo LabRI.

- [08-Semana de Relações Internacionais](https://drive.google.com/drive/folders/0BzDme5Ht7oi2SFBjck9ubFV6dms?usp=sharing)
Contém arquivos referentes à Semana de Relações Internacionais na Unesp-Franca.

- [09-Imagens](https://drive.google.com/drive/folders/0BzDme5Ht7oi2Vng1dGZQeXVnXzg?usp=sharing)
Concentra fotos de equipamentos, imagens de eventos e banners. Dentro da subpasta [01-Fotos de Equipamentos](https://drive.google.com/drive/folders/0BzDme5Ht7oi2ZkFTQmRZbzlodkU?usp=sharing) encontra-se outras subpastas que reúnem as fotos dos computadores e servidores, mais as fotos dos equipamentos do LabRI, que compõe o inventário do Laboratório.

- [10-Notas Ficais de Equipamentos](https://drive.google.com/drive/folders/0BzDme5Ht7oi2X3lHeTRsdVdoYzg?usp=sharing)
Reúne as Notas Fiscais dos equipamentos adquiridos pelo LabRI/LANTRI.

- [11-arquivos anexos ao DTI](https://drive.google.com/drive/folders/0BzDme5Ht7oi2Z09qWUoxRWlOOWM?usp=sharing)
Concentra arquivos zipados que foram enviados em anexo aos chamados de manutenção solicitados ao DTI. O acesso ao portal do DTI é [suporte.franca.unesp.br](https://suporte.franca.unesp.br/)

- [12-Manuais de equipamentos](https://drive.google.com/drive/folders/1ZfRlshMtzhXSH2_hxWIQhD7eZkvDLggA?usp=sharing)
Reúne os manuais de computadores, servidores e outros equipamentos do LabRI.

- [13-Atividades de Estagiário](https://drive.google.com/drive/folders/1gHeF8AeOpiPJiKtx6svCxpwVaaHKYP3L?usp=sharing)
Concentra os projetos realizados pelos estagiários voluntários do LabRI. Na subpasta [2017-Jaqueline e Norberto](https://drive.google.com/drive/folders/0B407ouo3y2S8ajVYZVhyMmFwdlk?usp=sharing), por exemplo, encontrar-se-á documentos levantados e minicursos realizados.

- [PASTA PROVISÓRIA (ZIP/EXE)](https://drive.google.com/drive/folders/0BzDme5Ht7oi2YnJiTzduMlYxSVk?usp=sharing)
Concentra arquivos diversos (.exe, .zip., .bat etc.) utilizados ou criados pelo LabRI

|Pasta|Descrição|
|---|---|
|__01-Digitalização__|Concentra, a princípio os livros digitalizados a pedido dos professores, assim, utiliza-se essa pasta para compartilhar com os professores o arquivo final.|
|__02-Documentos de Texto__|Reúne todos os documentos de texto (DOCS) produzido pelo LabRI. Alguns exemplos: atividades do estagiário, edital de estágio, mensagens padrão para e-mails, cursos, relatório anual de estágio, entre outros.|
|__03-Planilhas e Formulários__|Reúne todas as planilhas e formulários produzidos pelo LabRI acerca de conteúdo áudio visual, bases de dados, formulários do edital de estágio, inventário, lista de tutoriais, entre outros|
|__04-Tutoriais__|Concentra todos os tutoriais feitos pelo LabRI na forma escrita (PDF/Docs). Os principais tutoriais podem ser acessados pela área de `Documentação > Tutoriais`|
|__05-Mapas mentais__|Encontra-se todos os mapas mentais (`.mm`) feitos pelo LabRI|
|__06-Diagramas__|Reúne todos os diagramas elaborados pelo LabRI, como o MAPA de computadores do Labri|
|__07-Arquivos em PDF__|Concentra os documentos em PDF criados pelo LabRI.|
|__08-Semana das Relações Internacionais__|Contém arquivos referentes à Semana de Relações Internacionais na Unesp-Franca.|
|__09-Imagens__|Concentra fotos de equipamentos, imagens de eventos e banners. Dentro da subpasta 01-Fotos de Equipamentos encontra-se outras subpastas que reúnem as fotos dos computadores e servidores, mais as fotos dos equipamentos do LabRI, que compõe o inventário do Laboratório.|
|__10-Notas Fiscais de Equipamentos__|Reúne as Notas Fiscais dos equipamentos adquiridos pelo LabRI/LANTRI.|
|__11-Arquivos anexos ao DTI__|Concentra arquivos zipados que foram enviados em anexo aos chamados de manutenção solicitados ao DTI. O acesso ao portal do DTI é suporte.franca.unesp.br|
|__12-Manuais de Equipamentos__|Reúne os manuais de computadores, servidores e outros equipamentos do LabRI|
|__13-Atividades de Estagiário__|Concentra os projetos realizados pelos estagiários voluntários do LabRI.|
|__14-Testes realizados__|Armazena dados referentes a testes realizados (como testes de memória, compatibilidade, etc.)|
|__15-Seleção-Estágio__|Contém material referente aos processos de admissão dos estagiários do LabRI|
|__16-Projetos LabRI__|Econtram-se arquivos referentes aos projetos ligados ao LabRI, que podem ser compartilhados externamente com outros colaboradores do LabRI|
|__17-Certificados__|Reúne os certificados emitidos pelo LabRI|
|__18-Logo-Labri__|Concentra os logotipos e imagens utilizadas pelo LabRI|
|__19-Grupoo-de-pesquisa__|Uma pasta que pode ser compartilhada, onde se armazena arquivos interessantes aos grupos de pesquisa|



