---
id: contatos-unesp-franca
title: Contatos Unesp Franca
sidebar_label: Contatos - Setores UNESP
slug: /geral/rotinas/contatos-unesp-franca
---

## Contatos do Setores e departamentos da UNESP-Franca

### DERI - Departamento de Relações Internacionais

*[Página Web](https://www.franca.unesp.br/#!/departamentos/relacoes-internacionais/)*

Endereço: Administração I - Sala 05

Telefone: *(16) 3706-8891*

E-mail: *deri.franca@unesp.br*

----

### DTI - Diretoria Técnica de Informatica

*[Página Web](https://www.franca.unesp.br/#!/dti)*

Endereço: Bloco 3 - Sala STI07

Telefone: *(16) 3706-8780*

E-mail: *dti.franca@unesp.br*

Expediente: 08h00 - 21h00

Para o +requerimento de serviços+ do DTI - como manutenção de máquinas - é necessário utilizar a seguinte plataforma:

*[Suporte DTI](https://suporte.franca.unesp.br/)*

**_Caso não tenha um login, entre em contato com o DTI para criar o seu._**

----

### STAEPE - Seção Técnica de Apoio ao Ensino, Pesquisa e Extensão

*[Página Web](https://www.franca.unesp.br/#!/administracao/divisao-tecnica-academica/secao-tecnica-de-apoio-ao-ensino-pesquisa-e-extensao/)*

Endereço: Bloco 1

Telefone: *(16) 3706-8758*

E-mail: *staepe.franca@unesp.br*

Expediente: 07h00 - 23h00

----

### SCM - Seção de Conservação e Manutenção

*[Página Web](https://www.franca.unesp.br/#!/administracao/diretoria-de-servicos-e-atividades-auxiliares/secao-de-conservacao-e-manutencao/)*

Telefone: 
*(16) 3706-8768* - Sala do Supervisor

*(16) 3706-8771* - Oficina

*(16) 3706-8772* - Sala de reunião, Copa e Almoxarifado

E-mail: *scm.franca@unesp.br*

Para o +requerimento de serviços+ do SCM - como manutenção no ar-condicionado ou projetor - é necessário utilizar o seguinte formulário:

[Manutenção SCM](https://docs.google.com/forms/d/e/1FAIpQLSefzt1YGYSXimtHnYm8lxaR_wMSMQP3SHpf1wSlU3SiWN-W9w/viewform)

Para acompanhar os serviços requisitados, pesquise na seguinte planilha:

[Planilha SCM](https://docs.google.com/spreadsheets/d/1sisZPFxo57X9hA6MbRtlTwlkzO9vysZ9iVhifvzbmf8/edit#gid=1657704734)

----

### STS - Seção Técnica de Saúde

*[Página Web](https://www.franca.unesp.br/#!/administracao/divisao-tecnica-administrativa/secao-tecnica-de-saude/)*

Telefones: 
*(16) 3706-8856*

*(16) 3706-8854* - (Recepção/Agendamento)

E-mail: *sts.franca@unesp.br*

Horário de atendimento ao público:

2ª feira - 8h às 12h - 13h às 16h e das 18h as 20:30h (noturno apenas em dias letivos)

3ª a 6ª feira - 8h às 12h e das 13h às 16h

De segunda a sexta-feira (dias úteis). 

----

### Comunicação

Endereço: Administração I - Sala 04

Telefone: *(16) 3706-8897*

E-mail: *comunicacao.franca@unesp.br*

Para solicitar serviços da Comunicação, basta enviar um e-mail para o endereço supracitado.

Expediente: 07h00 - 12h00 / 13h00 - 16h00

----

### Biblioteca

*[Página Web](https://www.franca.unesp.br/#!/sobre2108/biblioteca/)

Endereço: Biblioteca

Telefone: *(16) 3706-8883*

E-mail: *biblioteca.franca@unesp.br*

Expediente: 09h00 - 21h00
