---
id: intro
title: Apresentação TweePInA
sidebar_label: Apresentação
slug: /projetos/dados/tweepina/intro
---

<center>
    <img src="/img/projetos/dados/tweepina/tweepina.svg" alt="centered image" />
</center>

- [Página de divulgação no site aberto](https://labriunesp.org/docs/projetos/dados/tweepina/intro)
- [Repositório no GitLab](https://gitlab.com/unesp-labri/projeto/coleta-twitter)


O Twitter é uma das principais redes sociais da atualidade, sendo muito utilizado por autoridades e instituições públicas que são objetos de estudo de várias pesquisas acadêmicas. Porém, ter acesso a série histórica de tweets dessas autoridades e instituições muitas vezes é difícil. Além disso, muitos tweets acabam sendo deletados, não estando disponíveis em arquivos e/ou repositórios públicos. Devido a isso, o objetivo geral desse projeto é reunir tweets de autoridades e instituições públicas com especial destaque ao Brasil e organismos internacionais. 

Mais especificamente, este projeto visa: 

- (1) auxiliar a construção de uma memória de informações de autoridades e instituições públicas divulgadas através do Twitter; 

- (2) subsidiar pesquisas acadêmicas que utilizam o Twitter como fonte de informação de seus objetos de estudo através da disponibilização das variáveis disponibilizadas pelo Twitter; 

- (3) indicar possibilidades e instrumentos que auxiliem uma análise mais detalhada dos tweets; 

- (4) fornecer um instrumento básico de análise de tweets

[Link - tabela de atividades](https://labriunesp.org/docs/projetos/dados/tweepina/intro)

## Grupos envolvidos

- LANTRI

## Situação atual

- Iniciado em junho de 2020

- O projeto ativo e em andamento

- Neste momento, o projeto é desenvolvido em repositório restrito 

- Primeiras coletas de perfis do Twitter:

    - get_id_user: obtem o screename e retorna o respectivo id;

    - get_diretorio_base: obter diretório absoluto dos arquivos de controle das coletas (.json);

    - get_perfis_coletados: retorna um dicionario com os perfis e a última data de coleta;

    - flatten_tweets: percorre lista de dicionários ou o dicionário retorna uma nova lista de dicionários desalinhados;

    - clear_username: normaliza o nome do perfil;

    - def main(): realiza a coleta;

##  Caracteríticas e fucionalidades

- Indicamos abaixo uma síntese do histórico do desenvolvimento e nosso planos para o futuro do projeto 

### Desenvolvidas

### Em desenvolvimento

### Planos futuros


## Trabalhos derivados do projeto


## Frentes de Trabalho

- Desenvolvimento
- Documentação
- Comunicação
- Tutorais



## Você quer colaborar neste projeto?

## Equipe

- Atual: Pedro Campagna (junho/2020), Pedro Rocha (junho/2020), Rafael de Almeida (junho/2020)

- Ex-colaboradores: 

### Sobre o Logo: 

* Fonte: Bebas Neue Cyrillic
* Cores: #444446; #50ACE4
* [Link(Canva)](https://www.canva.com/design/DAEOn9JDEko/UkG36icGx3N2m-czVdYiRA/view?utm_content=DAEOn9JDEko&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton)


