---
id: intro
title: Apresentação Full Text
sidebar_label: Apresentação
slug: /projetos/dados/full-text/intro
---

<center>
    <img src="/img/projetos/dados/full-text/full-text.svg" alt="centered image" />
</center>

O projeto Full Text trata-se na disponibilização de diversos conteúdos de diversas mídias em formato de texto para permitir o processamento computacional. A partir do momento que essas mídias são transformadas, os textos irão permitir ao pesquisador realizar o processamento automático, baseado em softwares próprios ou de terceiros. Irá ajudar também a manter uma memória institucional/cultural dos conteúdos que estão sendo transformados em texto, pois haverá um backup dessas mídias.

- [Página de divulgação no site aberto](https://labriunesp.org/docs/projetos/dados/full-text/intro)
- [Repositório no GitLab](https://gitlab.com/unesp-labri/projeto/full-text)

