---
id: ocr
title: Serviço de OCR
sidebar_label: Serviço de OCR
slug: /projetos/dados/full-text/ocr
---

## Serviço de OCR - Workflow

Para Criar o serviço de OCR mais eficiente e atualizado, que é ainda mais necessário agora, com a quarentena, estão sendo utilizado por base os seguintes projetos do gitHub:

> [OCR My PDF](https://github.com/jbarlow83/OCRmyPDF) -> Clique para ver [Tutorial](http://tarefas.lantri.org/projects/wiki-01/wiki/101_TutorialOcrMyPdf_)

Serviço Instalado: [Serviço OCR](http://tarefas.lantri.org/projects/wiki-01/wiki/Servi%C3%A7o_OCR)
