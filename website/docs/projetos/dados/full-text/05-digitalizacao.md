---
id: digitalizacao
title: Digitalização e Pós-Processamento
sidebar_label: Digitalização e Pós-Processamento
slug: /projetos/dados/full-text/digitalizacao
---

## Digitalização e Pós-Processamento 

O LabRI reúne esforços no que tange a digitalização de material importante aos discentes e docentes de Relações Internacionais. Nesse caso, o estagiário devera realizar as digitalizações demandadas pelos professores do curso e pelos professores responsáveis pelo laboratório, realizar o pós-processamento do material e disponibilizá-lo na base de dados existente e atualizar as planilhas de obras e tempo utilizado no processo. Além disso o estagiário deve providenciar auxílio para discentes e grupos de extensão que necessitem da digitalização, ensinando os procedimentos. 

Para tanto, lista-se abaixo os principais links e tutoriais para fazê-los:

* *[GoogleDocs da Base de Dados Acadêmica (BDA)](https://docs.google.com/document/d/1QPhY0Xy3wUVCKcbJT-1D5LPCDddsLPrzsdzvfpSjgt0/edit?ts=5ac7c088)* - No arquivo acima constam informações sobre a estrutura e a alocação dos arquivos da base geral de dados acadêmicos disposta no LANTRI.
Nele também consta o objetivo do trabalho do grupo em relação aos dados coletados, e um passo a passo simplificado do trabalho a ser Feito (Checklist) - para novas digitalizações. 
* *[Fluxograma da Base de Dados Acadêmica(BDA)](https://drive.google.com/file/d/19oUGlreyFUkpowcbouivXjOUSTKHn6aG/view?usp=sharing)*

* *[Fluxograma de Passo a Passo (Checklist) para novas Digitalizações](https://www.draw.io/#G0BwquYFt95odqWU1YZDNVSlAtMEU)*

* *[Fluxograma de Passo a Passo (Checklist) para novas Digitalizações - NOVO](https://drive.google.com/file/d/1Gopyhkv9Yp7TAzhFOKTJr9SzTwsNxm1O/view?usp=sharing)*

* *[Lista de textos para digitalização](https://docs.google.com/spreadsheets/d/1p3fujsmOKkpRgqQ6CZ_HXQ08mh0dRJ0gDocti3kz-cQ/edit?usp=sharing)* - Nesta lista estão os livros ou textos impressos que devem ser digitalizados, os alunos podem complementar a lista com as demandas de suas pesquisas

* *[Tutorial Scanners](https://docs.google.com/document/d/1uoDBJW-FF3PeVcdw_Rb_W32xdO2R3jBorVYMGrY3iBE/edit?usp=sharing)* - Tutorial de como usar cada scanner do LabRI


* *[DIGITALIZAÇÃO-Planilha de registro e controle](https://docs.google.com/spreadsheets/d/1BPB6fwxIY3BZwqcGGqsOQ0APk9_SCzdCi1QyoVNgBCY/edit#gid=0)* - Registrar na planilha acima todos os textos digitalizados no laboratório

* *[Vídeo explicativo sobre como inserir os dados da digitalização na planilha](https://youtu.be/bTe3qPT7f_k)*


* [DIGITALIZAÇAO - Livros, Teses e Artigos soltos](https://docs.google.com/spreadsheets/d/1E7TehGhd3ays7esqShP7l1pXNWc8bzSovm9yggWECTs/edit#gid=0)

*[[01-01 Localizacao | Localização dos arquivos]]*

### Pós-processamneto das imagens da Revista de Política Externa


**PASSO 1** - deslocar imagens pós-processadas para a pasta raiz da revista

**PASSO 2** - compactar os arquivos através do script "compactar-zip.sh"

**PASSO 3** - renomear arquivo zipado. Acrescentar o nome da pasta como prefixo do nome do arquivo zipado

**PASSO 4** - fazer upload dos arquivos zipados para a realização do OCR no site: https://sites.google.com/unesp.br/labri-servicos-disponveis/ocr

**PASSO 5** - pegar o arquivo na pasta de output do site de ocr do labri e colocar na pasta de dados: /media/hdvm09/bd/007/002/002/010/Revista-Politica_Externa/PDF/
