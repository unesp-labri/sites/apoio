---
id: intro
title: Apresentação MercoDocs
sidebar_label: Apresentação
slug: /projetos/dados/mercodocs/intro
---

<center>
    <img src="/img/projetos/dados/mercodocs/mercodocs.svg" alt="centered image" />
</center>

O Mercosul é uma das instituições de integração latino-americanas mais reconhecida e importante desde os anos 90. Sendo assim, o projeto MercoDocs tem o intuito de criar uma memória institucional mais sólida dos documentos já produzidos e disponibilizados por essa instituição. Além disso, para garantir a acessibilidade do projeto, a biblioteca digital será gratuita e pesquisável. 

- [Página de divulgação no site aberto](https://labriunesp.org/docs/projetos/dados/mercodocs/intro)
- [Repositório no GitLab](https://gitlab.com/unesp-labri/projeto/mercosul-normas-selenium)


