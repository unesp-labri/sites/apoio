---
id: intro
title: Apresentação News Cloud
sidebar_label: Apresentação
slug: /projetos/dados/newscloud/intro
---

<center>
    <img src="/img/projetos/dados/news-cloud/news-cloud.svg" alt="centered image" />
</center>

Uma importante fonte para pesquisa acadêmica são as notícias veiculadas pelos jornais impressos. Nesses veículos de comunicação, além de informações nacionais e internacionais relevantes, são encontrados opiniões de importantes atores políticos. Porém, o conjunto de informações de cada jornal se encontram em bases de dados distintas sem um mecanismo que viabilize uma busca agregada; a indexação integral dos dados vinculados apresenta limitações que dificultam a pesquisa avançada (utilização de operadores booleanos) através da busca por palavras-chaves, especialmente, quando selecionamos um período temporal longo e abarcamos o grande volume de informação; as informações veiculadas em formato textual não estão estruturadas, isso dificulta o cruzamento de metadados importantes (título, autor, caderno, entre outros). Devido a isso, o objetivo geral desse projeto é coletar, indexar, tratar e estruturar as informações veiculadas por jornais impressos. Mais especificamente, este projeto visa (1) coletar integralmente os jornais impressos selecionados, realizando o devido tratamento das informações veiculadas para uma melhor utilização dos dados para pesquisas acadêmicas; (2) subsidiar pesquisas acadêmicas que utilizam jornais impressos como fontes de informação ou objeto de estudo; (3) fornecer um instrumento básico para análise das informações veiculadas; (4) indicar possibilidades e instrumentos que auxiliem análises mais detalhadas das informações veiculadas.


Devido aos direitos autorais dos jornais coletados, este projeto não é um projeto aberto. Com isso, tanto o repositório no GitLab quanto as demais informações e bases de dados formadas, são de utilização restrita aos docentes e discentes que têm acesso à estrutura do LabRI/UNESP. Apesar dessa restrição, no site aberto divulgamos esse projeto e temos uma tabela com as atividades realizadas em torno dele. 

|Assunto|Link|
|---|---|
|Página de divulgação no site aberto|[Acesse aqui](https://labriunesp.org/docs/projetos/dados/newscloud/intro)|
|Repositório no GitLab|[Acesse aqui](https://gitlab.com/unesp-labri/projeto/newscloud)|
|Pasta do Drive|[Acesse aqui](https://drive.google.com/drive/u/1/folders/1LZ5QHvSojjB5jXb0Ycp6JTcYbZ5aS2Zj)|
|DrawIO|[Acesse aqui](https://app.diagrams.net/#G1ocP0GbS4f3Iw3I_CjBJssart2VJY70SE)|

