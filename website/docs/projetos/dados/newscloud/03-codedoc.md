---
id: codedoc
title: Documentação News Cloud
sidebar_label: Documentação
slug: /projetos/dados/newscloud/codedoc
---

### Notícias Folha de S. Paulo

- De janeiro/1994 a março/2020, as pastas são organizadas de acordo com o mês, ou seja, não há uma divisão diária para as matérias
- A partir de abril/2020, cada dia tem a sua pasta, organizando as matérias dentro de cada pasta que representa o dia


|Script|Descrição|Funções|
|---|---|---|
|`noticia.py`|Cria objeto-notícia; realiza a extração das informações no HTML||
|`html_base.py`|É o modelo do HTML final após o tratamento dos dados||
|`disparar_tratamento.py`|Armazena a origem e o destino dos dados; indica nomes de arquivos a serem desconsiderados no tratamento; verifica as notícias já tratadas; cria uma estrutura de pastas; dispara o tratamento das notícias|`set_caminho()`, `get_pular_nomes()`, `set_entrada()`, `verifica_se_noticia_foi_limpa()`, `cria_estrutura()`|

|Variável|Descrição|
|---|---|
|`origin`|Pasta/diretório raiz onde estão localizados os arquivos a serem tratados (limpos).|
|`destiny`|Pasta/diretório raiz onde estão localizados os arquivos que foram tratados (limpos).|
|`dir_origin.list()`|Lista de todos os diretórios|
|`dir_origin`|Diretórios que serão tratados (limpos)|
|`files_already_treated`|Arquivos que já foram tratados (limpos)|
|`files_for_treatment`|Arquivos que serão tratados (limpos)|
|`skip_names`|Pula nomes de arquivos que não são notícias|

|Funções do noticia.py|Descrição|
|---|---|
|`format_date()`|Formatar a data|
|`generate_html_files_treated()`|Gera HTML dos arquivos tratados|
|`set_encoding()`|Lidar com "encoding" dos arquivos; abrir arquivos a serem tratados com BeautifulSoup e tentar extrair informações armazenadas na tag `meta`|
|`get_title()`|Obtém os títulos das notícias|
|`get_subtitle()`|Obtém os subtítulos das notícias|
|`get_author()`|Obtém os autores das notícias|
|`get_date()`|Obtém a data de publicação|
|`get_sections_newspaper()`|Extrair os nomes dos cadernos em que a notícia foi vinculada|
|`move_news_to_folder_files_with_errors()`|Move arquivos corrompidos para a pasta específica|
|`get_link()`|Obtém os links (href) das páginas HTML|
|`get_body()`|Verifica existência e obtém o corpo (tag `body`) das noticias|
|`find_body()`|Procura pelo `body`|
|`write_abnt()`|Faz as referências em ABNT|
|`get_month_in_text()`|Obtém o mês em texto|
|`insert_in_html_base_body_news()`|Insere o corpo no `html_base.py`|
|`insert_in_html_base_authors()`|Insere os autores no `html_base.py` |
|`insert_in_html_base_date_news()`|Insere a data da notícia no `html_base.py`|
|`get_relative_path()`|Obtém o caminho relativo|



### SQL 

- https://questdb.io
- https://sqlmap.org
- https://prestd.com
- https://samsung.github.io/qaboard/
- https://www.cockroachlabs.com 
- https://github.com/mkleehammer/pyodbc
- https://github.com/PyMySQL/PyMySQL
- https://github.com/kayak/pypika
- https://github.com/INWTlab/dbrequests
- https://github.com/nackjicholson/aiosql
- https://github.com/yhat/db.py 
- https://dataset.readthedocs.io/en/latest/quickstart.html 

