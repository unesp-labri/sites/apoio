---
id: id-visual
title: Identidade Visual
sidebar_label: Identidade Visual
slug: /projetos/dados/id-visual
---

### Sobre o Logo

* Fontes: Playfair Display; Hammersmith One.
* Cores: #51704A; #0A4021; #6F6F70.
* [Link (Canva)](https://www.canva.com/design/DAEdhYi4uJ0/s3vDkL8vudvpqh_vMKw7yQ/view?utm_content=DAEdhYi4uJ0&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton)