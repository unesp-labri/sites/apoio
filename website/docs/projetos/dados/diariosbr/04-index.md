---
id: index_diarios 
title: Indexação
sidebar_label: Indexação
slug: /projetos/dados/diariosbr/index_diarios
---


## Indexação realizada pelo Recoll  

- planilha de controle dos dados 
- diretórios dos dados e diretórios de indexação
- script de indexação do Recoll 

## Operadores Booleanos

- `and`, `or`, `not`

