---
id: comandos_linux
title: Comandos básicos do terminal linux
sidebar_label: Comandos Linux  
slug: /projetos/dados/diariosbr/comandos_linux
---


## Básico


| Comando | Funcionalidade | Descrição/Forma de usar |
| :-----: | :------------: | :---------------------: |
| ls |  Listar pastas|ls -la: lista todas as pastas, inclusive as ocultas|
| cd| Comando para se locomover |cd `nome da pasta`: entrar em uma pasta; cd `nome da pasta`: sair de uma pasta|
| rm | Comando de remoção |rm -r: remover  arquivos/diretórios; rmdir: remover um diretório vazio|
| mkdir| Comando de criação de pastas|Ex: mkdir `nome da pasta`|
| touch | Comando de criação de arquivo vazio |-|
| history | Comando para listar o que já foi editado|-|
| clear | Comando para limpar o terminal |-|
| mv | Comando para mover ou renomear arquivos|-|
| ps | Comando que exibe informações sobre os processos atuais|ps - e: exibe todos os processos|
| wget | Comando para fazer download de arquivos |--no-clobber: evita o download de arquivos repetidos; `--directory-prefix=nome_da_pasta`: faz o download do arquivo em uma pasta específica|
| bash script | executa um comando shell |-|

## Git

| Comando | Funcionalidade | Descrição/Forma de usar |
| :-----: | :------------: | :----------------------:| 
| git config | definir usuário e email | necessário para submeter commits |
| git help | ajuda a entender comandos | git help ou git `comando` --help |
| git init | adicionar um projeto existente | - |
| git clone | clonar um repositório do gitlab/github | git clone `url` |
| git add | comando para iniciar submissão de commit | - |
| git commit | descreve o que será enviado | "git commit -m `descrição` |
| git pull | adiciona ao projeto novas modificações externas | "git pull origin" |
| git push | enviar o commit | "git push origin main" |
| git status | analisar status do arquivo | - |

```

ls (listar pastas e diretórios)

ls -la (listar todos os arquivos e pastas - ocultos ou normais/visiveis )

cd (mudar diretório) 

cd .. (voltar um diretório)

rm (remover arquivos)

clear

rm -r

mkdir

cp

mv (move ou renomeia)

history, 




 
  521  ls -la
  522  cd Documentos/
  523  ls
  524  cd .
  525  rm .
  526  rm -r .
  527  rm -r ..
  528  rm -r *
  529  cd 
  530  cd Documentos/
  531  mkdir teste
  532  cd mkdir
  533  cd teste/
  534  cd
  535  cd Documentos/teste/
  536  cd ..
  537  cd teste/
  538  nano teste.txt
  539  ls
  540  touch teste02.txt
  541  touch teste{1..10}.txt
  542  mkdir teste{1..10}
  543  ls 
  544  ls /home/lantri_rafael
  545  clear
  546  history


chmod +x limpar.sh



https://crontab.guru/
crontab -e

ps -e | grep “do”
kill - 9


```

### Agendamento de tarefas - crontab

- agendar script

```
contrab -e

```
- listar scripts agendados

```
contrab -l

```

- arquivo do crontab

```

# Diário Oficial do Legislativo do Estado de SP # usuário do vitorio
18 02 * * * /media/hdvm05/scripts/diariosbr/doespleg/doespleg.sh

#Diário Oficial do Executivo do Estado de SP  # usuário do vitorio
28 02 * * * /media/hdvm05/scripts/diariosbr/doesp/doesp.sh

#Diário Oficial de Franca  # usuário do vitorio
38 17 * * * bash /media/hdvm05/scripts/diariosbr/domfranca/dof-2021-start.sh


```


## Video do dia 12/05 (Julia)
| Comando | Funcionalidade | Descrição/Forma de usar |
| :--- | :---: | :---: |
| touch | criação de arquivo vazio | - |
| ls | lista todas as pastas | "ls -la" para visualizar também as pastas ocultas |
| nano | editor de texto simples para o terminal linux | nano (arquivo.txt) |
| cat | visualização completa de arquivos | "cat -n": numera  todas as linhas |
| wget | fazer download de arquivos | - |


## Video do dia 19/05 (Tonetto)
| Comando: Ctrl + shift + v | Funcionalidade: colar algo no terminal |

| Comando: Ctrl + shift + c | Funcionalidade: copiar algo do terminal |

| Comando: Ctrl + x e, depois, clicar em s | Funcionalidade: salvar as edições e sair do terminal |

| Comando: wget -- input + nome do arquivo.txt | Funcionalidade: baixar uma lista de links em um arquivo txt. |

| Comando: wget -- no clobber -- input + nome da pasta | Funcionalidade: verificar se o arquivo já existe. Se já existir, ele não é baixado | 

| Comando: wget -- no clobber -- input + nome do arquivo + -- directory - prefix - homer/nome do usuárior/nome da pasta desejada | Funcionalidade: indicar onde se quer que o arquivo seja baixado |

| Comando: Ctrl + c | Funcionalidade: quebrar a linha do terminal | 

| Comando: # | Funcionalidade: fazer algum comentário | 

| Comando: #!bin/bash | Funcionalidade: comentário que mostra que vai utilizar bash | 
  
## Video do dia 02/06 (Vitorio)


### Como chegar aos documentos do projeto?
1. [Página inicial do Notion do Lantri] (https://www.notion.so/Bem-vindo-ao-LabRI-42b2ae426089479ab832af4f53e6e800)
2. Em Gestão de Atividades, acessar Projetos
3. Em Desenvolvimento, acessar Projeto - DiariosBR

#### Quais informações estão disponíveis no Notion?
1. Documentação;
2. Repositório de códigos (GitLab);
3. Drive do NEPPS;
4. Playlist das gravações das reuniões;
5. Material de apoio.

### Site aberto do projeto
1. Acessar o site labriunesp.org;
2. Ir na aba Projetos (Saiba mais);
3. Ir na aba Projetos de Dados (Saiba mais);
4. Acessar DiáriosBR.

### Acesso remoto e Krusader (gerenciador de arquivos)
1. Acessar os servidores da Unesp pelo X2Go ou pelo acesso remoto do Chrome;
2. Menu (1º atalho do canto inferior esquerdo);
3. Acessórios;
4. Krusader.

### Configurar o Krusader e acessar as coletas dos diários oficias

Para voltar o caminho percorrido anteriormente pelo Krusader, deve-se clicar na seta verde do canto superior esquerdo, em ambas as telas


### O que fazem os códigos de coleta?

#### Exemplo da linha 29 do código diariosbr/domfranca/dof-2020.sh


```

wget --no-clobber https://www.franca.sp.gov.br/arquivos/diario-oficial/documentos/{1424..1700}-{01..31}092020.pdf

Essa linha de código coleta os diários oficiais de Franca no intervalo 1424 a 1700, dos dias 01 a 31 de Setembro de 2020, no formato PDF, disponibilizados pela prefeitura do município

O parâmetro faz a combinação de todos os números do intervalo 1424 a 1700 combinados com os dias do mês de Setembro. As urls que existirem ele coleta.

O parâmetro no-clobber faz com que o wget não colete duas vezes o mesmo link


```

### Começando no Git

1. Comando `git add .` : monitora os arquivos a partir da pasta local
2. Comando `git commit -m mensagem que deseja (ex: início versionamento Vitório)`
3. Cadastrar com email e nome
4. Comando `git pull origin master - puxa os arquivos do gitlab pro local (É possível ver o diretório exemplos criado anteriormente)`

### Criando arquivos e tornando-os executáveis

1. Comando ‘touch `<nome do arquivo>` - cria o arquivo
2. Comando ‘chmod +x `<nome do arquivo>` - torna o arquivo executável

