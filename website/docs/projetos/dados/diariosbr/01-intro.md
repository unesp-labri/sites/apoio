---
id: intro
title: Apresentação DiáriosBR
sidebar_label: Apresentação
slug: /projetos/dados/diariosbr/intro
---

<center>
    <img src="/img/projetos/dados/diariosbr/diariosbr.svg" alt="centered image" />
</center>

Os Diários Oficiais são onde toda a movimentação legal dos governos federal, estadual e municipal são publicadas, sendo esse fator o que os tornam uma fonte de pesquisa importante para o acompanhamento da destinação de recursos, transferência de cargos e o embasamento legal das atividades da administração pública. 

- [Página de divulgação no site aberto](https://labriunesp.org/docs/projetos/dados/diariosbr/intro)
- [Repositório GitLab](https://gitlab.com/unesp-labri/projeto/diariosbr)


