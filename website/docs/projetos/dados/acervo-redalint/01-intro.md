---
id: intro
title: Apresentação Acervo Redalint
sidebar_label: Apresentação
slug: /projetos/dados/acervo-redalint/intro
---


<center>
    <img src="/img/projetos/dados/acervo-redalint/redalint.svg" alt="centered image" />
</center>



- [Página de divulgação no site aberto](https://labriunesp.org/docs/projetos/dados/acervo-redalint/intro)
- [Repositório GitLab](https://gitlab.com/unesp-labri/projeto/acervo-redalint)

- e-mail: acervoredalint@gmail.com
- [Pasta Google Drive](https://drive.google.com/drive/u/2/folders/1npycu_yv0hd-nuASxNJuH1oVXsMbo49F)
