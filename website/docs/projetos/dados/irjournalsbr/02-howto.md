---
id: howto
title: Como Utilizar IRJournalsBR
sidebar_label: Como Utilizar
slug: /projetos/dados/irjournalsbr/howto
---

### Criar pasta para armazenar o código do projeto

```
mkdir -p ~/codigo && cd codigo
```
### Clonar repositório do projeto

```
git clone https://gitlab.com/unesp-labri/projeto/irindexbr.git
```
### Entrar na pasta do projeto

```
cd irindexbr
```

### Criar ambiente virtual e instalar dependências do projeto  

- O comando abaixo é utilizado apenas quando o arquivo environment.yml é criado pela primeira vez. Caso ele já esteja no repositório, não é necessário rodar o comando abaixo.

```
touch environment.yml && conda env create -f environment.yml  
```

### Indicar inicializador
- Após rodar o comando abaixo reinicie o terminal

```
conda init bash
```

### Entrar na pasta do projeto 

```
cd ~/codigo/irindexbr
```

### Ativar ambiente virtual 

```
conda activate env_irindexbr
```

### Atualizar dependências do projeto 

- O comando abaixo é utilizado para que seu ambiente virtual esteja atualizado com todas as dependências necessárias. Indicamos que este comando seja utilizado sempre que você for mexer no projeto, para garantir que você tenha acesso a todas as bibliotecas.

```
git pull origin main && conda activate env_irindexbr && conda env update 
```

### Inserir novas bibliotecas no environment.yml
 
- O comando abaixo é utilizado para instalar uma nova biblioteca utilizada no projeto. Ao rodar o comando ele irá atualizar as dependências do projeto para todos os colaboradores.

```
conda env export > environment.yml
```
