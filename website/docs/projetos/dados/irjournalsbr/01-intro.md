---
id: intro
title: Apresentação IRJournalsBR
sidebar_label: Apresentação
slug: /projetos/dados/irjournalsbr/intro
---


<center>
    <img src="/img/projetos/dados/irjournalsbr/irjournalsbr.svg" alt="centered image" />
</center>

- [Página de divulgação no site aberto](https://labriunesp.org/docs/projetos/dados/irjournalsbr/intro)
- [Repositório no GitLab](https://gitlab.com/unesp-labri/projeto/irjournalsbr-parse-xml)


