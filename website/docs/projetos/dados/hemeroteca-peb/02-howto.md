---
id: howto
title: Como Utilizar Hemeroteca PEB
sidebar_label: Como Utilizar
slug: /projetos/dados/hemeroteca-peb/howto
---

## Configuração inicial

 - As configurações indicadas neste item são necessárias APENAS na primeira vez em que você utiliza o repositório deste projeto. Se você já realizou tais configurtações de agora em diante é necessário apenas atualizar e ativar o ambiente virtual do repositório do projeto. Para isso, veja  as instruções indicadas no item "Após configuração inicial".


### Criar pasta para armazenar o código do projeto

```
mkdir -p ~/codigo && cd codigo
```
### Clonar repositório do projeto

```
git clone https://gitlab.com/unesp-labri/projeto/hemeroteca-peb.git
```
### Entrar na pasta do projeto

```
cd hemeroteca-peb
```



### Indicar inicializador

- Após rodar o comando abaixo reinicie o terminal

```
conda init bash
```

### Entrar na pasta do projeto 

```
cd ~/codigo/hemeroteca-peb
```


### Atualizar dependências do projeto 

- O comando abaixo é utilizado para que seu ambiente virtual esteja atualizado com todas as dependências necessárias. Indicamos que este comando seja utilizado sempre que você for mexer no projeto, para garantir que você tenha acesso a todas as bibliotecas.

```
git pull origin main && conda activate env_hemeroteca-peb && conda env update 

```



## Informações abaixo são destinadas a criação do environment.yml inicial

### Criar pasta para armazenar o código do projeto

```
mkdir -p ~/codigo && cd codigo
```
### Clonar repositório do projeto

```
git clone https://gitlab.com/unesp-labri/projeto/hemeroteca-peb.git
```
### Entrar na pasta do projeto

```
cd hemeroteca-peb
```

### Criar ambiente virtual e instalar dependências do projeto  

- O comando abaixo é utilizado apenas quando o arquivo environment.yml é criado pela primeira vez. Caso ele já esteja no repositório, não é necessário rodar o comando abaixo.

```
touch environment.yml && conda env create -f environment.yml  
```

### Indicar inicializador
- Após rodar o comando abaixo reinicie o terminal

```
conda init bash
```

### Entrar na pasta do projeto 

```
cd ~/codigo/hemeroteca-peb
```

### Ativar ambiente virtual 

```
conda activate env_hemeroteca-peb
```

## Após configuração inicial 

### Atualizar dependências do projeto 

- O comando abaixo é utilizado para que seu ambiente virtual esteja atualizado com todas as dependências necessárias. Indicamos que este comando seja utilizado sempre que você for mexer no projeto, para garantir que você tenha acesso a todas as bibliotecas.

```
git pull origin main && conda activate env_hemeroteca-peb && conda env update 
```

### Inserir novas bibliotecas no environment.yml
 
- O comando abaixo é utilizado para instalar uma nova biblioteca utilizada no projeto. Ao rodar o comando ele irá atualizar as dependências do projeto para todos os colaboradores.

```
conda env export > environment.yml
```
