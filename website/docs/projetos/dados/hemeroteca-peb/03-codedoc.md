---
id: codedoc
title: Documentação Hemeroteca PEB
sidebar_label: Documentação
slug: /projetos/dados/hemeroteca-peb/codedoc
---

|Item | Link |
| --- | --- |
|Repositório |[Clique aqui](https://gitlab.com/unesp-labri/projeto/hemeroteca-peb)|
| Diretório local | `/media/hdvm07/bd/002/997/001/hemeroteca-peb-metadados/tratamento-dados`|

#### Criação de link simbólico

- O link simbólico cria um atalho para pastas ou arquivos. No nosso caso estamos criando um atalho para a pasta que contêm os dados da hemeroteca (seleção de notícias). Tais dados não são versionados , ou seja, não se encontram no repositório do projeto estando apenas em nossos diretórios locais.

``` 
ln -s /media/hdvm07/bd/002/997/001/hemeroteca-peb-metadados/tratamento-dados tratamento-dados
```
