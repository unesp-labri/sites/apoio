---
id: bases-dados
title: Bases de Dados
sidebar_label: Bases de Dados
slug: /projetos/dados/bases-dados
---

|Código|Conteúdo|
|---|---|
|001|Diário Oficial|
|001/001|Diário Oficial - União|
|001/002|Diário Oficial Estaduais e Municipais|
|001/002/001|Diário Oficial - Estado de São Paulo|
|001/002/002|Diário Oficial  - Municipio de Franca-SP|
|002|Jornais e Notícias|
|002/001|Jornais - Brasil|
|002/001/001|Jornal - Valor Economico|
|002/001/002|Jornal - O Estado de S. Paulo|
|002/001/003|Jornal - Folha de S. Paulo|
|002/002|Jornais - Argentina|
|002/997|Hemerotecas de Notícias|
|002/998|Seleção de Notícias|
|002/999/001|Noticias Soltas|
|003|Dados estatais (governos federal, estadual e municipal)|
|003/001|Dados estatais - América Latina|
|003/001/001|Dados estatais - América Latina - Brasil|
|003/001/001/001|Dados estatais - Am Latina - Brasil - Gov Federal|
|003/001/001/001/002|Dados estatais - Am Latina - Brasil - Gov Federal - Ministério das Relações Exteriores|
|003/001/001/002|Dados estatais - América Latina - Brasil - Câmara Federal|
|003/001/001/003|Dados estatais - América Latina - Brasil - Senado Federal|
|003/001/002/001|Dados estatais - América Latina - Uruguay|
|003/002|Dados estatais - Europa|
|003/003|Dados estatais - América do Norte|
|003/004|Dados estatais - Ásia|
|003/005|Dados estatais - África|
|004|Dados de Instituições internacionais|
|005|Dados de Intituições Regionais|
|005/001|Dados de Intituições Regionais - América Latina|
|005/001/001|Mercosul|
|005/002|Dados de Intituições Regionais - Europa|
|005/002/001|União Europeia|
|005/003|Dados de Intituições Regionais - África|
|005/004|Dados de Intituições Regionais - Ásia|
|005/005|Dados de Intituições Regionais- América do Norte|
|006|Organizações da Sociedade Civil|
|006/001|Organizações da Sociedade Civil - ONGs|
|006/002|Organizações da Sociedade Civil - Empresariado|
|006/003|Organizações da Sociedade Civil - Movimentos sociais|
|007|Bibliografia Academica|
|007/001|Bibliografia Academica - Artigos Avulsos|
|007/002|Bibliografia Academica - Coleções Abertas|
|007/002/001|Bibliografia Academica - Coleções Abertas - Anais|
|007/002/001/001|Bibliografia Academica - Coleções Abertas - Anais - ABRI|
|007/002/002|Bibliografia Academica - Coleções Abertas - Revistas|
|007/002/003|FUNAG|
|007/002/004|Scielo|
|007/002/005|Bibliografia Academica - Coleções Abertas - Anuários|
|007/002/006|Livros|
|007/002/007|Teses, Dissertações, TCC|
|007/003|Bibliografia Academica - Coleções Fechadas|
|007/003/001|Livros Fechados|
|008|Redes sociais|
|009|Observatórios|
|009/001|Observatório - Brasil|
|009/001/001|Observatório de Política Externa Brasil - Gedes|
|009/001/002|Observatório de Defesa - GEDES|
|010/000|Base de Vídeos|
|011/000|Base de Audios|
|100|Bases tematicas|
|100/001|Temática - Tecnologia|
|100/001/001|/media/hdvm05/index/100/001/001/001/tematica-tecnologia-tics-01|
|200|Bases espefificas dos pesquisadores|
|200/001|Marcelo Mariano|
|200/002|Rafael de Almeida|
|200/003|Barbara Neves|
|200/004|Jaqueline Pigatto|
|200/005|Elias Souza|
|200/006|Guilherme Pinto|
|200/007|Luiz Moreira|
|200/008|Ana Elisa|
|200/009|Julia Borba|
|300|Bases de Grupos|
|300/001|GEICD|
|300/002|GETED|
|300/003|REDALINT|
|300/004|NEPPS|
|900|Documentos sigilosos|













