---
id: intro
title: Apresentação Governo LatinAmerica
sidebar_label: GovLatinAmerica
slug: /projetos/dados/gov-latin-america/intro
---

<center>
    <img src="/img/projetos/dados/gov-latam/gov-latam.svg" alt="centered image" />
</center>

Os dados públicos dos órgãos governamentais latino americanos disponíveis via web com frequência são retirados dos sites oficiais, especialmente, após a passagem de um mandato presidencial para outro. A partir deste diagnóstico, o objetivo do projeto GovLatinAmerica é coletar tais dados para que possam ser utilizados em pesquisas acadêmicas diversas.




|Conteúdo|Link de Acesso|
|---|---|
|Código das Coletas|[Repositório GitLab](https://gitlab.com/unesp-labri/projeto/govlatinamerica)|
|Informações gerais|[Pasta Google Drive](https://drive.google.com/drive/u/1/folders/1_g01RcccLl2PpTupxQyCoXEJka30VXeG)|
|Tarefas do projeto|[Notion](https://www.notion.so/Projeto-GovLatinAmerica-9219a9b60ae24cb98a197f7bdab42209)|
|Documentação|[Site](https://apoio.labriunesp.org/docs/projetos/dados/gov-latin-america/intro/)|



|Data|Conteúdo realizado|
|---|---|
|05/08/2021|Estruturação do mapeamento e início da estruturação do fluxograma da coleta e tratamento dos dados.|
|29/07/2021|Ajustes BeautifulSoup e extração das informações do `xml` do sidemap|
|22/07/2021|Configuração do ambiente virtual Conda e do VSCode|


