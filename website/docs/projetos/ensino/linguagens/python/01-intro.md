---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/ensino/linguagens/python/intro
---

- Avaliar
  - https://www.pythoncheatsheet.org/#JSON,-YAML-and-configuration-files
  - https://python-guide-pt-br.readthedocs.io/pt_BR/latest/writing/logging.html
  - https://pypi.org/project/webdriver-manager/
  - https://betterprogramming.pub/3-simple-ways-to-download-files-with-python-569cb91acae6
   -https://chalk.ist/