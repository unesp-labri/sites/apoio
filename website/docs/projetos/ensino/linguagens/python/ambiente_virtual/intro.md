---
id: intro
title: Introdução - Ambiente Virtual
sidebar_label: Intro-Ambiente Virtual
slug: /projetos/ensino/linguagens/python/ambiente_virtual/intro
---

## Configuração utilizando o conda


```
conda config --set pip_interop_enabled True
conda config --set env_prompt '({name})'
conda config --add envs_dirs ./env
touch environment.yml && conda env create -f environment.yml
git pull origin main && conda activate env_hemeroteca-peb && conda env update --prune

```


```

wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh

bash Miniconda3-latest-Linux-x86_64.sh 

conda config --set pip_interop_enabled True
conda config --set env_prompt '({name})'
conda config --add envs_dirs ./env

touch environment.yml && conda env create -f environment.yml

```


```
hostnamectl set-hostname <novo_nome>

sudo apt-get install nfs-common
sudo nano /etc/fstab 

nano /etc/ssh/sshd_config
nano .ssh/authorized_keys
systemctl restart sshd
```
















