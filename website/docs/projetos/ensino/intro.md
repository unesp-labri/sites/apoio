---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/ensino/intro
---

<center>
    <img src="/img/projetos/ensino/logo-projetos-ensino.svg" alt="centered image" />
</center>

Os projetos de ensino são iniciativas voltadas à promoção de oficinas, material de apoio e cursos que incentivem e auxiliem uma melhor incorporação de ferramentas tecnológicas nas atividades de pesquisa. Sendo assim, neste espaço é possível encontrar projetos conectados para melhorar o intercâmbio entre as Relações Internacionais e a ciência de dados.
