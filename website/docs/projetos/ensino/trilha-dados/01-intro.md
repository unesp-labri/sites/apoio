---
id: intro
title: Apresentação Trilha de Dados
sidebar_label: Trilha de Dados
slug: /projetos/ensino/trilha-dados/intro
---

## Trilha de Dados
O projeto Trilha de Dados trata-se da disponibilização de curso básico de Ciência de Dados acessível a todos os estudantes de todos os níveis de conhecimento de programação, focado em aplicações nas Relações Internacionais. 

<center>
    <img src="/img/projetos/ensino/trilha-dados/logo-trilha-dados.png" alt="centered image" />
</center>


### Sobre o Logo: 

* Fonte League Gothic
* Cores: #004455; #D54050; #000000
* [Link (Canva)](https://www.canva.com/design/DAEOn9JDEko/UkG36icGx3N2m-czVdYiRA/view?utm_content=DAEOn9JDEko&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton)