---
id: iniciativas
title: Iniciativas
sidebar_label: Iniciativas
slug: /projetos/ensino/iniciativas
---

<center>
    <img src="/img/projetos/ensino/logo-projetos-ensino.svg" alt="centered image" />
</center>

- [Trilha de Dados](https://labriunesp.org/docs/projetos/ensino/trilha-dados/intro)
- [Tecnologias Digitais](https://labriunesp.org/docs/projetos/ensino/tec-digitais/intro)
