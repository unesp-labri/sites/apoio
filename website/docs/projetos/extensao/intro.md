---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/extensao/intro
---

<center>
    <img src="/img/projetos/extensao/logo-projetos-extensao.svg" alt="centered image" />
</center>

Os projetos de extensão são iniciativas voltadas para melhorar e incentivar a interação entre o meio acadêmico e a sociedade. Assim, neste espaço é possível encontrar projetos que buscam envolver docentes e discentes em atividades voltadas à divulgação científica.
