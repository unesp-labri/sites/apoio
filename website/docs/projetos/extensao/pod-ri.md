---
id: pod-ri
title: Pod-RI - Podcast de Relações Internacionais
sidebar_label: Pod-RI - Podcast de Relações Internacionais
slug: /projetos/extensao/pod-ri
---

## Apresentação 

<center>
    <img src="/img/projetos/extensao/pod-ri/logo-pod-ri.svg" alt="centered image" />
</center>

## Tutorial: Colocar os episódios do Pod-RI no Youtube 

:::caution

As senhas das Redes Sociais do Pod-RI encontram-se no Psono do LabRI 

:::

#### Passo 1 

Acessar o site "[Anchor](https://anchor.fm/)", acessar a conta do Pod-RI e entrar nos **episódios** do Pod-RI.

![GIF do Passo 1: Acessar o site Anchor](https://i.imgur.com/3pY8A5K.gif)

#### Passo 2 

Fazer download do episódio que deseja colocar no Youtube 

![GIF do Passo 2: Baixar episódio desejado](https://i.imgur.com/vlxKj9M.gif)

#### Passo 3 

Seguir o tutorial de edicação do episódio feito pelo LabRI: [Acesse aqui](https://apoio.labriunesp.org/docs/geral/comunicacao/tutorial-pod-ri). 

#### Passo 4 

Fazer o upload do vídeo no Youtube 


