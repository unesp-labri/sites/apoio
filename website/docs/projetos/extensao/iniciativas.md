---
id: iniciativas
title: Iniciativas
sidebar_label: Iniciativas
slug: /projetos/extensao/iniciativas
---

<center>
    <img src="/img/projetos/extensao/logo-projetos-extensao.svg" alt="centered image" />
</center>

- [Pandemia e as Relações Internacionais](https://labriunesp.org/docs/projetos/extensao/covid19/intro)
- [Pod-RI](https://labriunesp.org/docs/projetos/extensao/pod-ri/intro)
