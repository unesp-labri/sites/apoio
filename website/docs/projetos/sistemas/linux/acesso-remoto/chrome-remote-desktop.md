---
id: chrome-remote-desktop
title: Chrome Remote Desktop
sidebar_label: Chrome Remote Desktop
slug: /projetos/sistemas/linux/acesso-remoto/chrome-remote-desktop
---



## Instalar Navegador - Google Chrome


```
sudo apt-get update && wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb

sudo apt-get update && sudo dpkg -i google-chrome-stable_current_amd64.deb

rm google-chrome*

```


# Instalar Chrome Remote Desktop



```
sudo apt-get update && wget https://dl.google.com/linux/direct/chrome-remote-desktop_current_amd64.deb
sudo apt-get update && sudo apt-get install --assume-yes ./chrome-remote-desktop_current_amd64.deb

rm chrome-remote-desktop*

```

- https://cloud.google.com/architecture/chrome-desktop-remote-on-compute-engine


## Acesso via SSH

Quando o acesso a área de trabalho remota acessada via Chrome Remote Desktop é interropido ou esta inativo você tem duas opções para reativa-lo.

A primeira opção é via x2go reativar o acesso via  Chrome Remote Desktop. A segunda opção é reativar o acesso via Chrome Remote Desktop a partir de acesso SSH

:::info

Necessário acesso admistrativo para realizar os procedimentos abaixo. Quando possivel prefira fazer este procedimento via x2go 

:::



### Etapa 1: Acesso via SSH

- Substitua `<user>` pelo seu usuário

```
ssh <user>@lantrivm01.lantri.org

```

### Etapa 2: SystemD


- ação: `status`, `stop`, `start`, `restart`
- substitua a palavra `<ação>` por uma das ações indicadas no item anterior

```
sudo systemctl <ação> chrome-remote-desktop@$USER

```

### Etapa 3: Refazer acesso via SSH

:::info

Caso a etapa 2 tenha sido bem sucedida esta etapa não será necessária

:::

- Acesse a [Area de trabalho remota do Google Chrome - configurar por SSH](https://remotedesktop.google.com/headless)
- Clicar  `começar`>> `Próxima` >> `Autorizar`
- Copiar o comando indicado em `Debian Linux`
- Colar o comando indicado em `Debian Linux` no terminal acesso via SSH
