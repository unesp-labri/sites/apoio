---
id: x2go
title: X2GO
sidebar_label: X2GO
slug: /projetos/sistemas/linux/acesso-remoto/x2go
---


## Instalando 

```
sudo add-apt-repository ppa:x2go/stable -y

sudo apt-get update && sudo apt-get install x2goclient -y


```

##  Configurando o X2GO para acesso via chave pública

#### Criar usuário

- antes de gerar a chave ssh para configurar seu acesso, é necessário estar logado pelo usuário `labri`
- caso o usuário ainda não exista, crie o usuário através do comando:

```
sudo adduser <nome_usuário>
```

- senha: xxxxx (verificar com responsável)

#### Troca de usuário

```
su labri

```

#### Localização e deslocamento

- ao trocar de usuário no terminal, é necessário verificar a pasta atual com `pwd`
- em seguida, vá para a pasta raíz do atual usuário com o comando `cd` antes de gerar a chave ssh

```
pwd

cd
```

![terminal_comando_local](/img/projetos/sistemas/terminal_comando_local.png)


#### Gerar chave privada e pública ssh  

``` 
ssh-keygen -b 2048 -t rsa -v
```

#### Enviar chave pública para o computador de destino

- em `<nome_pc>`, inserir o nome atual do computador

``` 
ssh-copy-id labri_<nome_pc>@lantrivm01.lantri.org
``` 

- Digite a senha do usuário

![chave-ssh-x2go2](/img/projetos/sistemas/chave-ssh-x2go2.png)


### Configurando acesso pelo X2GO

![chave-ssh-x2go](/img/projetos/sistemas/chave-ssh-x2go.png)


### Deixando o Acesso Remoto funcional

- Ao terminar de configurar o Acesso Remoto pelo X2Go, é necessário deixá-lo funcional 
- Para isso, realize o acesso e adicione os atalhos "Google Chrome" e "Recoll" na área de trabalho, como mostram as imagens abaixo

![print_1_x2go](/img/projetos/sistemas/print_1_x2go.png)

![print_2_x2go](/img/projetos/sistemas/print_2_x2go.png)

![print_3_x2go](/img/projetos/sistemas/print_3_x2go.png)