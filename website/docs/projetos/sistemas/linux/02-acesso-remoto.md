---
id: howto
title: Configuração de acesso remoto
sidebar_label: Acesso Remoto
slug: /projetos/sistemas/linux/howto
---


### Chrome remoto Desktop

- [PolicyKit](https://wiki.debian.org/PolicyKit)
- [](https://www.freedesktop.org/software/polkit/docs/0.105/)
- [polkit — Authorization Manager](https://www.freedesktop.org/software/polkit/docs/latest/polkit.8.html)
- [How to Install Chrome Remote Desktop on Ubuntu 18.04](https://medium.com/@vsimon/how-to-install-chrome-remote-desktop-on-ubuntu-18-04-52d99980d83e)
- [How to get Barrier and Chrome Remote Desktop working in Ubuntu 20.04 (Focal Fossa)](https://github.com/GObaddie/ubuntu_chrome_remote_desktop)
- http://c-nergy.be/blog/?p=13641
- https://linuxconfig.org/configure-sudo-without-password-on-ubuntu-20-04-focal-fossa-linux
- https://aus800.com.au/remote-desktop-to-ubuntu-from-windows/
- https://www.admin-magazine.com/Articles/Assigning-Privileges-with-sudo-and-PolicyKit
- https://www.golinuxcloud.com/run-systemd-service-specific-user-group-linux/
- https://unix.stackexchange.com/questions/504806/systemd-start-as-unprivileged-user-in-a-group
- https://unix.stackexchange.com/questions/507975/systemd-service-management-using-pkla-equivalents-to-polkits-rules-on-debian
- https://serverfault.com/questions/841306/authentication-is-required-to-manage-system-services-or-units
- https://unix.stackexchange.com/questions/496982/restarting-systemd-service-only-as-a-specific-user
- https://archive.is/fiE4y


### Ambiente Virtual - Conda

#### Instalar Anaconda em distribuição LINUX


```

# criação de pasta de instalação acessivel a todos os usuários

sudo mkdir /opt/anaconda
sudo chmod ugo+w /opt/anaconda

# Instalação de dependencias

sudo apt install libgl1-mesa-glx libegl1-mesa libxrandr2 libxrandr2 libxss1 libxcursor1 libxcomposite1 libasound2 libxi6 libxtst6

# Download do arquivo de instalação
wget https://repo.anaconda.com/archive/Anaconda3-2021.05-Linux-x86_64.sh

# Instalação (indicar caminho de instalação como "/opt/anaconda" após rodar o comando abaixo)

bash Anaconda3-2021.05-Linux-x86_64.sh -u 


# Verificação

which python

# Instalação acessivel a todos os users

sudo chgrp -R chrome-remote-desktop /opt/anaconda
sudo chmod 770 -R /opt/anaconda

# Configurar inicialização do conda 

# retirado de .bashrc do user que instalou o conda e colocado no /etc/profile
# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/opt/anaconda/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/opt/anaconda/etc/profile.d/conda.sh" ]; then
        . "/opt/anaconda/etc/profile.d/conda.sh"
    else
        export PATH="/opt/anaconda/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<



```

```
conda init

```

#### Criar e ativar ambiente conda

```
conda config --set auto_activate_base false # desativar startup conda terminal
conda init # para inicializar o conda

which python

conda list

conda create --name env_projeto_A

conda activate env_projeto_A

```


#### 


```
conda install python
conda install -n env_projeto_A scipy

conda create --name env_projeto_A python=3.7 scipy pandas=0.25.3

conda info --envs # verificar ambiente existentes

conda env export > environment.yml # exportar as configs

conda env create -f environment.yml # criar um ambiente a partir de um arquivo de ambiente previamente criado

conda deactivate # desativer ambiente

 conda remove --name your_env_name --all # Remover ambiente conda


```




### Material de apoio

- https://archive.is/FTPEi

- https://archive.is/4uu2X




### VSCode


```

# instalar dependencias
sudo apt update
sudo apt install software-properties-common apt-transport-https wget

# add Microsoft GPG key
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -

# And enable the Visual Studio Code repository by typing

sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"

# instalar

sudo apt install code




```

 - Material de apoio
   - https://archive.is/ZgQrD


