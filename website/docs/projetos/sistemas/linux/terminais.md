---
id: terminais
title: Terminal
sidebar_label: Terminal
slug: /projetos/sistemas/linux/terminais
---



## Alterar nome do computador

### Verifivar nome atual

``` 
hostnamectl status
```

### Novo nome do computador

- substitua `<novo_nome>` pelo nome que deseja dar ao computador

``` 
hostnamectl set-hostname <novo_nome>
```

## Usuários

### Criar usuário

```
sudo adduser <nome_usuário>
```

### Trocar de usuário

```
su <nome_usuário>
```


## Formatação de disco rigido (HD)

```

# identificar HD

lsblk ou fdisk -l

##Particionar o HD que será adicionado
sudo cfdisk /dev/sdc


## formatar hd para ext4
sudo mkfs.ext4 /dev/sdc1
sudo umount /dev/sdc1

#criar diretorio de apontamento
sudo mkdir /media/hdvm12

## montagem na iniicialização do sistema
blkid

echo "UUID=40a445a9-eac6-48c6-b019-96ef1446e491 /media/hdvm12 ext4 errors=remount-ro 0 1" >> /etc/fstab

sudo mount /hdreal02

sudo chmod 777 /caminho/de/montagem/do/hd


```

## Informações de Hardware

### Informações do processador do computador

```
sudo dmidecode -t processor
```

### Lista de HDs instalados no computador

```
lsblk  
```

### Descrição das informações do HD

```
sudo smartctl -d ata -a -i /dev/sda  
```


