---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/sistemas/linux/intro
---


Esta página reúne configurações feitas nos computadores localizados na sala do LlabRI/UNESP

- [Install MATE Desktop Environment on Debian 11](https://techviewleo.com/install-mate-desktop-environment-on-debian/)
- [Error al iniciar Linux - initramfs](https://youtu.be/I_Nnq9HDQrA)


## Entrar na BIOS

- Para entrar na BIOS dos computadores Dell, primeiro clique em F12
(inserir imagem)

- A seguir, selecione a opção BIOS SETUP como mostra a imagem abaixo
(inserir imagem)

## Terminar todos os processos de um usuário

```
sudo kill -9 `ps -fu lantri_marcelo |awk '{ print $2 }'|grep -v PID`

```


## Resolver "/usr/bin/dpkg returned an error" In Ubuntu

```
sudo dpkg --configure -a
sudo apt install --fix-broken
sudo rm -rf /var/lib/dpkg/info/*.*
sudo apt remove --purge package_name
sudo apt clean
sudo apt autoremove

sudo apt update

```

### Material de apoio

- [How to Solve “Sub-process /usr/bin/dpkg returned an error code (1)” In Ubuntu](https://www.tecmint.com/sub-process-usr-bin-dpkg-returned-an-error-in-ubuntu/)
- [apt - E: Sub-process /usr/bin/dpkg returned an error code (1)](https://www.vivaolinux.com.br/dica/apt-E-Sub-process-usrbindpkg-returned-an-error-code-1-Resolvido)






