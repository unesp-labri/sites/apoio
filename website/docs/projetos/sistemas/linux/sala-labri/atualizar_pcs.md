---
id: atualizar-pcs
title: Atualizar e Limpar sistema operacional
sidebar_label: Atualizar PCs
slug: /projetos/sistemas/linux/sala-labri/atualizar-pcs
---

Esta página reúne configurações feitas nos computadores localizados na sala do LlabRI/UNESP

## Atualização dos PCs

```
sudo apt update && sudo apt upgrade -y

```

## Limpeza sistema PCs


```

sudo apt autoremove -y && sudo apt autoclean -y && sudo apt clean -y

```

### Material de apoio

- https://elias.praciano.com/2014/08/apt-get-quais-as-diferencas-entre-autoremove-autoclean-e-clean/
