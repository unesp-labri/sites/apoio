---
id: container
title: Container
sidebar_label: Container
slug: /projetos/sistemas/vm-container/container
---


## Material de Apoio

- [Buildpacks vs Dockerfiles - Exploring the tradeoffs of building container images at scale](https://technology.doximity.com/articles/buildpacks-vs-dockerfiles)
