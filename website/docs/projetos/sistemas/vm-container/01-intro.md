---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/sistemas/vm-container/intro
---

- https://crossplane.io/
- [Elasticsearch para leigos Criando um cluster e entendendo os principais conceitos](https://thiagomont-portifolio.gitlab.io/portifolio/post/0008-elasticsearch-para-leigos/)
- https://www.pulumi.com/
- https://medium.com/trainingcenter/localstack-testando-servi%C3%A7os-aws-7f9f24de293c
- https://explore.skillbuilder.aws/learn

- https://blauaraujo.com/livro/

- systemd
- https://serverfault.com/questions/628610/increasing-nproc-for-processes-launched-by-systemd-on-centos-7
- https://askubuntu.com/questions/659267/how-do-i-override-or-configure-systemd-services



- caroussel

```
https://swiperjs.com/
https://stitches.dev/
https://www.embla-carousel.com/examples/basic/
https://owlcarousel2.github.io/OwlCarousel2/demos/demos.html
https://stackshare.io/playwright/alternatives

```

