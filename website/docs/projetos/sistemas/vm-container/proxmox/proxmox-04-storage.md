---
id: storage
title: Storage
sidebar_label: Storage
slug: /projetos/sistemas/vm-container/proxmox/storage
---



## Localização de iso

```
/var/lib/vz/template/iso


## formatar

cfdisk /dev/sdd


##fixar nomes hds

ls /dev/disk/by-id/

nano /etc/zfs/vdev_id.conf

alias d01 wwn-0x5000c50071410e3f
alias d02 wwn-0x5000c500713e9223

```

## ZFS

- [ZFS: Usando Scrub para Ver Se o Pool Está Íntegro](https://www.gnulinuxbrasil.com.br/2021/02/11/zfs-usando-scrub-para-ver-se-o-pool-esta-integro-parte-14/)

```
apt-get install zfsutils zfs-initramfs zfs-zed

apt-get install parted lsscsi


zpool create -f -o ashift=12 <nome_pool> <device1> <device2>
zpool create -f -o ashift=12 zfs-rpl02 d01 d02

zpool list
zpool status zfs-rpl02
zfs list
tree /zfs-rpl02

```


```
zfs create <nome_pool>/vms
zfs create zfs-rpl02/vms

zfs get compression <nome_pool>

zfs set compression=on <nome_pool>
zfs set compression=on zfs-rpl02/vms

zfs set compression=<algorithm> <nome_pool/fs>
zfs set compression=lz4 zfs-rpl02/vms


```


```
pvesm zfsscan
zpool iostat -v
```


```
## interface grafica
Datacenter > Storage > Add > ZFS

enable - ok
thin provision - ok
block size 8k - ok

https://forum.proxmox.com/threads/enable-sparse-on-existing-zfs-storage.74078/
https://forum.proxmox.com/threads/adding-zfs-pool-with-ashift-12-which-block-size-option.46594/
https://forum.proxmox.com/threads/problem-with-volblocksize.83193/


```


```
/ - 51G
/boot - 1GB
/home - 52GB
/swapp - 16GB

/250GB

https://diolinux.com.br/sistemas-operacionais/como-instalar-o-zram-no-ubuntu-e-outras-dicas-para-melhorar-o-desempenho.html

```


```
https://wiki.archlinux.org/title/ZFS_(Portugu%C3%AAs)
https://youtu.be/71o6Tyh2MUk
https://pve.proxmox.com/wiki/Storage:_ZFS
https://pve.proxmox.com/wiki/ZFS_on_Linux#zfs_compression
https://pve.proxmox.com/wiki/ZFS:_Tips_and_Tricks
https://openzfs.github.io/openzfs-docs/Project%20and%20Community/FAQ.html?highlight=vdev_id#setting-up-the-etc-zfs-vdev-id-conf-file
https://tbellembois.github.io/ZFS_debian.html



```

## NFS

https://www.dobitaobyte.com.br/network-file-system-nfs/

## IO Delay

- https://forum.proxmox.com/threads/high-io-delay.89599/
- https://forum.proxmox.com/threads/very-high-io-delay-on-any-load.37693/
- https://forum.level1techs.com/t/zfs-high-i-o-issue-txg-sync-100-and-z-trim-int-60-to-70/163166/38

