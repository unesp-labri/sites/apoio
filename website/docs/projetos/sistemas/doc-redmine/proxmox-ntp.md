---
id: proxmox-ntp
title: Proxmox NTP
sidebar_label: Proxmox NTP
slug: /projetos/sistemas/doc-redmine/proxmox-ntp
---


# Habilitar Network Time Protocol (NTP)

1. Sincronizar as datas através do comando abaixo:

```
date --set=hh:mm:ss
```


2. Sincronizar a data da BIOS com a data do sistema:

```
hwclock -w
```


3. Instalar o NTP:

```
apt-get install ntp ntpdate nmap
```


4. Verificar se a porta NTP está aberta:

```
nmap -p123 -sU -P0 localhost
```


5. Caso a porta esteja fechada, digite o seguinte comando:

```
ufw allow 123/udp
```


6. Substituir a linha que contém o NTP padrão pelo NTP da Unesp (ntp.unesp.br):

```
nano /etc/systemd/timesyncd.conf
```


Obs.: Retirar a "#" da linha do Server. Esse linha ficará assim:

```
Servers=ntp.unesp.br
```


7. Criar arquivo NTP:

```
touch /etc/ntp.drift
```


8. Editar o documento NTP abaixo:

```
nano /etc/ntp.conf
```


9. Substituir o NTP Server padrão (provavelmente será algo parecido com "server ntp.debian.org") pelo indicado abaixo:

```
server ntp.unesp.br iburst
```


10. Reiniciar o serviço de NTP:

```
/etc/init.d/ntp restart
```


11. Verificar se NTP está habilitado:

```
ntpq -c lpeer
```


12. Verificar sincronização com servidor NTP:

```
ntpdate -u ntp.unesp.br
```

