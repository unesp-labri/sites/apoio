---
id: debian-proxmox  
title: Debian Proxmox
sidebar_label: Debian Proxmox
slug: /projetos/sistemas/doc-redmine/debian-proxmox
---


## Instalação do Proxmox VE no Debian Buster

* * *



## Instalar Debian Buster

* * *



### Verificação do hostname do Debian Stretch

2.1\. Add an /etc/hosts entry for your IP address

2.2\. Confirmar IP

```
hostname --ip-address
```

```
127.0.0.1    localhost

200.145.122.123 pve01.lantri.org pve01 pvelocalhost
200.145.122.118 pve02.lantri.org pve02 pvelocalhost
200.145.122.121 pve03.lantri.org pve03 pvelocalhost
200.145.122.111 pve04.lantri.org pve04 pvelocalhost
200.145.122.120 pve05.lantri.org pve05 pvelocalhost
200.145.122.122 pve06.lantri.org pve06 pvelocalhost

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters

```

* * *


### Instalar o Proxmox VE

Habilitar Proxmox VE No-Subscription Repository (Adapt your sources.list)

```
nano /etc/apt/sources.list
```

```
# deb cdrom:[Debian GNU/Linux 9.1.0 _Stretch_ - Official amd64 DVD Binary-1 20170722-11:31]/ stretch contrib main
```

```
###
### Proxmox
###

deb http://ftp.debian.org/debian buster main contrib
deb http://ftp.debian.org/debian buster-updates main contrib

# PVE pve-no-subscription repository provided by proxmox.com,
# NOT recommended for production use
deb http://download.proxmox.com/debian/pve buster pve-no-subscription

# security updates
deb http://security.debian.org/debian-security buster/updates main contrib

```

Add the Proxmox VE repository key:

```
wget http://download.proxmox.com/debian/proxmox-ve-release-6.x.gpg -O /etc/apt/trusted.gpg.d/proxmox-ve-release-6.x.gpg
chmod +r /etc/apt/trusted.gpg.d/proxmox-ve-release-6.x.gpg
```

Update your repository and system by running

```
apt update && apt full-upgrade
apt update && apt dist-upgrade
```



#### Install Proxmox VE packages

```
apt install proxmox-ve postfix open-iscsi
```



#### remove the os-prober package

```
apt remove os-prober
```


#### Deabilitar Proxmox VE Enterprise Repository[¶](#Deabilitar-Proxmox-VE-Enterprise-Repository)

```
nano /etc/apt/sources.list.d/pve-enterprise.list
```

```
# deb https://enterprise.proxmox.com/debian/pve stretch pve-enterprise
```



#### Connect to the Proxmox VE web interface

Once logged in, create a Linux Bridge called vmbr0, and add your first network interface to it.

* * *

Fontes:  
[https://pve.proxmox.com/wiki/Install_Proxmox_VE_on_Debian_Buster](https://pve.proxmox.com/wiki/Install_Proxmox_VE_on_Debian_Buster)  
[https://pve.proxmox.com/wiki/Install_Proxmox_VE_on_Debian_Stretch](https://pve.proxmox.com/wiki/Install_Proxmox_VE_on_Debian_Stretch)  
[https://pve.proxmox.com/wiki/Package_Repositories](https://pve.proxmox.com/wiki/Package_Repositories)  
[https://pve.proxmox.com/wiki/Downloads#Proxmox_Virtual_Environment_5.1_.28ISO_Image.29](https://pve.proxmox.com/wiki/Downloads#Proxmox_Virtual_Environment_5.1_.28ISO_Image.29)

* * *
