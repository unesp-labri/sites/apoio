---
id: cluster-ssh
title: Cluster SSH
sidebar_label: Cluster SSH
slug: /projetos/sistemas/doc-redmine/cluster-ssh
---
## CLUSTER SSH

### Instalação

É necessário instalar o clusterssh em todas as máquinas que serão utilizadas. Em distribuições baseadas no Debian, digite:
```
 sudo apt install clusterssh 
```
### Configuração

Caso não tenha instalado na máquina em que fará as configurações, digite o seguinte comando para instalar o editor de textos Vim:
```
 sudo apt install vim 
```
É necessário criar um arquivo em /etc com as informações das máquinas que farão parte do(s) cluster(s). Abra o terminal (Ctrl+Alt+T) e digite o seguinte comando:
```
 sudo vim /etc/clusters 
```
Um arquivo de edição no Vim aparecerá. Edite de acordo com suas necessidades, por exemplo:
```
 cluster1 login@ip1 login@ip2 login@ip3
 cluster2 login@ip4 login@ip5 login@ip6 
```
- O login sempre deve ser de um usuário com permissões de administrador.
No caso do LabRI, editei três clusters que acessam o usuário labri_estagiario:

- Computadores Itautec - 108 109 113 114 115 117
- Computadores HP - 97 98 99 102 104 105
- Computadores Itautec e HP - 97 98 99 102 104 105 108 109 113 114 115 117
```
cluster1 labri_estagiario@200.145.122.108 labri_estagiario@200.145.122.117 labri_estagiario@200.145.122.115 labri_estagiario@200.145.122.114 labri_estagiario@200.145.122.109 labri_estagiario@200.145.122.113
cluster2 labri_estagiario@200.145.122.105 labri_estagiario@200.145.122.97 labri_estagiario@200.145.122.98 labri_estagiario@200.145.122.102 labri_estagiario@200.145.122.104 labri_estagiario@200.145.122.99
cluster3 labri_estagiario@200.145.122.97 labri_estagiario@200.145.122.98 labri_estagiario@200.145.122.99 labri_estagiario@200.145.122.102 labri_estagiario@200.145.122.104 labri_estagiario@200.145.122.105 labri_estagiario@200.145.122.108 labri_estagiario@200.145.122.109 labri_estagiario@200.145.122.113 labri_estagiario@200.145.122.114 labri_estagiario@200.145.122.115 labri_estagiario@200.145.122.117 
```

### Executando
```
 cssh -l dev clusterX 
```
Sendo 'X' o número do cluster.

Caso queira acessar outros clusters, basta substituir no comando supracitado.


### Exceções
Computadores com Proxmox não devem ser colocados no cluster.
```
 111 118 120 121 123 
```
OBS: Atualmente o Rafael é responsável pelas atualizações das máquinas com ProxMox. Qualquer questão sobre isso entrar em contato com ele.


### Informações adicionais

[Atualização dos computadores](http://tarefas.lantri.org/projects/lantri-39/wiki/Atualiza%C3%A7%C3%A3o_dos_computadores)

- #2225
