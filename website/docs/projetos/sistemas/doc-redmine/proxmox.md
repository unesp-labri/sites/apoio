---
id: proxmox
title: Proxmox
sidebar_label: Proxmox
slug: /projetos/sistemas/doc-redmine/proxmox
---


# ProxMox - Servidor de Virtualização

* * *

[Estrutura Geral - MindMup](https://drive.mindmup.com/map/0BwquYFt95odqLVVKZ3ByZHUxcFE)

```
[Inclusão de Disco Rígido Virtual](06-01.html)

* * *

[06-05-debian-proxmox](06-05-debian-proxmox.html)  
[06-06-proxmox-ntp](06-06-proxmox-ntp.html)  
[06-07-proxmox-debian-buster](06-07-proxmox-debian-buster.html)  
[ZFS](ZFS.html)

* * *
```


# Instalação Proxmox

Os passos abaixo indicam como instalar o Proxmox como um sistema operacional de gerenciamento de máquinas virtuais. Em outras palavras, é apontado como instalá-lo em máquinas reais.

```
*   [ProxMox - Servidor de Virtualização](#ProxMox-Servidor-de-Virtualização)
*   [Instalação Proxmox](#Instalação-Proxmox)
    *   [Passo 1: Verificar se o processador tem suporte para virtualização](#Passo-1-Verificar-se-o-processador-tem-suporte-para-virtualização)
    *   [Passo 2: Antes de usar o CD, verificar informações da rede.](#Passo-2-Antes-de-usar-o-CD-verificar-informações-da-rede)
    *   [Passo 3: Na instalação com o CD, seguir os procedimentos abaixo:](#Passo-3-Na-instalação-com-o-CD-seguir-os-procedimentos-abaixo)
    *   [Passo 4: Habilitar atualização do ProxMox](#Passo-4-Habilitar-atualização-do-ProxMox)
    *   [Passo 5: Habilitar Network Time Protocol (NTP)](#Passo-5-Habilitar-Network-Time-Protocol-NTP)
    *   [Passo 6: Habilitar Servidor X-11 no Proxmox](#Passo-6-Habilitar-Servidor-X-11-no-Proxmox)
    *   [Passo 7: Desabilitar SSH do root](#Passo-7-Desabilitar-SSH-do-root)
*   [Instalação ProxMox via USB](#Instalação-ProxMox-via-USB)
    *   [Passo 1: localizar o dispositivo USB](#Passo-1-localizar-o-dispositivo-USB)
    *   [Passo 2: instalar o proxmox no USB](#Passo-2-instalar-o-proxmox-no-USB)
*   [Visão geral sobre o Proxmox](#Visão-geral-sobre-o-Proxmox)
    *   [O que é o Proxmox? E para que serve?](#O-que-é-o-Proxmox-E-para-que-serve)
    *   [Explicação sobre os recursos disponíveis na interface web do Proxmox](#Explicação-sobre-os-recursos-disponíveis-na-interface-web-do-Proxmox)
*   [Criação de máquinas virtuais](#Criação-de-máquinas-virtuais)
    *   [Instalação em modo OEM (para template)](#Instalação-em-modo-OEM-para-template)
*   [Estrutura de diretórios](#Estrutura-de-diretórios)
*   [Gerenciamento das máquinas virtuais](#Gerenciamento-das-máquinas-virtuais)
*   [Gerenciamento de disco rígido (HDs)](#Gerenciamento-de-disco-rígido-HDs)
*   [1. Disco Rígidos Reais](#1-Disco-Rígidos-Reais)
    *   [Adicionando o HD](#Adicionando-o-HD)
        *   [Passo 1: Digite o comando abaixo no terminal (como usuário sudo) e identifique o hd que será habilitado (você precisará do caminho do HD "/dev..." no proximo passo, então anote esta informação)](#Passo-1-Digite-o-comando-abaixo-no-terminal-como-usuário-sudo-e-identifique-o-hd-que-será-habilitado-você-precisará-do-caminho-do-HD-dev-no-proximo-passo-então-anote-esta-informação)
        *   [Passo 2: Digite Digite o comando abaixo no terminal para formatar e particionar o HD](#Passo-2-Digite-Digite-o-comando-abaixo-no-terminal-para-formatar-e-particionar-o-HD)
        *   [Passo 2: Digite o comando abaixo no terminal para criar o sistema de arquivos (substitua o /dev..., pelo o identificado no passo 1)](#Passo-2-Digite-o-comando-abaixo-no-terminal-para-criar-o-sistema-de-arquivos-substitua-o-dev-pelo-o-identificado-no-passo-1)
        *   [Passo 3: Digite o comando abaixo no terminal para criar uma pasta com o nome do seu hd para que o mesmo seja montado.](#Passo-3-Digite-o-comando-abaixo-no-terminal-para-criar-uma-pasta-com-o-nome-do-seu-hd-para-que-o-mesmo-seja-montado)
        *   [Passo 4: Digite o comando abaixo no terminal para identificar a partição](#Passo-4-Digite-o-comando-abaixo-no-terminal-para-identificar-a-partição)
        *   [Passo 5: Digite o comando abaixo no terminal para habilitar o autostart do HD](#Passo-5-Digite-o-comando-abaixo-no-terminal-para-habilitar-o-autostart-do-HD)
        *   [Passo 6: acrescente a linha abaixo no final do arquivo aberto com o editor nano. Substitua as informações do UUID e do diretório em que o HD será montado pelas informações de você obteve nos passos anteriores)](#Passo-6-acrescente-a-linha-abaixo-no-final-do-arquivo-aberto-com-o-editor-nano-Substitua-as-informações-do-UUID-e-do-diretório-em-que-o-HD-será-montado-pelas-informações-de-você-obteve-nos-passos-anteriores)
        *   [Passo 8: reiniciar a máquina](#Passo-8-reiniciar-a-máquina)
        *   [Passo 9: dar permissão de acesso](#Passo-9-dar-permissão-de-acesso)
    *   [Adicionando o HD Físico ao Proxmox](#Adicionando-o-HD-Físico-ao-Proxmox)
*   [1. Discos Rígidos Virtuais](#2-Discos-Rígidos-Virtuais)
    *   [Migraçao de OVA para proxmox](#Migraçao-de-OVA-para-proxmox)
*   [1. Gerenciamento de Storage](#3-Gerenciamento-de-Storage)
    *   [Logical Volume Manager (LVM)](#Logical-Volume-Manager-LVM)
        *   [Introdução](#Introdução)
            *   [Instalar LVM2](#Instalar-LVM2)
            *   [Listar discos e partições](#Listar-discos-e-partições)
            *   [Criar Partições](#Criar-Partições)
            *   [Mudar tipos de partições](#Mudar-tipos-de-partições)
            *   [Listar partições para verificar as alterações](#Listar-partições-para-verificar-as-alterações)
        *   [Volumes Físicos](#Volumes-Físicos)
            *   [Criando os volumes físicos](#Criando-os-volumes-físicos)
            *   [Verificar ps volumes fícisos criados](#Verificar-ps-volumes-fícisos-criados)
        *   [Grupo de volume](#Grupo-de-volume)
            *   [Criando Grupo de volume](#Criando-Grupo-de-volume)
            *   [Verificar os grupos de volumes criados](#Verificar-os-grupos-de-volumes-criados)
        *   [Criar volumes lógico](#Criar-volumes-lógico)
            *   [Verificar volume lógico criado](#Verificar-volume-lógico-criado)
            *   [Renomear volume lógico](#Renomear-volume-lógico)
        *   [Formatar Volume lógico](#Formatar-Volume-lógico)
        *   [Montar volumes lógicos](#Montar-volumes-lógicos)
            *   [Criar diretório](#Criar-diretório)
            *   [Criando o Ponto de montagem](#Criando-o-Ponto-de-montagem)
        *   [Expandir volume lógico](#Expandir-volume-lógico)
        *   [Diminuir volume lógico](#Diminuir-volume-lógico)
        *   [Montagem do volumes lógicos no FSTAB](#Montagem-do-volumes-lógicos-no-FSTAB)
            *   [Listar volumes lógicos](#Listar-volumes-lógicos)
            *   [Abrir FSTAB](#Abrir-FSTAB)
            *   [Forçar o montagem de todas as partições](#Forçar-o-montagem-de-todas-as-partições)
        *   [Estendendo um grupo de volume (inclusão de HD)](#Estendendo-um-grupo-de-volume-inclusão-de-HD)
            *   [Listar os discos](#Listar-os-discos)
*   [1. Raid](#4-Raid)
*   [1. Tabela](#5-Tabela)
*   [Gerenciamento de rede](#Gerenciamento-de-rede)
*   [Gerenciamento de Usuários e Grupos](#Gerenciamento-de-Usuários-e-Grupos)
*   [Formação de cluster](#Formação-de-cluster)
*   [Ajustando a rede de MULTICAST para UNICAST](#Ajustando-a-rede-de-MULTICAST-para-UNICAST)
    *   [Passo 5: Habilitar Network Time Protocol (NTP)](#Passo-5-Habilitar-Network-Time-Protocol-NTP-2)
*   [Utilização do Spice no Proxmox](#Utilização-do-Spice-no-Proxmox)
*   [Instalação de softwares nas máquinas reais com ProxMox](#Instalação-de-softwares-nas-máquinas-reais-com-ProxMox)
    *   [1. Configuracao do sistema para portugues:](#1-Configuracao-do-sistema-para-portugues)
    *   [1. Configuracao do teclado para portugues](#2-Configuracao-do-teclado-para-portugues)
    *   [1. Instalacao do LibreOffice](#3-Instalacao-do-LibreOffice)
*   [Adicionando o HD](#Adicionando-o-HD-2)
    *   [Passo 1: Digite o comando abaixo no terminal (como usuário sudo) e identifique o hd que será habilitado (você precisará do caminho do HD "/dev..." no proximo passo, então anote esta informação)](#Passo-1-Digite-o-comando-abaixo-no-terminal-como-usuário-sudo-e-identifique-o-hd-que-será-habilitado-você-precisará-do-caminho-do-HD-dev-no-proximo-passo-então-anote-esta-informação-2)
    *   [Passo 2: Digite Digite o comando abaixo no terminal para formatar e particionar o HD](#Passo-2-Digite-Digite-o-comando-abaixo-no-terminal-para-formatar-e-particionar-o-HD-2)
    *   [Passo 2: Digite o comando abaixo no terminal para criar o sistema de arquivos (substitua o /dev..., pelo o identificado no passo 1)](#Passo-2-Digite-o-comando-abaixo-no-terminal-para-criar-o-sistema-de-arquivos-substitua-o-dev-pelo-o-identificado-no-passo-1-2)
    *   [Passo 3: Digite o comando abaixo no terminal para criar uma pasta com o nome do seu hd para que o mesmo seja montado.](#Passo-3-Digite-o-comando-abaixo-no-terminal-para-criar-uma-pasta-com-o-nome-do-seu-hd-para-que-o-mesmo-seja-montado-2)
    *   [Passo 4: Digite o comando abaixo no terminal para identificar a partição](#Passo-4-Digite-o-comando-abaixo-no-terminal-para-identificar-a-partição-2)
    *   [Passo 5: Digite o comando abaixo no terminal para habilitar o autostart do HD](#Passo-5-Digite-o-comando-abaixo-no-terminal-para-habilitar-o-autostart-do-HD-2)
    *   [Passo 6: acrescente a linha abaixo no final do arquivo aberto com o editor nano. Substitua as informações do UUID e do diretório em que o HD será montado pelas informações de você obteve nos passos anteriores)](#Passo-6-acrescente-a-linha-abaixo-no-final-do-arquivo-aberto-com-o-editor-nano-Substitua-as-informações-do-UUID-e-do-diretório-em-que-o-HD-será-montado-pelas-informações-de-você-obteve-nos-passos-anteriores-2)
    *   [Passo 8: reiniciar a máquina](#Passo-8-reiniciar-a-máquina-2)
    *   [Passo 9: dar permissão de acesso](#Passo-9-dar-permissão-de-acesso-2)
*   [Adicionando o HD](#Adicionando-o-HD-3)
    *   [Passo 1: Digite o comando abaixo no terminal (como usuário sudo) e identifique o hd que será habilitado (você precisará do caminho do HD "/dev..." no proximo passo, então anote esta informação)](#Passo-1-Digite-o-comando-abaixo-no-terminal-como-usuário-sudo-e-identifique-o-hd-que-será-habilitado-você-precisará-do-caminho-do-HD-dev-no-proximo-passo-então-anote-esta-informação-3)
    *   [Passo 2: Digite Digite o comando abaixo no terminal para formatar e particionar o HD](#Passo-2-Digite-Digite-o-comando-abaixo-no-terminal-para-formatar-e-particionar-o-HD-3)
    *   [Passo 2: Digite o comando abaixo no terminal para criar o sistema de arquivos (substitua o /dev..., pelo o identificado no passo 1)](#Passo-2-Digite-o-comando-abaixo-no-terminal-para-criar-o-sistema-de-arquivos-substitua-o-dev-pelo-o-identificado-no-passo-1-3)
    *   [Passo 3: Digite o comando abaixo no terminal para criar uma pasta com o nome do seu hd para que o mesmo seja montado.](#Passo-3-Digite-o-comando-abaixo-no-terminal-para-criar-uma-pasta-com-o-nome-do-seu-hd-para-que-o-mesmo-seja-montado-3)
    *   [Passo 4: Digite o comando abaixo no terminal para identificar a partição](#Passo-4-Digite-o-comando-abaixo-no-terminal-para-identificar-a-partição-3)
    *   [Passo 5: Digite o comando abaixo no terminal para habilitar o autostart do HD](#Passo-5-Digite-o-comando-abaixo-no-terminal-para-habilitar-o-autostart-do-HD-3)
    *   [Passo 6: acrescente a linha abaixo no final do arquivo aberto com o editor nano. Substitua as informações do UUID e do diretório em que o HD será montado pelas informações de você obteve nos passos anteriores)](#Passo-6-acrescente-a-linha-abaixo-no-final-do-arquivo-aberto-com-o-editor-nano-Substitua-as-informações-do-UUID-e-do-diretório-em-que-o-HD-será-montado-pelas-informações-de-você-obteve-nos-passos-anteriores-3)
    *   [Passo 8: reiniciar a máquina](#Passo-8-reiniciar-a-máquina-3)
    *   [Passo 9: dar permissão de acesso](#Passo-9-dar-permissão-de-acesso-3)
*   [Adicionando o HD Físico ao Proxmox](#Adicionando-o-HD-Físico-ao-Proxmox-2)
*   [Migraçao de OVA para proxmox](#Migraçao-de-OVA-para-proxmox-2)
```

## Passo 1: Verificar se o processador tem suporte para virtualização

É necessário verificar se o processador tem suporte para virtualização para ser possível rodar máquinas virtuais. Ao rodar o comando abaixo na máquina real verifique se constará vmx ou svm. Em caso afirmativo o processador tem suporte para virtualização.

```

egrep '(vmx|svm)' --color=always /proc/cpuinfo
```



## Passo 2: Antes de usar o CD, verificar informações da rede. Passo-2-Antes-de-usar-o-CD-verificar-informações-da-rede)

> 1. Para saber Gateway e Netmask/Genmask digite o seguinte comando na maquina real:

```

route
```

OBS: normalmente no gateway o número de IP aparece na primeira linha de sua respectiva coluna e no netmask/genmask o número de IP aparece na segunda linha de sua respectiva coluna (são números diferentes de 0.0.0.0).

> 1. Para saber DNS Server digite o seguinte comando na maquina real:

```
cat /etc/resolv.conf
```



## Passo 3: Na instalação com o CD, seguir os procedimentos abaixo:

1.  Selecionar "Install Proxmox VE"
2.  Selecional "I agree"
3.  Selecionar o disco em que o proxmox será instalado (no momento, estamos deixando todas as configurações de default, somente certificando que o sistema de arquivos é ext4 - clicar em "options" para verificar)
4.  Selecionar Country: Brazil; Time Zone: São Paulo; e Keyboard Layout: Brazil-Portuguese
5.  Indicar senha e e-mail válido para receber notificações
6.  Configuração de rede (o ProxMox faz uma verificação automática na hora da instalação, porém é importante ter estas informações em mãos para verificar se o ProxMox identificou corretamente as informações necessárias).
7.  Clique em "next" e aguarde a finalização da instalação do ProxMox
8.  Reinicie o sistema e retire o CD ou USB que utilizou para a instalação
9.  Acesse o interface web do Proxmox através no IP fornacido após a instalação
10.  Coloque o login (normalmente root) e a senha criada no momento da instalação



## Passo 4: Habilitar atualização do ProxMox


O ProxMox vem configurado para o uso com assinaturas pagas, compradas do "proxmox.com". Para uma correta atualização sem que seja necessária a compra da assinatura de suporte, alguns repositórios tem que ser alterados

1. Editar arquivo "pve-enterprise.list"

```
nano /etc/apt/sources.list.d/pve-enterprise.list
```

OBS: (comente a linha com o #, salve e saia do arquivo (Crtl+X e Y). O arquivo editado com o nano deve ficar assim :

```
# deb https://enterprise.proxmox.com/debian jessie pve-enterprise
```

1. Editar o arquivo "sources.list"

```
nano /etc/apt/sources.list
```

OBS: Adicionar a seguinte linha ao arquivo, salvar e sair do arquivo:

```
deb http://ftp.br.debian.org/debian stretch main contrib

deb http://ftp.debian.org/debian stretch main contrib

# PVE pve-no-subscription repository provided by proxmox.com,
# NOT recommended for production use
deb http://download.proxmox.com/debian/pve stretch pve-no-subscription

# security updates
deb http://security.debian.org stretch/updates main contrib
```

1. Fazer a primeira atualização do Proxmox:

```
apt-get update && apt-get upgrade
```



## Passo 5: Habilitar Network Time Protocol (NTP)


1. Sincronizar as datas através do comando abaixo:

```
date --set=hh:mm:ss
```

1. Sincronizar a data da BIOS com a data do sistema:

```
hwclock -w
```

1. Instalar o NTP:

```
apt-get install ntp ntpdate
```

1. Verificar se a porta NTP está aberta:

```
nmap -p123 -sU -P0 localhost
```

1. Caso a porta esteja fechada, digite o seguinte comando:

```
ufw allow 123/udp
```

1. Substituir a linha que contém o NTP padrão pelo NTP da Unesp (ntp.unesp.br):

```
nano /etc/systemd/timesyncd.conf
```

Obs.: Retirar a "#" da linha do Server. Esse linha ficará assim:

```
Servers=ntp.unesp.br
```

1. Criar arquivo NTP:

```
touch /etc/ntp.drift
```

1. Editar o documento NTP abaixo:

```
nano /etc/ntp.conf
```

1. Substituir o NTP Server padrão (provavelmente será algo parecido com "server ntp.debian.org") pelo indicado abaixo:

```
server ntp.unesp.br iburst
```

1. Reiniciar o serviço de NTP:

```
/etc/init.d/ntp restart
```

1. Verificar se NTP está habilitado:

```
ntpq -c lpeer
```

1. Verificar sincronização com servidor NTP:

```
ntpdate -u ntp.unesp.br
```

1. Reiniciar a máquina:

```
reboot now
```


## Passo 6: Habilitar Servidor X-11 no Proxmox

1. Realizar update do repositório e do sistema com o seguinte comando:

```
aptitude update && aptitude full-upgrade
```

1. Instalar os seguintes recursos:

``` 
aptitude install lxde chromium lightdm x2goclient filezilla firefox
```

1. Antes de iniciar "x-windows" criar um usuário:

```
adduser nome_do_novo_usuario
```

1. Habilitar o gerenciador de login:

```
service lightdm start
```

1. Reinicie a máquina:

```
reboot now
```

Obs.: as informações indicadas acima foram retiradas da seguinte wiki do proxmox: [https://pve.proxmox.com/wiki/Developer_Workstations_with_Proxmox_VE_and_X11](https://pve.proxmox.com/wiki/Developer_Workstations_with_Proxmox_VE_and_X11)



## Passo 7: Desabilitar SSH do root

**1. Adicionar úsuario sudo (se não houver)**

```
~$ adduser nome_usuario 
```

```
~$ adduser nome_usuario sudo
```

**1. Entrar no arquivo o arquivo sshd_config**:

```
nano /etc/ssh/sshd_config
```

**1. Procurar a linha "PermitRootLogin" em _Authentication_ e deixá-la como a linha abaixo**

```
PermitRootLogin no ou PermitRootLogin without-password
```

**1. Reiniciar o ssh**  

```
service sshd restart
```



# **Instalação ProxMox via USB**



## Passo 1: localizar o dispositivo USB

```
sudo fdisk -l
```

OBS01: após rodar o comando acima procure o dispositivo USB em que se tornará um pen-drive bootável com proxmox.  
OBS02: o USB irá ser formatado. Todos os dados que estiverem no USB serão perdidos.



## Passo 2: instalar o proxmox no USB

```
sudo dd if=./nome_da_iso /dev/dispositivo_do_usb
```

OBS01: no lugar de"nome_da_iso" coloque o nome do arquivo .iso do proxmox (é necessário estar na pasta em que se encontra a iso)

OBS02 no lugar de "dispositivo_do_usb"



# Visão geral sobre o Proxmox


## O que é o Proxmox? E para que serve?

* * *



## Explicação sobre os recursos disponíveis na interface web do Proxmox

O Proxmox está estruturado da seguinte maneira:

1.  Datacenter
2.  Máquina Real
3.  Máquina Virtual
4.  Discos Rígidos Disponíveis

* * *



# Criação de máquinas virtuais

Abaixo é indicado aspectos importantes na hora de criação de máquinas virtuais no Proxmox.



## Instalação em modo OEM (para template)

A instalação do Ubuntu no modo OEM permite que você pré-configure a instalação do sistema e distribua para o usuário onde ele terá apenas a necessidade de configurar o seu usuário, senhas e mais algumas preferências como localidade, idioma e teclado na primeira vez que o computador for ligado, isso permite que você como revendedor de computador personalize a instalação com drivers e programas para deixar o computador pronto para o seu cliente.

Para Mais detalhes e para um tutorial que como fazer uma instalação OEM ver: [Como instalar o Ubuntu no modo OEM](https://archive.is/qjok9#selection-263.1-263.448)

Pontos importantes na instalação OEM

1. Para poder escolher o modo OEM é necessário que você abra as opções na tela de boot do Ubuntu, para fazer isso, pressione a tecla SHIFT enquanto os sistema está dando boot, logo na primeira tela que você ver e você entrará neste modo.

1. Chegamos em um ponto importante. Agora você deve definir o usuário e senhas da instalação OEM, ambos serão temporários mas a senha será necessária para você fazer a preparação final do sistema, portanto não esqueça dela!

1. Agora estamos quase no final, depois que você fez a instalação previamente ao ligar o computador pela primeira vez depois da instalação você terá na área de trabalho um ícone chamado "Prepare for shipping to end user", antes de clicar nele você pode fazer quaisquer mudanças que você deseje, como instalar ou remover programas, instalar drivers, configurações opções, etc.

* * *



# Estrutura de diretórios

1. Disco local

Imagens ISO

```
/var/lib/vz/template/iso
```

VM Imagens

```
/var/lib/vz/images/<VMID>/
```

Container

```
/var/lib/vz/template/cache/
```

Backup files

```
/var/lib/vz/dump/
```

Referência: [Wiki Proxmox](https://pve.proxmox.com/wiki/Storage:_Directory)

* * *



# Gerenciamento das máquinas virtuais

* * *



# Gerenciamento de disco rígido (HDs)

* * *

Questões para serem documentadas: Raid0, ZFS e Ceph, btrfs Filesystem

* * *



# 1. Disco Rígidos Reais



## Adicionando o HD


### Passo 1: Digite o comando abaixo no terminal (como usuário sudo) e identifique o hd que será habilitado (você precisará do caminho do HD "/dev..." no proximo passo, então anote esta informação)

```
fdisk -l
```



### Passo 2: Digite Digite o comando abaixo no terminal para formatar e particionar o HD

```
fdisk /dev/vdc
```

> e em seguida

```
Comando (m para ajuda): 
Digite n 

Comando - ação:
Digite e 

Para as próximas etapas aperte enter. 

Comando (m para ajuda): digite w e aperte enter. 
```



### Passo 2: Digite o comando abaixo no terminal para criar o sistema de arquivos (substitua o /dev..., pelo o identificado no passo 1)

```
mkfs.ext4 /dev/vdc
```



### Passo 3: Digite o comando abaixo no terminal para criar uma pasta com o nome do seu hd para que o mesmo seja montado.
```
mkdir /media/nome_do_hd
```



### Passo 4: Digite o comando abaixo no terminal para identificar a partição

```
blkid
```

> Procure a linha do HD a ser habilitado, deve ser algo parecido com a linha abaixo:


```
/dev/vdc: UUID="9835bc44-19f4-4900-b275-e136f2a2acde" TYPE="ext4" 
```


### Passo 5: Digite o comando abaixo no terminal para habilitar o autostart do HD

```
nano /etc/fstab
```

### Passo 6: acrescente a linha abaixo no final do arquivo aberto com o editor nano. Substitua as informações do UUID e do diretório em que o HD será montado pelas informações de você obteve nos passos anteriores)

```
UUID=9835bc44-19f4-4900-b275-e136f2a2acde /media/hd-lantri-01 ext4 defaults 0 0
```



### Passo 8: reiniciar a máquina



### Passo 9: dar permissão de acesso

```
chmod 777 /caminho/de/montagem/do/hd
```


## Adicionando o HD Físico ao Proxmox

Referência: [https://archive.is/5nMZr](https://archive.is/5nMZr)

* * *



# 1. Discos Rígidos Virtuais


## Migraçao de OVA para proxmox

**PASSO 1** Converter .ova para .qcow2

```
tar xvf MyAppliance.ova
```

De vmdk para qcow2  

```
qemu-img convert -O qcow2 MyAppliance-disk1.vmdk MyAppliance.qcow2
```

de vmdk para raw  

```
qemu-img convert -f vmdk -O raw image.vmdk image.img
```

deqcow2 para raw  

```
qemu-img convert -O raw imagem-original.qcow2 imagem-convertida.raw
```

**PASSO 2** Criar Máquina Virtual (VM) no Proxmox

> > Será uma VM vazia que em nos próximos passos será sobrescrita pela VM convertida no **passo 1**.

**PASSO 3** Pelo terminal, ir para o diretório em que se encontra a máquina criada no passo acima

```
user@pve01# cd /disk3/images/105  (exemplo)
```

OBS: por padrão o proxmox coloca as VMs em "/var/lib/vz/images/". Se há mais de um HD na Máquina real, é necessário verificar qual HD foi selecionado para armazenar o disco da VMs na agora se sua criação no proxmox

**PASSO 4** Transferir arquivo convertido (qcow2)para o diretório que contem a máquina criada

```
user@pve01:/disk3/images/105# scp lantri_vm@200.145.122.119:/home/lantri_vm/Documentos/MyAppliance.qcow2 .
```

**PASSO 5** Sobrescrever a máquina transferida em cima na VM criada no Proxmox

```
mv MyAppliance.qcow2 vm-105-disk-1.qcow2
```

Pronto!! VM já pode ser ligada na interface web do proxmox.

* * *



# 1. Gerenciamento de Storage



## Logical Volume Manager (LVM)


### Introdução



#### Instalar LVM2

```
$ sudo apt-get install lvm2
```



#### Listar discos e partições

```
$ sudo fdisk -l
```



#### Criar Partições
```
$ sudo fdisk /dev/s..

n
p
1
```



#### Mudar tipos de partições

```m
l
t
1
8e
w
```


#### Listar partições para verificar as alterações

```
$ sudo fdisk -l
```



### Volumes Físicos



#### Criando os volumes físicos

```
$ sudo pvcreate /de/sdb1
```



#### Verificar ps volumes fícisos criados

```
$ sudo pvdisplay
```


### Grupo de volume



#### Criando Grupo de volume

```
$ sudo vgcreate nome_do_grupo_de_volume /dev/sdb1 /dev/sdb2
```



#### Verificar os grupos de volumes criados

```
$ sudo vgdisplay
```



### Criar volumes lógico

```
$ sudo lvcreate -L <x>GB -n <nome do vol lógico> nome_do_grupo_de_volume_viculado
```



#### Verificar volume lógico criado

```
S sudo lvdisplay
```



#### Renomear volume lógico

```
S sudo lvrename  <indicar nome do grupo de volume> <indicar nome atual do volume lógico> <indicar novo nome do volume lógico>
```



### Formatar Volume lógico

```

$ sudo mkfs.ext4 <indicar caminho do volume>
```



### Montar volumes lógicos



#### Criar diretório

```
$ sudo mkdir /caminho_de_montagem
```



#### Criando o Ponto de montagem

```
$ sudo mount -t ext4 /dev/<nomedogrupodevolume>/<nomedovolumelógico> /caminho-de-montagem
```

h4.Listar todos os sistemas de arquivos criados

```
df -h
```



### Expandir volume lógico

desmontar volume

```
$ sudo umount /ponto-de-montagem
```

expandir volume logico

```
$ sudo lvresize -L <novo tamanho> <caminho-do volume-logico>
```


verificar se tem erro

```
$ sudo e2fsck -f <caminho do volume lógico>
```


atualizar informações no sistema de arquivos

```
$ sudo rezise2fs <caminho do volume lógico>
```


```
lvdisplay
```


```
$ sudo mount -t ext4 <caminho do volume lógico>
```



### Diminuir volume lógico

Desmontar volume

```
$ sudo umount /ponto-de-montagem
```


Verificar se tem erro

```
 $ sudo e2fsck -f <caminho do volume lógico>
```


Atualizar informações no sistema de arquivos

```
$ sudo rezise2fs <caminho do volume lógico>  <novo tamanho reduzido>
```


Reduzir volume logico

```
$ sudo lvresize -L <novo tamanho> <caminho-do volume-logico>
```




### Montagem do volumes lógicos no FSTAB



#### Listar volumes lógicos

```
$ sudo lvscan
```


#### Abrir FSTAB

```
$ sudo nano /etc/fstab

//dev/<nome do grupo de volume/volume lógico> /ponto-de-montagem defaults 0 2
```




#### Forçar o montagem de todas as partições

```
$ sudo mount -a
```



### Estendendo um grupo de volume (inclusão de HD)



#### Listar os discos

```
$ sudo fdisk -l
```


1. Criar partição no disco novo

```
$ sudo fdisk /dev/sdd
$n
$p
$1
$ <clique ENTER>
$ <clique ENTER>
$t
$8e
```


2.

```
$ sudo vgdisplay <nome do grupo devolume>
```


1. Criar Volume físico

```
$ sudo pvecreate /dev/sdd1
```


1. Expandir grupo de volume atual

```
$ sudo vgextend <nome do grupo de volume> /dev/sdd1
```


* * *



1. Raid

* * *

1. Tabela
* * *



# Gerenciamento de rede

* * *


## Gerenciamento de Usuários e Grupos

1. Criar usuário por ssh

```
sudo adduser nome_usuário
```


1. Acessar interface GUI do ProxMox

1.  Clicar em DataCenter
2.  Verificar se o grupo adequado já esta criado, se não estiver, crie e depois clique em permisões e conecte o grupo a permissão adequada
3.  Adicione o usuário e vincule ao grupo adequado  
    **criar primeiro grupo,depois adicionar usuário (vincule ao grupo), e por fim clique em permissões**

Referência: [Wiki ProxMox Gerencimanto de Usuários](https://pve.proxmox.com/wiki/User_Management)

* * *


# Formação de cluster



# Ajustando a rede de MULTICAST para UNICAST

1. Ejetar o seguinte arquivo em cada um dos hosts:

```
nano /etc/hosts
```


Obs.: No arquivo acima deve constar o número do IP e o nome de cada HOST. Por exemplo:

```
200.145.122.109 pve01 localhost
200.145.122.111 pve02 localhost
200.145.122.113 pve03 localhost
```




## Passo 5: Habilitar Network Time Protocol (NTP)

1. Sincronizar as datas através do comando abaixo:

```
date --set=hh:mm:ss
```


1. Sincronizar a data da BIOS com a data do sistema:

```
hwclock -w
```


1. Instalar o NTP:

```
apt-get install ntp ntpdate
```


1. Verificar se a porta NTP está aberta:

```
nmap -p123 -sU -P0 localhost
```


1. Caso a porta esteja fechada, digite o seguinte comando:

```
ufw allow 123/udp
```


1. Substituir a linha que contém o NTP padrão pelo NTP da Unesp (ntp.unesp.br):

```
nano /etc/systemd/timesyncd.conf
```


Obs.: Retirar a "#" da linha do Server. Esse linha ficará assim:

```
Servers=ntp.unesp.br
```


1. Criar arquivo NTP:

```
touch /etc/ntp.drift
```


1. Editar o documento NTP abaixo:

```
nano /etc/ntp.conf
```


1. Substituir o NTP Server padrão (provavelmente será algo parecido com "server ntp.debian.org") pelo indicado abaixo:

```
server ntp.unesp.br iburst
```


1. Reiniciar o serviço de NTP:

```
/etc/init.d/ntp restart
```


1. Verificar se NTP está habilitado:

```
ntpq -c lpeer
```


1. Verificar sincronização com servidor NTP:

```
ntpdate -u ntp.unesp.br
```


1. Reiniciar a máquina:

```
reboot now
```


1. Criar o CLUSTER:

```
pvecm create nome_do_cluster
```


1. Modificar temporariamente o quorum mínimo do cluster

```
pvecm expected 1
```


1. Editar o seguinte arquivo:

```
nano /etc/pve/corosync.conf
```


1. Copiar e colar os itens abaixo no arquivo indicado acima:

```
logging {
  debug: off
  to_syslog: yes
}

nodelist {
  node {
    name: proxmox07
    nodeid: 3
    quorum_votes: 1
    ring0_addr: proxmox07
  }

  node {
    name: proxmox05
    nodeid: 2
    quorum_votes: 1
    ring0_addr: proxmox05
  }

  node {
    name: proxmox04
    nodeid: 1
    quorum_votes: 1
    ring0_addr: proxmox04
  }

}

quorum {
  provider: corosync_votequorum
}

totem {
  cluster_name: clusterlantri
  config_version: 3
  ip_version: ipv4
  secauth: on
  version: 2
  transport: udpu
  interface {
    bindnetaddr: 200.145.122.98
    ringnumber: 0
  }

}
```


Obs.: Atenção o IP do nó principal do cluster, com os nomes dos nós e com o nome do cluster que precisam ser modificados de acordo com as configurações que você realizou no processo de instalação do Proxmox.

1. Reinicie a máquina:

```
reboot now
```


1. Modificar temporariamente o quorum mínimo do cluster

```
pvecm expected 1
```


1. Adicionar nós ao cluster:

```
pvecm add nome_do_node
```


Obs.: adicionar os nós indicados no item 6

1. Reinicie a máquina:

```
reboot now
```


* * *


# Utilização do Spice no Proxmox

[https://archive.is/8N4bY](https://archive.is/8N4bY)  
[https://forum.proxmox.com/threads/how-to-configure-spice-in-pve.32509/](https://forum.proxmox.com/threads/how-to-configure-spice-in-pve.32509/)  
[https://archive.is/glToZ](https://archive.is/glToZ)  
[https://archive.is/v46YG](https://archive.is/v46YG)  
[https://archive.is/0FCcf](https://archive.is/0FCcf)

* * *


# Instalação de softwares nas máquinas reais com ProxMox



## 1. Configuracao do sistema para portugues:

configuracao do idioma portugues conforme o site: [https://www.vivaolinux.com.br/topico/Debian/Debian-portugues](https://www.vivaolinux.com.br/topico/Debian/Debian-portugues)

Idioma português do sistema funcionou após reinicio do computador.

O que foi feito:

 ```
Digite como root:
> 
> # dpkg-reconfigure locales
> 
> Daí marque as opções:
> 
> en_US.ISO8859-1
> en_US.UTF-8
> pt_BR.ISO8859-1
> pt_BR.UTF-8
> 
> Daí quando ele perguntar qual o idioma padrão, escolha o pt_BR.ISO8859-1, reinicie o sistema e desfrute
> 
> Depois voce pode mudar o idioma alterando o arquivo /etc/environment
 ```




## 1. Configuracao do teclado para portugues

Para reconhecimento do teclado em português foi feita a seguinte modificação (de acordo com o [https://archive.is/ibur0](https://archive.is/ibur0))

Passo 1: Edite o arquivo /etc/default/keyboard usando um editor em modo texto e deixe a configuração como mostrada abaixo:

```
XKBMODEL="pc105" 
XKBLAYOUT="br" 
XKBVARIANT="abnt2" 
XKBOPTIONS="" 
```


Passo 2: Salve as alterações, reinicie o computador e terá seu teclado configurado com layout ABNT2.

> OBS.: os comandos abaixo mudam o teclado para português imediatamente, mas ao encerrar a sessão ou religar a máquina, o teclado volta para o idioma padrão.

```
$sudo setxkbmap -model abnt2 -layout br OU setxkbmap -model pc105 -layout br -variant abnt2 
```


## 1. Instalacao do LibreOffice

Procedimento de instalação do LibreOffice no 109 e 111 (instalação como root):

A instalação foi feita conforme o site: [https://wiki.debian.org/LibreOffice](https://wiki.debian.org/LibreOffice)

**Passo 1:** adicionar jessie backports:  
abrir o repositório do debian

```
nano /etc/apt/sources.list
```


adicionar as seguintes linhas:

```
# jessie backports
deb http://deb.debian.org/debian jessie-backports main contr$

deb http://deb.debian.org/debian jessie-backports-sloppy mai$

# para instalar os pacotes do libreoffice pelo terminal com aptitude
deb http://ftp.de.debian.org/debian jessie-backports main
```


salve o arquivo com: CTRL+X, S (shift+s: sim) e Enter

**Passo 2:** atualize

```
aptitude update
```


**Passo 3:** Instalação o LibreOffice

```
apt-get install -t jessie-backports libreoffice
```


**Passo 4:** instalação do pacotes:

*   tradução do programa

```
aptitude install libreoffice-l10n-pt-br
```


> > pode baixar o pacote .deb aqui: [https://packages.debian.org/jessie-backports/libreoffice-l10n-pt](https://packages.debian.org/jessie-backports/libreoffice-l10n-pt)

*   ajuda

```
aptitude install libreoffice-help-pt-br
```


> > pode baixar o pacote .deb aqui: [https://packages.debian.org/jessie-backports/libreoffice-help-pt-br](https://packages.debian.org/jessie-backports/libreoffice-help-pt-br)

*   mithes_br

```
aptitude install mythes-pt-br
```


> > pode baixar o pacote .deb aqui: [https://packages.debian.org/stretch/mythes-pt-pt](https://packages.debian.org/stretch/mythes-pt-pt)

*   myspell_br

```
aptitude install myspell-pt-br
```


> > pode baixar o pacote .deb aqui: [https://packages.debian.org/jessie/all/myspell-pt/download](https://packages.debian.org/jessie/all/myspell-pt/download)

*   libmythes

```
aptitude install libmythes-dev
```


*   openclipart

```
aptitude install openclipart-png
```


```
aptitude install openclipart-libreoffice
```


> > pode baixar o pacote .deb aqui: [https://packages.debian.org/jessie/openclipart-libreoffice](https://packages.debian.org/jessie/openclipart-libreoffice)

*   menu: baixar do site do debian ([https://packages.debian.org/jessie/amd64/menu/download](https://packages.debian.org/jessie/amd64/menu/download)) e instalar o pacote via

```
dpkg -i nome_do_programa.deb
```


*   fontes

```
aptitude install ttf-mscorefonts-installer
```


> > pode baixar o pacote .deb aqui: [https://packages.debian.org/jessie/ttf-mscorefonts-installer](https://packages.debian.org/jessie/ttf-mscorefonts-installer)

*   ttf-liberation

```
aptitude install ttf-liberation
```


> > pode baixar o pacote .deb aqui: [https://packages.debian.org/jessie/ttf-liberation](https://packages.debian.org/jessie/ttf-liberation)

**Passo 5:** instalar corretor ortográfico e hifenizador VERO

*   Baixe o programa Vero no arquivo .oxt aqui: [https://pt-br.libreoffice.org/projetos/vero/](https://pt-br.libreoffice.org/projetos/vero/)
*   abra o arquivo com duplo clique
*   confirme a adição da extensão
*   clique em fechar
*   as configurações do corretor podem ser feitas em Ferramentas>Opções

* * *



# Adicionando o HD



## Passo 1: Digite o comando abaixo no terminal (como usuário sudo) e identifique o hd que será habilitado (você precisará do caminho do HD "/dev..." no proximo passo, então anote esta informação)

```
fdisk -l
```



## Passo 2: Digite Digite o comando abaixo no terminal para formatar e particionar o HD

```
fdisk /dev/vdc
```


> e em seguida

```

Comando (m para ajuda): 
Digite n 

Comando - ação:
Digite e 

Para as próximas etapas aperte enter. 

Comando (m para ajuda): digite w e aperte enter. 
```




## Passo 2: Digite o comando abaixo no terminal para criar o sistema de arquivos (substitua o /dev..., pelo o identificado no passo 1)

```
mkfs.ext4 /dev/vdc
```


## Passo 3: Digite o comando abaixo no terminal para criar uma pasta com o nome do seu hd para que o mesmo seja montado.

```
mkdir /media/nome_do_hd
```



## Passo 4: Digite o comando abaixo no terminal para identificar a partição

```
blkid
```


> Procure a linha do HD a ser habilitado, deve ser algo parecido com a linha abaixo:

```
/dev/vdc: UUID="9835bc44-19f4-4900-b275-e136f2a2acde" TYPE="ext4" 
```




## Passo 5: Digite o comando abaixo no terminal para habilitar o autostart do HD

```
nano /etc/fstab
```

## Passo 6: acrescente a linha abaixo no final do arquivo aberto com o editor nano. Substitua as informações do UUID e do diretório em que o HD será montado pelas informações de você obteve nos passos anteriores)

```
UUID=9835bc44-19f4-4900-b275-e136f2a2acde /media/hd-lantri-01 ext4 defaults 0 0
```




## Passo 8: reiniciar a máquina



## Passo 9: dar permissão de acesso

```
chmod 777 /caminho/de/montagem/do/hd
```


* * *



# Adicionando o HD


## Passo 1: Digite o comando abaixo no terminal (como usuário sudo) e identifique o hd que será habilitado (você precisará do caminho do HD "/dev..." no proximo passo, então anote esta informação)

```
fdisk -l
```




## Passo 2: Digite Digite o comando abaixo no terminal para formatar e particionar o HD

```
fdisk /dev/vdc
```


> e em seguida

```

Comando (m para ajuda): 
Digite n 

Comando - ação:
Digite e 

Para as próximas etapas aperte enter. 

Comando (m para ajuda): digite w e aperte enter. 
```




## Passo 2: Digite o comando abaixo no terminal para criar o sistema de arquivos (substitua o /dev..., pelo o identificado no passo 1)

```
mkfs.ext4 /dev/vdc
```



## Passo 3: Digite o comando abaixo no terminal para criar uma pasta com o nome do seu hd para que o mesmo seja montado.

```
mkdir /media/nome_do_hd
```


## Passo 4: Digite o comando abaixo no terminal para identificar a partição

```
blkid
```


> Procure a linha do HD a ser habilitado, deve ser algo parecido com a linha abaixo:

```
/dev/vdc: UUID="9835bc44-19f4-4900-b275-e136f2a2acde" TYPE="ext4" 
```


## Passo 5: Digite o comando abaixo no terminal para habilitar o autostart do HD

```
nano /etc/fstab
```



## Passo 6: acrescente a linha abaixo no final do arquivo aberto com o editor nano. Substitua as informações do UUID e do diretório em que o HD será montado pelas informações de você obteve nos passos anteriores)

```
UUID=9835bc44-19f4-4900-b275-e136f2a2acde /media/hd-lantri-01 ext4 defaults 0 0
```



## Passo 8: reiniciar a máquina


## Passo 9: dar permissão de acesso

```
chmod 777 /caminho/de/montagem/do/hd
```



# Adicionando o HD Físico ao Proxmox

Referência: [https://archive.is/5nMZr](https://archive.is/5nMZr)


# Migraçao de OVA para proxmox

**PASSO 1** Converter .ova para .qcow2

```
tar xvf MyAppliance.ova
```


De vmdk para qcow2  

```
qemu-img convert -O qcow2 MyAppliance-disk1.vmdk MyAppliance.qcow2
```


de vmdk para raw  

```
qemu-img convert -f vmdk -O raw image.vmdk image.img
```


deqcow2 para raw  

```
qemu-img convert -O raw imagem-original.qcow2 imagem-convertida.raw
```


**PASSO 2** Criar Máquina Virtual (VM) no Proxmox

> > Será uma VM vazia que em nos próximos passos será sobrescrita pela VM convertida no **passo 1**.

**PASSO 3** Pelo terminal, ir para o diretório em que se encontra a máquina criada no passo acima

```
user@pve01# cd /disk3/images/105  (exemplo)
```


OBS: por padrão o proxmox coloca as VMs em "/var/lib/vz/images/". Se há mais de um HD na Máquina real, é necessário verificar qual HD foi selecionado para armazenar o disco da VMs na agora se sua criação no proxmox

**PASSO 4** Transferir arquivo convertido (qcow2)para o diretório que contem a máquina criada

```
user@pve01:/disk3/images/105# scp lantri_vm@200.145.122.119:/home/lantri_vm/Documentos/MyAppliance.qcow2 .
```


**PASSO 5** Sobrescrever a máquina transferida em cima na VM criada no Proxmox

```
mv MyAppliance.qcow2 vm-105-disk-1.qcow2
```


Pronto!! VM já pode ser ligada na interface web do proxmox.

1.  install: sudo apt --fix-broken install
