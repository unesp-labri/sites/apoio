---
id: tutorial
title: Tutorial Dual Boot nos PCs Dell
sidebar_label: Tutorial Dual Boot
slug: /projetos/sistemas/tutorial-dual-boot/tutorial
---

Tutorial de instalação de um dual boot com os sistemas Windows e Linux nos computadores Dell. 

## Passo a Passo

1. Para iniciar um dual boot em seu computador, abra o Gerenciamento de disco do Windows e selecione o espaço utilizado por seu HD ou SSD. Em seguida, com o botão direito, selecione a opção “Diminuir volume…”

![IMG_4101](/img/projetos/sistemas/IMG_4101.JPG)

2. Em “Digite o espaço a diminuir em MB”, o usuário informa a quantidade de espaço a se tornar livre e vazio para a instalação do segundo sistema. O LabRI reserva um espaço de 335GB para o sistema Linux e 150GB para o sistema Windows. 

![IMG_4102](/img/projetos/sistemas/IMG_4102.JPG)

3. Em seguida, conecte o pendrive bootável do sistema desejado (neste caso, Linux Ubuntu 22.04) e reinicie o computador. Antes que a tela de login apareça, clique em F12 para abrir a tela abaixo. 
Com o pendrive conectado no computador, selecione-o como mostra a imagem (haverá um símbolo de USB ao seu lado). Neste caso, nosso pendrive identifica-se por UEFI USB DISK 2.0

![IMG_4091](/img/projetos/sistemas/IMG_4091.jpg)

4. O computador começa a carregar a instalação, e será necessário selecionar a opção desejada. Em nosso caso, selecionamos Try or Install Ubuntu. Em seguida, aparece a tela de carregamento. Basta esperar. 

![IMG_4092](/img/projetos/sistemas/IMG_4092.jpg)

![IMG_4093](/img/projetos/sistemas/IMG_4093.jpg)

5. Quando a instalação começar, basta selecionar as opções desejadas. Neste caso, instalamos o Ubuntu e acertamos as configurações para Português do Brasil. 

![IMG_4094](/img/projetos/sistemas/IMG_4094.jpg)

![IMG_4095](/img/projetos/sistemas/IMG_4095.jpg)

6. Para prosseguir a instalação, é possível selecionar uma conexão de internet para baixar softwares de terceiros e afins. Para conectar-se ao eduroam, rede de WiFi da instituição, siga as configurações mostradas na imagem abaixo. 
No campo “nome de usuário”, informe seu email institucional da Unesp e, no campo abaixo, sua senha. 

![IMG_4096](/img/projetos/sistemas/IMG_4096.jpg)

7. Nos próximos passos, selecione as opções como mostradas nas imagens abaixo. Ao chegar em Tipo de Instalação, você inicia seu dual boot ao selecionar a opção “Instalar [sistema operacional] ao lado do Windows Boot Manager”. 

![IMG_4097](/img/projetos/sistemas/IMG_4097.jpg)

![IMG_4098](/img/projetos/sistemas/IMG_4098.JPG)

![IMG_4099](/img/projetos/sistemas/IMG_4099.JPG)

8. Para finalizar sua instalação, configure as informações do computador inserindo os dados do usuário e do pc. Reproduza as configurações abaixo e altere as informações conforme o computador utilizado. Em seguida, basta prosseguir e aguardar a instalação completa. 
Ao final da instalação, aparecerá uma imagem para reiniciar o computador e remover o dispositivo de instalação (no caso, o pendrive utilizado). 

![IMG_4100](/img/projetos/sistemas/IMG_4100.jpg)