
---
id: anaconda
title: Intro-Implementação
sidebar_label: Intro-Implementação
slug: /projetos/sistemas/implementacao/anaconda
---

```

installation finished.
Do you wish the installer to initialize Anaconda3
by running conda init? [yes|no]
[no] >>> yes
no change     /home/rafael/anaconda3/condabin/conda
no change     /home/rafael/anaconda3/bin/conda
no change     /home/rafael/anaconda3/bin/conda-env
no change     /home/rafael/anaconda3/bin/activate
no change     /home/rafael/anaconda3/bin/deactivate
no change     /home/rafael/anaconda3/etc/profile.d/conda.sh
no change     /home/rafael/anaconda3/etc/fish/conf.d/conda.fish
no change     /home/rafael/anaconda3/shell/condabin/Conda.psm1
no change     /home/rafael/anaconda3/shell/condabin/conda-hook.ps1
no change     /home/rafael/anaconda3/lib/python3.8/site-packages/xontrib/conda.xsh
no change     /home/rafael/anaconda3/etc/profile.d/conda.csh
modified      /home/rafael/.bashrc

==> For changes to take effect, close and re-open your current shell. <==

If you'd prefer that conda's base environment not be activated on startup, 
   set the auto_activate_base parameter to false: 

conda config --set auto_activate_base false

Thank you for installing Anaconda3!

===========================================================================

Working with Python and Jupyter notebooks is a breeze with PyCharm Pro,
designed to be used with Anaconda. Download now and have the best data
tools at your fingertips.

PyCharm Pro for Anaconda is available at: https://www.anaconda.com/pycharm


```


### Ambiente virtual

- Tornar replicavel o ambiente de desenvolvimento (scripts do repositório gitlab) em qualquer sistema operacional
- Isolar bibliotecas e programas instalados do sistema padrão do usuário




```

sudo apt install libgl1-mesa-glx libegl1-mesa libxrandr2 libxrandr2 libxss1 libxcursor1 libxcomposite1 libasound2 libxi6 libxtst6
wget https://repo.anaconda.com/archive/Anaconda3-2021.05-Linux-x86_64.sh
sudo mkdir /opt/anaconda
sudo chmod ugo+w /opt/anaconda
 bash Anaconda3-2021.05-Linux-x86_64.sh -u
which python
deactivate
conda deactivate
which python
 
 1984  sudo groupadd anaconda
 1985  sudo chgrp -R anaconda /opt/anaconda
 1986  sudo chmod 770 -R /opt/anaconda
 1987  sudo adduser rafael anaconda
 1988  conda list
 1989  conda create --name env_govlatinamerica
 1990  conda activate env_govlatinamerica
 1991  which python
 1992  conda list
 1993  clear
 1994  conda list
 1995  conda install python
 1996  conda list
 1997  python --version


```



































