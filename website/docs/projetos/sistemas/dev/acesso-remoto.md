---
id: acesso-remoto
title: Acesso Remoto
sidebar_label: Acesso Remoto 
slug: /projetos/sistemas/implementacao/acesso-remoto
---


```
Note: To avoid losing work, any previously-started chrome-remote-desktop
sessions are still running, not controlled by systemd. To stop your existing
session and start it again under systemd, run the following commands:

    /opt/google/chrome-remote-desktop/chrome-remote-desktop --stop
    systemctl start chrome-remote-desktop@$USER

This will close any programs currently running in the session. Alternatively,
you can just reboot the machine.

This update also moves log messages to the journal. View them with

    journalctl SYSLOG_IDENTIFIER=chrome-remote-desktop

Restarting Chrome Remote Desktop hosts (sessions will be unaffected)...
A processar 'triggers' para systemd (237-3ubuntu10.48) ...
A processar 'triggers' para ureadahead (0.100.0-21) ...
```

- Para habilitar o acesso remoto pelo Chrome Remote Desktop, é necessário selecionar o usuário `chrome-remote-desktop` e colocar a senha `chrome`

# Polkit

- https://stackoverflow.com/questions/40270671/systemd-service-ask-password
- https://blog.victormendonca.com/2020/04/02/how-to-install-chrome-remote-desktop-on-arch/
- https://askubuntu.com/questions/1318473/authentication-required-popup
- https://askubuntu.com/questions/1315720/cannot-turn-on-chrome-remote-desktop-in-ubuntu-20-04
- https://askubuntu.com/questions/1232917/how-to-add-google-remote-to-a-headless-docker-on-vast
- https://cloud.google.com/architecture/chrome-desktop-remote-on-compute-engine
- https://www.tecmint.com/manage-services-using-systemd-and-systemctl-in-linux/
- https://www.google.com/search?q=Systemctl+permission+all+user+run+service+chrome+remote&sxsrf=AOaemvKGazoTDj7gNutPcWaneQUwxokxRQ%3A1633243081170&ei=yU9ZYb7nCcPH5OUP1fyCyAs&ved=0ahUKEwj-77zN0K3zAhXDI7kGHVW-ALkQ4dUDCA4&uact=5&oq=Systemctl+permission+all+user+run+service+chrome+remote&gs_lcp=Cgdnd3Mtd2l6EAMyBQghEKABOgcIIxCwAxAnOgcIABBHELADOgcIIxCuAhAnOggIIRAWEB0QHjoHCCEQChCgAUoFCDoSATFKBAhBGABQuTxYnlxg_15oAXACeACAAZYBiAG6DZIBBDAuMTSYAQCgAQHIAQnAAQE&sclient=gws-wiz
- https://kifarunix.com/install-and-setup-chrome-remote-desktop-on-ubuntu-20-04/
- https://askubuntu.com/questions/1264418/chrome-remote-desktop-host-setup-instructions-not-showing-for-ubuntu-18-04
- https://blog.victormendonca.com/2020/04/02/how-to-install-chrome-remote-desktop-on-arch/
- https://bytexd.com/install-chrome-remote-desktop-headless/
- https://serverfault.com/questions/917171/allow-non-root-user-to-run-certain-systemd-services-not-without-a-password
- https://stackoverflow.com/questions/61480914/using-policykit-to-allow-non-root-users-to-start-and-stop-a-service
- https://www.freedesktop.org/software/polkit/docs/latest/polkit.8.html
- https://www.google.com/search?q=chrome+remote+desktop+github&sxsrf=AOaemvJAKss03FjGxol50AMxJC6pAJaGpQ:1633240338672&ei=EkVZYcq5KLXX1sQPnJSVwA0&start=20&sa=N&ved=2ahUKEwjKiuCxxq3zAhW1q5UCHRxKBdg4ChDw0wN6BAgBEE8&biw=1920&bih=936&dpr=1
- https://medium.com/@KeithIMyers/chrome-remote-desktop-on-ubuntu-20-04-setup-guide-setup-script-8265f5fe7787
- https://kmyers.me/blog/linux/chrome-remote-desktop-on-ubuntu-20-04-setup-guide-setup-script/
- https://chromebook.community/d/6-linux-chrome-remote-desktop-troubleshooting/19
- https://kmyers.me/blog/linux/chrome-remote-desktop-on-ubuntu-20-04-setup-guide-setup-script/

 



