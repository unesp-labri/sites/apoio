---
id: intro
title: Sites LabRI
sidebar_label: Sites LabRI
slug: /projetos/sistemas/dev/sites/intro
---



- O LabRI conta com um site aberto e outro fechado.
- O site do LabRI/UNESP é construído a partir de um projeto opensource do Facebook chamado Docusaurus. 
- As subpastas de "docs" possuem a mesma estrutura tanto no site restrito quanto no site aberto. O objetivo disso é facilitar a migração de arquivos entre os dois sites. 
- Tanto no site aberto quanto no fechado inserimos a documentação das nossas atividades no LabRI/UNESP. 


### Site aberto 

O site aberto é voltado para a comunicação geral do LabRI/UNESP. O conteúdo colocado nesse site não possui nenhum tipo de restrição. O site fechado está restrito somente aos colaboradores, discentes, docentes e grupos de pesquisa que utilizam a infraestrutura do LabRI/UNESP. 


- [Site aberto](https://labriunesp.org)
- Hospedado no [Render](https://dashboard.render.com/static/srv-c1hpj55ua9vtg41nbkkg)


### Site fechado

O conteúdo do site restrito ou contém informações sensíveis que não podem estar públicas, ou contém informações que estão em processo de construção, não estando em uma versão considerada ideal para se tornar pública.

- [Site fechado](https://apoio.labriunesp.org)
- Hospedado no [GitLab](https://gitlab.com/unesp-labri/sites/apoio) 

### Edição dos sites

- Edição do site (Sidebar, markdown, paginas.js)


- [Markdown](https://www.markdownguide.org/basic-syntax/)  
- Para testar os comandos, utilize este [playground](http://markdown-it.github.io/)


|Comando|Função|Exemplo|
|---|---|---|
|`#`|Usada para criar títulos (`header`)|# Título 1, ## Título 2, ### Título 3|
|`-`|Usado para criar tópicos| - tópico 1|
|`[]` e `()`|Usado para citar links| `[Google](google.com)`|
|`\|`|Usado para criar tabelas| 1   2   3 <br/> \|---\|---\|---\| <br/> a   b   c|
|\```|Usado para criar um bloco de código| \```py |

- Sidebar
    - Quando criamos um Markdown na pasta docs, para que este formato apareça no site, precisamos citar o "caminho" para o mesmo na Sidebar (`sidebars.js`)
    
```
---
id: `ultima palavra do slug`
title: `título da página`
sidebar_label: `título do que aparecerá na sidebar`
slug: caminho a partir da pasta `docs` (/projetos/sistemas/dev/sites/intro)
    `ex: slug: /projetos/sistemas/dev/sites/intro` 
---

Exemplo:
---
id: intro
title: Sites LabRI
sidebar_label: Sites LabRI
slug: /projetos/sistemas/dev/sites/intro
---

``` 

- Páginas.js

### Edição rápida dos sites

- WebIDE do GitLab



### Edição completa dos sites

- Edição local do site

### Estrutura do diretório `docs`

- A pasta `docs` contém todos os arquivos `markdown` relativos a documentação dos projetos e atividades realizadas pelo LabRI/UNESP. 
- Buscamos manter a estrutura desta pasta igual, tanto no site aberto quanto no site fechado, para facilitar a migração dos arquivos entre os sites. Como a pasta `docs` dos dois sites tem a mesma estrutura, a migração de arquivos entre eles é realizada apenas movendo o arquivo de um site para o outro

#### A pasta `docs` está estruturada da seguinte maneira: 

|Diretório/Arquivo|Localização|Conteúdo| 
|---|---|---| 
|`cadernos`| Site aberto | Informações relativas à revista do LabRI/UNESP |
|`geral`|Site aberto ou site fechado| `equipe`, `rotinas` e `info` |
|`projetos`|Site aberto ou site fechado| `dados`, `ensino`, `extensao` e `sistema`|




#### Relativo ao caminho `/docs/geral`

|Diretório|Localização|Conteúdo|Arquivo| 
|---|---|---|---|
|`equipe`| Site aberto | Páginas para membros/colaboradores ||
|`equipe`| Site fechado | Páginas para membros/colaboradores ||
|`rotinas`|Site fechado| Informações internas para colaboradores|`acesso-sala-labri`, `contatos-unesp-franca`, `google`,`intro`, <br/> `inventario`, `redes-sociais`, `transmissao`, `video-chamada`|
|`info`|Site aberto| Informações sobre atendimento, apoio e estágio|`01-atendimento`, `02-estagio`, `03-processo-seletivo`, <br/>`04-colaborador`, `05-rotinas`, `06-emprestimos` |






### Atualização do site

- Fazer tutorial 
