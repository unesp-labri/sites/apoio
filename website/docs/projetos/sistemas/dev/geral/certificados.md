---
id: certificados
title: Certificados
sidebar_label: Certificados
slug: /projetos/sistemas/dev/geral/certificados
---

## Como Gerar Certificados Automaticamente 

Para gerar certificados automáticos, é preciso primeiro uma planilha com os dados das pessoas que deseja-se gerar os certificados, é importante que dentre esses dados estejam nome, Registro Acadêmico, e o que for necessário.

Então, deve-se fazer um modelo de certificado para o qual os dados serão inseridos no LibreOffice Writer.

Depois, nos locais onde deverão ser inseridos as informações de cada pessoa, crie um quadro. Inserir > Quadro > Quadro ...
OBS: Recomenda-se utilizar um quadro sem bordas e com transparecia de fundo, além de ancorar o texto no parágrafo, caso seja um texto grande,  ou como caractér, caso seja um texto menor.

Deve-se então linkar o documento com a planilha, via Editar > Trocar Banco de Dados > Procurar > Selecionar o arquivo > Selecionar a Planilha

E exibir os dados linkados em Exibir > Fontes de dados

Depois disso basta arrastar o nome do campo desejado para o quadro em que ele se encaixará

Para gerar os certificados, então, deve-se Imprimr > Imprimir como carta-fotmulário > Selecionar imprimir todos e como arquivo > Selecionar salvar como documentos individuais > Selecionar Gerar o nome a partir do banco de dados > Selecionar o campo que deseja tornar o nome dos arquivos (recomenda-se nome) > Selecionar o formato de arquivo (recomenda-se pdf) e clicar em imprimir
