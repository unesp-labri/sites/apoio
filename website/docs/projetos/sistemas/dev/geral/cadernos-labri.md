---
id: cadernos-labri
title: Cadernos LabRI
sidebar_label: Cadernos LabRI
slug: /projetos/sistemas/dev/geral/cadernos-labri
---

Este espaço visa reunir informações relativas a geração de PDF's dos Cadernos LabRI/UNESP. O código para a realização deste trabalho se encontra no [repositório do site aberto do LabRI](https://gitlab.com/unesp-labri/sites/labri).

|Data|Atividades Realizadas|
|---|---|
|26/08/2021|Lidando com problemas na geração dos pdf's|
|25/08/2021|Lidando com problemas na geração dos sumários nos pdf's|
|24/08/2021|Mesclagem do código feito pelo Pedro |
|23/08/2021|atualização do ambiente virtual; ajustes nas versões das bibliotecas utilizadas|
|05/08/2021|Criação de ambiente virtual `conda` para o projeto.|

## Como utilizar


### Criar pasta para armazenar o código do projeto

```
mkdir -p ~/codigo && cd codigo
```
### Clonar repositório do projeto

```
git clone https://gitlab.com/unesp-labri/sites/labri
```
### Entrar na pasta do projeto

```
cd ~/codigo/labri/python

```

### Criar ambiente virtual e instalar dependências do projeto  

- O comando abaixo é utilizado apenas quando o arquivo environment.yml é criado pela primeira vez. Caso ele já esteja no repositório, não é necessário rodar o comando abaixo.

```
touch environment.yml && conda env create -f environment.yml  
```
### Indicar inicializador
- Após rodar o comando abaixo reinicie o terminal

```
conda init bash
```

### Ativar ambiente virtual 

```
conda activate env_cadernos

```

### Atualizar dependências do projeto 

- O comando abaixo é utilizado para que seu ambiente virtual esteja atualizado com todas as dependências necessárias. Indicamos que este comando seja utilizado sempre que você for mexer no projeto, para garantir que você tenha acesso a todas as bibliotecas.

```
git pull origin main && conda activate env_cadernos && conda env update 
```

### Inserir novas bibliotecas no environment.yml
 
- O comando abaixo é utilizado para instalar uma nova biblioteca utilizada no projeto. Ao rodar o comando ele irá atualizar as dependências do projeto para todos os colaboradores.

```
conda env export > environment.yml
```



