---
id: intro
title: Aplicações
sidebar_label: Aplicações
slug: /projetos/sistemas/dev/intro
---

## Recoll Desktop Search

[Google Docs - Utilização Recoll](https://docs.google.com/document/d/1msD8d3czzZaCW5Iv1Q17CN--OnNc96MZPwO_QxSNB98/edit?usp=sharing)

## Criação de pasta para dado

É possível o estudante ter uma pasta para armazenar os seus dados de pesquisa, tais dados podem, como será indicado abaixo, ter uma indexação específica no Recoll. Isso significa que as pastas de pesquisa estarão contidas em um indíce específico do software, sendo possível uma busca circunscrita da pasta do estudante. 

- criar pasta

## Indexar dados específicos

Os dados colocados na seguinte pasta de dados do pesquisador são indexados pelo Recoll em um indice específico: `/user/200/xxx/bd`. E podem ser pesquisadas a partir do seguinte atalho `recollindex-nome_usuario` 

## Indexar arquivos diversos de bases disponíveis

É possível o estudante gerar uma indexação específica a partir da seleção de arquivos das variadas bases disponíveis, ou seja, você pode, por exemplo, realizar uma pesquisa na base da Folha de S. Paulo e identificar cinco notícias como importantes para sua pesquisa. Em seguida, você faz uma busca na base da revista Contexto Internacional e identifica dois artigos como importantes a sua pesquisa. A partir da identificação destes sete arquivos relacionados a sua pesquisa, é possível gerar uma indexação que contenha apenas os arquivos que você julgou importantes e com isso realizar busca por palavras chaves circunscritas apenas a esses arquivos.

``` 
/media/hdvm05/scripts/shell-script/recoll-multiplo02.sh
# pasta de index: /media/hdvm05/index/200/002/teste
# pasta de dados: /media/hdvm10/user/200/002/bd/teste
# pasta localização txt: /media/hdvm10/user/200/002/index-recoll.txt
```
