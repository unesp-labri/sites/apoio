---
id: ssl-free-namecheap
title: SSL Namecheap
sidebar_label: SSL Namecheap
slug: /projetos/sistemas/implementacao/ssl-free-namecheap
---


```
via ssh rdeahfar@premium75.web-hosting.com -p21098

Cpanel >> DNS Zone editor
www.nepps.org.	1200	IN	A	198.187.31.225
nepps.org.	1200	IN	A	198.187.31.225



```


```
curl https://get.acme.sh | sh

acme.sh --register-account --accountemail email@example.com

acme.sh --issue --webroot ~/dipp.labriunesp.org -d dipp.labriunesp.org -d www.dipp.labriunesp.org --staging

acme.sh --issue --webroot ~/dipp.labriunesp.org -d dipp.labriunesp.org -d www.dipp.labriunesp.org --force

acme.sh --deploy --deploy-hook cpanel_uapi --domain dipp.labriunesp.org




```


```
Cpanel >> SSL/TLS >> Manage SSL Hosts

```

- nano ~/dipp.labriunesp.org/.htaccess

```

RewriteEngine On
RewriteCond %{HTTPS} !=on
RewriteRule .* https://dipp.labriunesp.org%{REQUEST_URI} [R=301,L]
# BEGIN WordPress
# As diretrizes (linhas) entre "BEGIN WordPress" e "END WordPress" são
# geradas dinamicamente e só devem ser modificadas através de filtros do WordPress.
# Quaisquer alterações nas diretivas entre esses marcadores serão sobrescritas.
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
RewriteBase /
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.php [L]
</IfModule>

# END WordPress



```

### Material de apoio

- [Let’s Encrypt SSL certificate in Namecheap AutoRenewal](https://archive.is/rSOiz)
