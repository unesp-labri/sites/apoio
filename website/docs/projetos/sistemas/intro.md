---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/sistemas/intro
---

<center>
    <img src="/img/projetos/sistemas/logo-projetos-sistemas.svg" alt="centered image" />
</center>

Os projetos de infraestrutura computacional são iniciativas que visam promover uma melhor utilização dos recursos computacionais nas pesquisas de Relações Internacionais. Com isso, neste espaço é possível encontrar projetos relacionados à construção e manutenção de pequenas e versáteis estações de trabalho multifuncionais que dão suporte às atividades de pesquisa, ensino e extensão.

- Abaixo listamos a estrutura geral da documentação dos Projetos de Sistemas

|Frente|Descrição|Link de Acesso|
|---|---|---|
|Dev|Aplicações desenvolvidas e/ou mantidas pelo LabRI/UNESP|[Acesso](/docs/projetos/sistemas/dev/intro)|
|Linux|Reúne informações sobre a administração dos sistemas operacionais Linux do LabRI/UNESP|[Acesso](/docs/projetos/sistemas/linux/intro)|
|Monitoramento|Reúne informações sobre as aplicações de monitoramento do LabRI/UNESP|[Acesso](/docs/projetos/sistemas/monitoramento/intro)|



