---
id: intro
title: Redes
sidebar_label: Apresentação
slug: /projetos/sistemas/redes/intro
---


- [Open vSwitch: O que é? O que come? Onde vive?](https://blog.4linux.com.br/open-vswitch-o-que-e-o-que-come-onde-vive/)

- https://moodle.ifsc.edu.br/mod/book/view.php?id=312208&chapterid=52706

- Exemplo de desenho
  - https://forum.proxmox.com/threads/the-pfsense-doesnt-work-corectly.83748/
  - https://forum.proxmox.com/threads/problems-accessing-proxmox-when-configuring-iptables-for-pfsense-ssh-or-web-interface.103174/
  - https://forum.proxmox.com/threads/pfsense-in-proxmox-and-vlans.100319/

- https://forum.proxmox.com/threads/how-to-configure-proxmox-and-pfsense-vm-so-that-all-network-requests-go-through-pfsense.64021/
- https://www.laroberto.com/vlans-with-proxmox-and-pfsense/
- https://youtu.be/OjLLJ71hKSU
- https://forum.proxmox.com/threads/solved-how-to-setup-nat-with-one-of-my-4-public-ips.107924/
- https://forum.proxmox.com/threads/ovs-setup-for-pfsense-diagram-included.84718/

 ## IPPRI-UNESP

 - https://youtu.be/GK0DOfdLCa8