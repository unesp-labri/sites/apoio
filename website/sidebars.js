module.exports = {
   rotinas: [ // rotinas gerais dos estagiários na sala do labri
    {
     type: 'category',
     label: 'Rotinas LabRI/UNESP',
     items: [
       'geral/intro', 
       'geral/rotinas/gestao-atividades',
       'geral/rotinas/google-drive', 
       {
              type: 'link',
              href: '/docs/geral/comunicacao/gestao-redes',
              label: 'Comunicação',
            }, 
       'geral/rotinas/contatos-unesp-franca',
       'geral/rotinas/acesso-sala-labri', 
       'geral/rotinas/inventario',
       'geral/rotinas/manutencao', //mauntençaão sala labri e servidores
       'geral/rotinas/video-chamada',
              {
              type: 'link',
              href: '/docs/geral/equipe/intro',
              label: 'Equipe',
            }, 
       'geral/rotinas/treinamento',
       {
        type: 'link',
        href: '/docs/geral/audiovisual/intro',
        label: 'Audiovisual',
      },
      ],
    },
  ],
  
    projetos: [ 
      {
       type: 'category',
       label: 'Projetos',
       items: ['projetos/intro'],
      },
    ],

    comunicacao: [ 
      {
       type: 'category',
       label: 'Comunicação',
       items: [
         'geral/comunicacao/gestao-redes',
         'geral/comunicacao/pag-colaborador',
         'geral/comunicacao/templates-ig',
         'geral/comunicacao/tutorial-gif',
         'geral/comunicacao/templates-figma',
         'geral/comunicacao/tutorial-pod-ri',
         'geral/comunicacao/tutorial-pagina',
         'geral/comunicacao/templates-publicacoes',
         'geral/comunicacao/edicao-trilha-dados',
       ],
      },
    ],
    
    projetosDados: [
        {
          type: 'category',
          label: 'Projetos de Dados',
          items: [
            {
              type: 'link',
              href: '/docs/projetos/intro',
              label: 'Projetos',
            },
        'projetos/dados/intro',
        'projetos/dados/bases-dados',
        'projetos/dados/id-visual',      
        ],
      },
    ],
    
    projetosDadosAcervoRedalint: [
      {
        type: 'category',
        label: 'Acervo REDALINT',
        items: [
          'projetos/dados/acervo-redalint/intro', 
          'projetos/dados/acervo-redalint/howto', 
          'projetos/dados/acervo-redalint/codedoc'],
        },
      ],
    projetosDadosDiariosbr: [
      {
        type: 'category',
        label: 'DiáriosBR',
        items: [
          'projetos/dados/diariosbr/intro',
          'projetos/dados/diariosbr/como-utilizar',
          'projetos/dados/diariosbr/info',
          'projetos/dados/diariosbr/dados_diarios',
          'projetos/dados/diariosbr/index_diarios',
          'projetos/dados/diariosbr/analise-dados',
          'projetos/dados/diariosbr/banco-termos',
          'projetos/dados/diariosbr/comandos_linux',
          'projetos/dados/diariosbr/id-visual'],
        },
      ],
    projetosDadosFullText: [
      {
        type: 'category',
        label: 'Full Text',
        items: [
          'projetos/dados/full-text/intro',
          'projetos/dados/full-text/howto',
          'projetos/dados/full-text/codedoc',
          'projetos/dados/full-text/id-visual',
          'projetos/dados/full-text/digitalizacao',
          'projetos/dados/full-text/ocr',
          ],
        },
      ],
    
    projetosDadosHemerotecaPEB: [
      {
        type: 'category',
        label: 'Hemeroteca PEB',
        items: [
          'projetos/dados/hemeroteca-peb/intro', 
          'projetos/dados/hemeroteca-peb/howto', 
          'projetos/dados/hemeroteca-peb/codedoc'],
        },
      ],
    
    projetosDadosIRJournalsbr: [
      {
        type: 'category',
        label: 'IRJournalsBR',
        items: [
          'projetos/dados/irjournalsbr/intro', 
          'projetos/dados/irjournalsbr/howto', 
          'projetos/dados/irjournalsbr/codedoc',
          'projetos/dados/irjournalsbr/id-visual'],
        },
      ],

    projetosDadosMercoDocs: [
      {
        type: 'category',
        label: 'MercoDocs',
        items: [
          'projetos/dados/mercodocs/intro', 
          'projetos/dados/mercodocs/howto', 
          'projetos/dados/mercodocs/codedoc',
          'projetos/dados/mercodocs/id-visual'],
        },
      ],

    projetosDadosNewsCloud: [
      {
        type: 'category',
        label: 'NewsCloud',
        items: [
          'projetos/dados/newscloud/intro', 
          'projetos/dados/newscloud/howto', 
          'projetos/dados/newscloud/codedoc',
          'projetos/dados/newscloud/id-visual'],
        },
      ],

    projetosDadosTweepina: [
      {
        type: 'category',
        label: 'TweePiNa',
        items: [
          'projetos/dados/tweepina/intro', 
          'projetos/dados/tweepina/howto', 
          'projetos/dados/tweepina/codedoc',
          'projetos/dados/tweepina/id-visual'],
        },
      ],

    projetosDadosGovLatinAmerica: [
      {
        type: 'category',
        label: 'GovLatinAmerica',
        items: [
          'projetos/dados/gov-latin-america/intro', 
          'projetos/dados/gov-latin-america/howto', 
          'projetos/dados/gov-latin-america/codedoc',
          'projetos/dados/gov-latin-america/id-visual'],       
        },
      ],
    
    projetosExtensao: [
      {
        type: 'category',
        label: 'Projetos de Extensão',
        items: [
          {
            type: 'link',
            href: '/docs/projetos/intro',
            label: 'Projetos',
          },
      'projetos/extensao/intro',
      'projetos/extensao/iniciativas',
      'projetos/extensao/id-visual',
      'projetos/extensao/pod-ri'
      ],
    },
  ],
    projetosEnsino: [
      {
        type: 'category',
        label: 'Projetos de Ensino',
        items: [
          {
            type: 'link',
            href: '/docs/projetos/intro',
            label: 'Projetos',
          },
      'projetos/ensino/intro',
      'projetos/ensino/iniciativas',
      'projetos/ensino/id-visual'
      ],
    },
    ],

    projetosEnsinoTrilhaDados: [
      {
        type: 'category',
        label: 'Trilha de Dados',
        items: [
          'projetos/ensino/trilha-dados/intro',
          'projetos/ensino/trilha-dados/howto',
          'projetos/ensino/trilha-dados/codedoc', 
          'projetos/ensino/trilha-dados/webscraping/intro', 
          'projetos/ensino/trilha-dados/metodos-quantitativos/intro'],
          
        },
      ],

    projetosEnsinoCursoPython: [
      {
        type: 'category',
        label: 'Curso de Python',
        items: [
          'projetos/ensino/linguagens/python/intro',
          'projetos/ensino/linguagens/python/howto',
          'projetos/ensino/linguagens/python/codedoc'],
        },
      ],
    projetosEnsinoCursoR: [
      {
        type: 'category',
        label: 'Curso de R',
        items: [
          'projetos/ensino/linguagens/r-lang/intro',
          'projetos/ensino/linguagens/r-lang/howto',
          'projetos/ensino/linguagens/r-lang/codedoc'],
      },
    ],
    projetosEnsinoGit: [
      {
        type: 'category',
        label: 'Versionamento - GIT',
        items: [
          'projetos/ensino/linguagens/versionamento/intro',
          'projetos/ensino/linguagens/versionamento/integrar-branches'],
      },
    ],
    projetosSistemas: [
      {
        type: 'category',
        label: 'Projetos de sistemas',
        items: [
          {
            type: 'link',
            href: '/docs/projetos/intro',
            label: 'Projetos',
          },
            'projetos/sistemas/intro',
             {
            type: 'link',
            href: '/docs/projetos/sistemas/linux/intro',
            label: 'Linux',
          },
          {
            type: 'link',
            href: '/docs/projetos/sistemas/redes/intro',
            label: 'Redes',
          },
          {
            type: 'link',
            href: '/docs/projetos/sistemas/vm-container/intro',
            label: 'Virtualização',
          },
          {
            type: 'link',
            href: '/docs/projetos/sistemas/infra-as-code/intro',
            label: 'Infra-as-Code',
          },
          {
            type: 'link',
            href: '/docs/projetos/sistemas/monitoramento/intro',
            label: 'Monitoramento',
          },
          {
            type: 'link',
            href: '/docs/projetos/sistemas/tutorial-dual-boot/tutorial',
            label: 'Tutorial Dual Boot',
          },
          {
            type: 'link',
            href: '/docs/projetos/sistemas/dev/intro',
            label: 'Aplicações e Softwares',
          },
        
            'projetos/sistemas/id-visual'],
          },
        ],

    projetosSistemasLinux: [
      {
        type: 'category',
        label: 'Linux',
        items: [
          'projetos/sistemas/linux/intro',
          {
            type: 'category',
            label: 'PCS LabRI/UNESP',
            items: [
              'projetos/sistemas/linux/sala-labri/atualizar-pcs',
              'projetos/sistemas/linux/sala-labri/tela_preta',
              
            ],
          },
          'projetos/sistemas/linux/terminais',
          
          {
            type: 'category',
            label: 'Acesso Remoto',
            items: [
              'projetos/sistemas/linux/acesso-remoto/chrome-remote-desktop',
              'projetos/sistemas/linux/acesso-remoto/x2go',  
            ],
          },
          'projetos/sistemas/linux/recoll',
          'projetos/sistemas/linux/backup',
          'projetos/sistemas/linux/storage',
          'projetos/sistemas/linux/ldap',
          'projetos/sistemas/linux/cron'],
        },
      ],
      projetosSistemasRedes: [
        {
          type: 'category',
          label: 'Redes',
          items: [
            'projetos/sistemas/redes/intro',
            'projetos/sistemas/redes/pfsense']
          },
        ],
        projetosSistemasInfaAsCode: [
          {
            type: 'category',
            label: 'Infra-as-Code',
            items: [
              'projetos/sistemas/infra-as-code/intro',
              'projetos/sistemas/infra-as-code/ansible',
              'projetos/sistemas/infra-as-code/packer',
              'projetos/sistemas/infra-as-code/terraform',]
            },
          ],
      geralAudioVisual: [
        {
          type: 'category',
          label: 'Audiovisual',
          items: [
            'geral/audiovisual/intro'],
          },
        ],

    projetosSistemasDevLabri: [
      {
        type: 'category',
        label: 'Aplicações e Softwares',
        items: [
          'projetos/sistemas/dev/intro',
          'projetos/sistemas/dev/sites/intro',
             {
              type: 'link',
              href: '/docs/projetos/sistemas/dev/recollweb/intro', 
              label: 'RecollWeb',
            }, 
          'projetos/sistemas/dev/geral/cadastro-usuarios',
          'projetos/sistemas/dev/geral/cadernos-labri',
          'projetos/sistemas/dev/geral/certificados',
          'projetos/sistemas/dev/geral/ocr'],

        },
      ],
      projetosSistemasVmContainer: [
        {
          type: 'category',
          label: 'Virtualização',
          items: [
            'projetos/sistemas/vm-container/intro',
            {
              type: 'category',
              label: 'Proxmox',
              items: [
                'projetos/sistemas/vm-container/proxmox/intro',
                'projetos/sistemas/vm-container/proxmox/instalar',
                'projetos/sistemas/vm-container/proxmox/network',
                'projetos/sistemas/vm-container/proxmox/storage',
                
              ]
            }, 
            ,
            'projetos/sistemas/vm-container/container',
            'projetos/sistemas/vm-container/kubernetes',
            'projetos/sistemas/vm-container/rancher'],
          },
        ],
      projetosSistemasMonitoramento: [
            {
              type: 'category',
              label: 'Monitoramento',
              items: [
                'projetos/sistemas/monitoramento/intro'],
              },
            ],
      projetosSistemasTutorialDualBoot: [
            {
              type: 'category',
              label: 'Tutorial Dual Boot',
              items: [
                'projetos/sistemas/tutorial-dual-boot/tutorial'],
              },
            ],
      projetosSistemasRecollWeb: [
              {
                type: 'category',
                label: 'RecollWeb',
                items: [
                  'projetos/sistemas/dev/recollweb/intro', 
                  'projetos/sistemas/dev/recollweb/howto', 
                  'projetos/sistemas/dev/recollweb/codedoc'],
                },
              ],
   equipe: [
    {
     type: 'category',
     label: 'Projetos',
     items: [
        'geral/equipe/intro',
        {
        'Equipe Atual': [
            {
              type: 'link',
              href: '/docs/geral/equipe/atual/fabio-paron/intro',
              label: 'Fabio Paron',
            }, 
             {
              type: 'link',
              href: '/docs/geral/equipe/atual/julia-silveira/intro',
              label: 'Julia Silveira',
            }, 
             {
              type: 'link',
              href: '/docs/geral/equipe/atual/rafael-almeida/intro',
              label: 'Rafael Almeida',
            }, 
            {
              type: 'link',
              href: '/docs/geral/equipe/atual/fulano-teste/intro',
              label: 'Fulano',
            }, 
          ],
        },
        {
        'Ex-membros': [
            {
              type: 'link',
              href: '/docs/geral/equipe/ex-membro/pedro-campagna/intro',
              label: 'Pedro Campagna',
            }, 
          ],
        },
        ],
      },
    ],

    equipeFabioParon: [ // projetos de dados LabRI/UNESP
        {
          type: 'category',
          label: 'Fabio Paron',
          items: ['geral/equipe/atual/fabio-paron/intro'],
        },
      ],
     equipeJuliaSilveira: [ // projetos de dados LabRI/UNESP
        {
          type: 'category',
          label: 'Julia Silveira',
          items: ['geral/equipe/atual/julia-silveira/intro'],
        },
      ],
       equipeRafaelAlmeida: [ // projetos de dados LabRI/UNESP
        {
          type: 'category',
          label: 'Rafael Almeida',
          items: [
            'geral/equipe/atual/rafael-almeida/intro',
            'geral/equipe/atual/rafael-almeida/ansible',
            'geral/equipe/atual/rafael-almeida/terraform',
            'geral/equipe/atual/rafael-almeida/container',
            'geral/equipe/atual/rafael-almeida/kubernetes',
            'geral/equipe/atual/rafael-almeida/rancher',
            'geral/equipe/atual/rafael-almeida/linux_mm/intro'
        ],
        },
      ],
      equipePedroCampagna: [ // projetos de dados LabRI/UNESP
        {
          type: 'category',
          label: 'Pedro Campagna',
          items: ['geral/equipe/ex-membro/pedro-campagna/intro'],
        },
      ],
    
  };
  
