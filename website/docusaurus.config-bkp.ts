// import navbarConfig = require("./config/navbar.config");
import type { Config } from "@docusaurus/types";

// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'LabRI - Site de Apoio',
  tagline: 'Desenvolvimento de projetos',
  organizationName: 'facebook', // Usually your GitHub org/user name.
  projectName: 'sites/apoio2', // Usually your repo name.
  baseUrl: '/',
  baseUrlIssueBanner: true,
  url: 'https://apoio.labriunesp.org',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  
  themes: [
    // ... Your other themes.
    [
      require.resolve("@easyops-cn/docusaurus-search-local"),
      {
        // ... Your options.
        // `hashed` is recommended as long-term-cache of index file is possible.
        hashed: true,
        // For Docs using Chinese, The `language` is recommended to set to:
        language: ["en", "pt", "es"],
        highlightSearchTermsOnTargetPage: true
        
      },
    ],
  ],

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      {
        debug: true,
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          sidebarCollapsible: true, 
          sidebarCollapsed: true,
          showLastUpdateAuthor: true,
          showLastUpdateTime: true,
          editUrl: 'https://gitlab.com/unesp-labri/sites/apoio/-/tree/main/website/',
          remarkPlugins: [require('mdx-mermaid')],
        },
        blog: {
          showReadingTime: true, 
          blogTitle: 'Cadernos LabRI',
          blogDescription: 'Publicações do LabRI',
          postsPerPage: 6,
          include: ['**/*.{md,mdx, js}'],
          blogListComponent: '@theme/BlogListPage',
          blogPostComponent: '@theme/BlogPostPage',
          blogTagsListComponent: '@theme/BlogTagsListPage',
          blogTagsPostsComponent: '@theme/BlogTagsPostsPage',
          blogSidebarTitle: 'Últimas publicações',
          blogSidebarCount: 0,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/unesp-labri/sites/apoio/-/tree/main/website/blog',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],  
  ],
  
  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      docs: {
        sidebar: {  
          hideable: true,
          autoCollapseCategories: true,
        },
      },
      colorMode: { // add
        defaultMode: 'light',
        disableSwitch: false,
        respectPrefersColorScheme: true,
      },
      navbar: {
        hideOnScroll: true, // add
        title: 'Apoio LabRI',
        logo: {
          alt: 'LabRI',
          src: 'img/labriunesp-02.svg',
        },
        items: [
          {
            type: 'doc',
            position: 'right',
            docId: 'geral/intro',
            label: 'Geral',
          },
          {
            type: 'doc',
            position: 'right',
            docId: 'projetos/dados/intro',
            label: 'Projetos de Dados',
          },
          {
            type: 'doc',
            position: 'right',
            docId: 'projetos/ensino/intro',
            label: 'Projetos de Ensino',
          },
          {
            type: 'doc',
            position: 'right',
            docId: 'projetos/extensao/intro',
            label: 'Projetos de Extensão',
          },
          {
            type: 'doc',
            position: 'right',
            docId: 'projetos/sistemas/intro',
            label: 'Projetos de Sistemas',
          },
          {
            href: 'https://gitlab.com/unesp-labri',
            label: 'Gitlab',
            position: 'right',
            className: 'header-gitlab-link',
            'aria-label': 'GitLab repository'
          },
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Tutorial',
          },
          {to: '/blog', label: 'Blog', position: 'left'},
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Tutorial',
                to: '/docs/intro',
              },
            ],
          },
          {
            title: 'Redes Sociais',
            items: [
              {
                label: 'Youtube',
                href: 'https://www.youtube.com/channel/UCHx_m-4Cv7_ZLEDUe_wYATA/featured',
              },
              {
                label: 'Facebook',
                href: 'https://www.facebook.com/labriunesp',
              },
              {
                label: 'Twitter',
                href: 'https://twitter.com/labriunesp',
              },
              {
                label: 'Instagram',
                href: 'https://www.instagram.com/unesplabri/',
              },
              {
                label: 'Linkedin',
                href: 'https://www.linkedin.com/company/labri-unesp-franca/',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'Blog',
                to: '/blog',
              },
              {
                label: 'GitLab',
                href: 'https://gitlab.com/unesp-labri/sites/apoio',
              },
            ],
          },
        ],
        copyright: `${new Date().getFullYear()} Site feito e mantido pelos membros do LabRI/UNESP. Construído com Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
